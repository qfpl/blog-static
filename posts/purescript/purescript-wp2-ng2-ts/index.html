<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Queensland FP Lab - Basic Integration of Purescript into a Webpack/NG2/TS Project</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../../../css/custom.css">
        <link rel="stylesheet" href="../../../css/syntax.css">
        
          
        
        <link rel="alternate" type="application/atom+xml" title="Queensland Functional Programming Lab Blog" href="../../../atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Queensland Functional Programming Lab Blog" href="../../../rss.xml">
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106321204-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments)};
          gtag('js', new Date());

          gtag('config', 'UA-106321204-1', { 'anonymize_ip': true });
        </script>
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg data61-green">
              <a class="navbar-brand text-white" href="../../../">
                <div class="card brand-logo">
                  <div class="card-body">
                    <div class="brand-anim-block">
                      <img id="qfpl-brand-data61-logo" height="55" alt="Data61 logo" src="../../../images/DATA61-CSIRO_MONO.png" />
                    </div>
                  </div>
                </div>
              </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <i class="fa fa-lg fa-fw fa-bars text-white"></i>
              </button>

              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../location">Location</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../people">People</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../talks">Talks</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../contact">Contact</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../archive">Archive</a>
                    </li>
                  </ul>
                </div>
            </nav>
        </header>

        <main>
          <div class="container-fluid">
              <div class="container">
  <div class="row">
    <div class="col my-2">
      <h2>Basic Integration of Purescript into a Webpack/NG2/TS Project</h2>
      <small>Posted on August 21, 2017</small>
    </div>
  </div>

  <div class="row">
    <div class="col my-2">
      <p>New tools and languages can be wonderful things. But all too often the excitement around these new things is focused around building an entirely new X, or ‘I had some spare time so I rewrote Y’. Where we often need them most is in an existing project, sometimes one that is starting to struggle under its own weight or showing its age.</p>
<p>This post hopes to demonstrate that Purescript is one new tool that you can integrate with an existing project without blowing out your complexity budget. This enables you to start subsuming your legacy code without the horrendous cost of a total rebuild. Additionally this process enables the team to be brought up to speed on Purescript on demand, without requiring an all-or-nothing approach.</p>
<p>So, with some assumptions, lets begin…</p>
<h3 id="some-things-i-have-assumed">Some things I have assumed</h3>
<ul>
<li>A *nix-like terminal environment</li>
<li>A working knowledge of Javascript, NPM, and Bower</li>
<li>A functioning install of NodeJS with <code>bower</code> and <code>npm</code> on the $PATH</li>
<li>A basic, or working knowledge of Purescript, and a desire for more</li>
<li>A functioning install of Purescript 0.11.5 or greater, <code>purs</code> available on the $PATH</li>
<li>A clone of <a href="https://github.com/AngularClass/angular-starter">angular-starter</a> ( I used commit: 5996474023922906c05351ff43e2334efe38eccf )</li>
</ul>
<h3 id="preparing-the-build-pipeline">Preparing the build pipeline</h3>
<p>Prepare the angular-starter dependencies by running:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1"></a>$ <span class="ex">npm</span> install</span></code></pre></div>
<p>This will install all of the base dependencies required by the standard project. To include Purescript in a Webpack project, you’re going to need a loader plugin that will tell Webpack what to do with Purescript files. The simplest option is the <a href="https://github.com/ethul/purs-loader" title="purs-loader">purs-loader</a> plugin. We’ll also use the <a href="https://github.com/natefaubion/purescript-psa" title="purescript-psa">purescript-psa</a> npm package to help with displaying the compiler output from Purescript. Lets install both of those. In your project root:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1"></a>$ <span class="ex">npm</span> install --save-dev purescript-psa purs-loader</span></code></pre></div>
<p>Once that is done and assuming everything has completed successfully, you should be able to run <code>$ npm start</code>. This will build the fresh application and open it in a browser window. Click around a bit and familiarise yourself with what is going on, because we’ll be breaking it soon.</p>
<p>First off, we have to tell Webpack what to do with the <code>.purs</code> files that it encounters. Add the following configuration to the ‘rules’ array in the ‘module’ property within the common Webpack configuration in <code>config/webpack.common.js</code>:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode javascript"><code class="sourceCode javascript"><span id="cb3-1"><a href="#cb3-1"></a><span class="op">{</span> </span>
<span id="cb3-2"><a href="#cb3-2"></a>  <span class="dt">test</span><span class="op">:</span> <span class="ss">/</span><span class="sc">\.</span><span class="ss">purs</span><span class="sc">$</span><span class="ss">/</span><span class="op">,</span></span>
<span id="cb3-3"><a href="#cb3-3"></a>  <span class="dt">use</span><span class="op">:</span> [</span>
<span id="cb3-4"><a href="#cb3-4"></a>    <span class="op">{</span></span>
<span id="cb3-5"><a href="#cb3-5"></a>      <span class="dt">loader</span><span class="op">:</span> <span class="st">&quot;purs-loader&quot;</span><span class="op">,</span></span>
<span id="cb3-6"><a href="#cb3-6"></a>      <span class="dt">options</span><span class="op">:</span> <span class="op">{</span></span>
<span id="cb3-7"><a href="#cb3-7"></a>        <span class="dt">psc</span><span class="op">:</span> <span class="st">&quot;psa&quot;</span><span class="op">,</span></span>
<span id="cb3-8"><a href="#cb3-8"></a>        <span class="dt">output</span><span class="op">:</span> <span class="st">&quot;dist&quot;</span><span class="op">,</span> <span class="co">// *** This must match your JS output directory</span></span>
<span id="cb3-9"><a href="#cb3-9"></a>        <span class="dt">src</span><span class="op">:</span> [</span>
<span id="cb3-10"><a href="#cb3-10"></a>          <span class="co">// This ensures that our library code is included</span></span>
<span id="cb3-11"><a href="#cb3-11"></a>          <span class="st">&quot;bower_components/purescript-*/src/**/*.purs&quot;</span><span class="op">,</span></span>
<span id="cb3-12"><a href="#cb3-12"></a>          <span class="co">// This is our source folder where we will keep our Purescript code</span></span>
<span id="cb3-13"><a href="#cb3-13"></a>          <span class="st">&quot;src/app/purescript/**/*.purs&quot;</span></span>
<span id="cb3-14"><a href="#cb3-14"></a>        ]</span>
<span id="cb3-15"><a href="#cb3-15"></a>      <span class="op">}</span></span>
<span id="cb3-16"><a href="#cb3-16"></a>    <span class="op">}</span></span>
<span id="cb3-17"><a href="#cb3-17"></a>  ]</span>
<span id="cb3-18"><a href="#cb3-18"></a><span class="op">}</span></span></code></pre></div>
<p>The above provides a rule to Webpack that when the test for a Purescript file extension is successful, it will run the ‘purs-loader’ with the given configuration. The destination for the compiled Purescript files defaults to <code>output</code>, this configuration setting must be changed to be the same destination as the rest of the JavaScript in the project. In our case, we will changing the value of the <code>output</code> setting to be <code>dist</code>. It also tells <code>purs-loader</code> to use the <code>purescript-psa</code> package as the communication gateway with the compiler, making for nicer error messages.</p>
<p>We need to make some other adjustments to our Webpack configuration to ensure that the Purescript code is included in the final application. First add the Purescript file extension to the ‘extensions’ property:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode javascript"><code class="sourceCode javascript"><span id="cb4-1"><a href="#cb4-1"></a>extensions<span class="op">:</span> [<span class="st">&quot;.purs&quot;</span><span class="op">,</span> ...]</span></code></pre></div>
<p>Finally, we have to ensure that our Purescript modules that are installed via Bower are included in the list of modules that Webpack includes in the build process. If we don’t do this then none of the library code we download to use will be included and our code won’t be very useful.</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode javascript"><code class="sourceCode javascript"><span id="cb5-1"><a href="#cb5-1"></a>modules<span class="op">:</span> [</span>
<span id="cb5-2"><a href="#cb5-2"></a>  ...<span class="op">,</span></span>
<span id="cb5-3"><a href="#cb5-3"></a>  <span class="st">&quot;bower_components&quot;</span></span>
<span id="cb5-4"><a href="#cb5-4"></a>]</span></code></pre></div>
<p>Without changing anything else, run <code>$ npm start</code> to ensure we haven’t broken anything. If everything comes up as it did before then we should be ready to start including some Purescript code!</p>
<h3 id="adding-some-purescript">Adding some Purescript</h3>
<p>Lets add something that will do some “work” with the input value before it is placed on the <code>AppState</code>. This will demonstrate how to build a Purescript module that can be used by Javascript like it were any normal package.</p>
<p>Start up the development server for this project so we can start to see any feedback from the compiler or build process.</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1"></a>$ <span class="ex">npm</span> start</span></code></pre></div>
<p>This should build everything and start up a webserver, additionally it will start watchers that will rebuild the project whenever files change. This includes compiling our Purescript code! It will happily purr away in the background whilst we work.</p>
<p><strong>NB:</strong> With the way this project is configured, any code that is not actually in use by the application will be discarded. So if you write some Purescript code and you don’t see any output regarding it from Webpack, it’s likely that is because it’s not being used/called by the Javascript code. Thus it will be pruned by the Webpack tree-shaking/dead-code elimination process.</p>
<p>Navigate to the <code>src/app</code> folder and create the <code>purescript</code> directory.</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb7-1"><a href="#cb7-1"></a>$ <span class="bu">cd</span> src/app</span>
<span id="cb7-2"><a href="#cb7-2"></a>$ <span class="fu">mkdir</span> purescript</span></code></pre></div>
<p>Or your operational equivalent…</p>
<p>Then create our first basic module, lets call it <code>Basic</code>.</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb8-1"><a href="#cb8-1"></a>$ <span class="bu">cd</span> purescript</span>
<span id="cb8-2"><a href="#cb8-2"></a>$ <span class="fu">touch</span> Basic.purs</span></code></pre></div>
<p>Open this file in your favourite editor.</p>
<p>Start with the Purescript module declaration. This line is required in every Purescript file and provides not only the name of the module, but also lists what this module exports. The requirement being that anything it exports must be in scope, and the module name must match the file name.</p>
<p>To start with, we’ll put the following at the top of our file:</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb9-1"><a href="#cb9-1"></a><span class="kw">module</span> <span class="dt">Basic</span> <span class="kw">where</span></span></code></pre></div>
<p>We’re not listing anything to export yet, so Purescript will export everything we define. We’ll fix that later.</p>
<p>Our function will be simple: duplicate the input string. It will take a <code>String</code> and return a <code>String</code>. So if we’re given <code>"Foo"</code> then we’ll return <code>"FooFoo"</code>.</p>
<p>To do that we’ll use a function from the Purescript <code>Prelude</code> to combine our input. Move down a couple of lines and then add the following:</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb10-1"><a href="#cb10-1"></a><span class="ot">doubler ::</span> <span class="dt">String</span> <span class="ot">-&gt;</span> <span class="dt">String</span></span>
<span id="cb10-2"><a href="#cb10-2"></a>doubler inp <span class="ot">=</span> inp <span class="op">&lt;&gt;</span> inp</span></code></pre></div>
<p>The operator, that <code>&lt;&gt;</code> thing in the middle, is an <a href="https://github.com/purescript/documentation/blob/master/language/Syntax.md#binary-operators">“infix function”</a> defined in the <code>Prelude</code>. It’s from the <code>Semigroup</code> typeclass, and when used with Strings will concatenate the given inputs.</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb11-1"><a href="#cb11-1"></a><span class="st">&quot;fuzz&quot;</span> <span class="op">&lt;&gt;</span> <span class="st">&quot;fuzz&quot;</span> <span class="ot">=</span> <span class="st">&quot;fuzzfuzz&quot;</span></span>
<span id="cb11-2"><a href="#cb11-2"></a><span class="st">&quot;foo&quot;</span> <span class="op">&lt;&gt;</span> <span class="st">&quot;bar&quot;</span> <span class="ot">=</span> <span class="st">&quot;foobar&quot;</span></span></code></pre></div>
<p>Now we have some bookkeeping to do to ensure that both the <code>String</code> type and the <code>&lt;&gt;</code> function are in scope. So lets add a <code>Prelude</code> import towards the top of our file to take care of this.</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb12-1"><a href="#cb12-1"></a><span class="kw">import</span> <span class="dt">Prelude</span></span></code></pre></div>
<p>We also need to make sure that the Purescript <code>Prelude</code> module is actually installed:</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb13-1"><a href="#cb13-1"></a>$ <span class="ex">bower</span> install --save purescript-prelude</span></code></pre></div>
<p>Now change the module declaration at the top of the file so that we’re only exporting this one function. To do that we add a list of functions or types before the <code>where</code> keyword. Surrounded by parentheses and separated by commas: So our <code>module</code> line changes from:</p>
<div class="sourceCode" id="cb14"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb14-1"><a href="#cb14-1"></a><span class="kw">module</span> <span class="dt">Basic</span> <span class="kw">where</span></span></code></pre></div>
<p>To:</p>
<div class="sourceCode" id="cb15"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb15-1"><a href="#cb15-1"></a><span class="kw">module</span> <span class="dt">Basic</span> (doubler) <span class="kw">where</span></span></code></pre></div>
<h3 id="importing-to-javascript">Importing to JavaScript</h3>
<p>Including this module in our Javascript code is the same as using any other module, with a few gotchas that we’ll touch on later. For now, open up the <code>home.component.ts</code> file and look for the <code>submitState</code> function.</p>
<p>This function takes the input from the text field on the ‘Home’ page, pushes it to the <code>appState</code> and then clears the value. We’re going to intrude on this process and pass the input through our Purescript function, before setting the value on the <code>appState</code>.</p>
<p>We need to import the function from our Purescript module. Purescript compiles to CommonJS modules, so importing into Javascript land is quite easy. This project provides ES6 features, so we’re able to use the new import syntax:</p>
<div class="sourceCode" id="cb16"><pre class="sourceCode javascript"><code class="sourceCode javascript"><span id="cb16-1"><a href="#cb16-1"></a><span class="im">import</span> <span class="op">*</span> Basic <span class="im">from</span> <span class="st">'../purescript/Basic'</span><span class="op">;</span></span></code></pre></div>
<p>This is the same as:</p>
<div class="sourceCode" id="cb17"><pre class="sourceCode javascript"><code class="sourceCode javascript"><span id="cb17-1"><a href="#cb17-1"></a><span class="kw">const</span> Basic <span class="op">=</span> <span class="at">require</span>(<span class="st">'../purescript/Basic'</span>)<span class="op">;</span></span></code></pre></div>
<p>You can do selective imports as well, if the functions are exported from your Purescript module:</p>
<div class="sourceCode" id="cb18"><pre class="sourceCode javascript"><code class="sourceCode javascript"><span id="cb18-1"><a href="#cb18-1"></a><span class="im">import</span> <span class="op">{</span> doubler <span class="op">}</span> <span class="im">from</span> <span class="st">&quot;../purescript/Basic&quot;</span><span class="op">;</span></span></code></pre></div>
<p>Using the first import example, we have our Purescript module imported as ‘Basic’, so we’re able to use it to modify our input. Go to the <code>submitState</code> function and change the following line:</p>
<div class="sourceCode" id="cb19"><pre class="sourceCode javascript"><code class="sourceCode javascript"><span id="cb19-1"><a href="#cb19-1"></a><span class="kw">this</span>.<span class="va">appState</span>.<span class="at">set</span>(<span class="st">&quot;value&quot;</span><span class="op">,</span> value)<span class="op">;</span></span></code></pre></div>
<p>To be:</p>
<div class="sourceCode" id="cb20"><pre class="sourceCode javascript"><code class="sourceCode javascript"><span id="cb20-1"><a href="#cb20-1"></a><span class="kw">this</span>.<span class="va">appState</span>.<span class="at">set</span>(<span class="st">&quot;value&quot;</span><span class="op">,</span> <span class="va">Basic</span>.<span class="at">doubler</span>(value))<span class="op">;</span></span></code></pre></div>
<h3 id="kicking-the-tyres">Kicking the tyres</h3>
<p>If your development server isn’t running, you can start it by typing <code>$ npm start</code> at the root of the project.</p>
<p>On the ‘Home’ page of the web-app, you should be able to type something into the ‘Local State’ text field and click the <code>Submit Value</code> button. As you type you should see the <code>localState</code> being updated with the latest value. Typing “Turbo” should take this:</p>
<div class="sourceCode" id="cb21"><pre class="sourceCode javascript"><code class="sourceCode javascript"><span id="cb21-1"><a href="#cb21-1"></a><span class="kw">this</span>.<span class="at">localState</span> <span class="op">=</span> <span class="op">{</span></span>
<span id="cb21-2"><a href="#cb21-2"></a>  <span class="st">&quot;value&quot;</span><span class="op">:</span> <span class="st">&quot;&quot;</span></span>
<span id="cb21-3"><a href="#cb21-3"></a><span class="op">}</span></span></code></pre></div>
<p>… to this</p>
<div class="sourceCode" id="cb22"><pre class="sourceCode javascript"><code class="sourceCode javascript"><span id="cb22-1"><a href="#cb22-1"></a><span class="kw">this</span>.<span class="at">localState</span> <span class="op">=</span> <span class="op">{</span></span>
<span id="cb22-2"><a href="#cb22-2"></a>  <span class="st">&quot;value&quot;</span><span class="op">:</span> <span class="st">&quot;Turbo&quot;</span></span>
<span id="cb22-3"><a href="#cb22-3"></a><span class="op">}</span></span></code></pre></div>
<p>Clicking <code>Submit Value</code> should then change both our <code>localState</code> and our <code>appState</code> to the following values:</p>
<div class="sourceCode" id="cb23"><pre class="sourceCode javascript"><code class="sourceCode javascript"><span id="cb23-1"><a href="#cb23-1"></a><span class="kw">this</span>.<span class="at">localState</span> <span class="op">=</span> <span class="op">{</span></span>
<span id="cb23-2"><a href="#cb23-2"></a>  <span class="st">&quot;value&quot;</span><span class="op">:</span> <span class="st">&quot;&quot;</span></span>
<span id="cb23-3"><a href="#cb23-3"></a><span class="op">}</span></span>
<span id="cb23-4"><a href="#cb23-4"></a><span class="kw">this</span>.<span class="va">appState</span>.<span class="at">state</span> <span class="op">=</span> <span class="op">{</span></span>
<span id="cb23-5"><a href="#cb23-5"></a>  <span class="st">&quot;value&quot;</span><span class="op">:</span> <span class="st">&quot;TurboTurbo&quot;</span></span>
<span id="cb23-6"><a href="#cb23-6"></a><span class="op">}</span></span></code></pre></div>
<p>If you see the duplicated value on the <code>appState</code> then congratulations, you have successfully integrated your Purescript module with the larger Javascript application.</p>
<p>This was a pure function, simple inputs and outputs with no side-effects. However you can still notice the benefits that this might provide if you have a particularly gnarly piece of business logic, or you would like to lean on the abstractions available in Purescript.</p>
<p>Perhaps more importantly, this technique and what will be covered in the next article provide the basis for gradually subsuming the Javascript in a project with Purescript. Components and functionality can be replaced piece by piece without requiring an ‘all or nothing’ type approach.</p>
<h3 id="where-to-from-here">Where to from here?</h3>
<p>Next time we’ll build a standalone UI component using <a href="https://github.com/sharkdp/purescript-flare">Flare</a>, and then demonstrate how to pass data from the Flare component back into our free wheeling javascript world.</p>
    </div>
  </div>

  
  <div class="row">
    <div class="col my-2">
      
    </div>
  </div>
  

  
  <div class="row">
    <div class="col my-2">
      
        <div class="p-1">
  <h4 class="data61-bottom-line">
    > <a href="../../../people/schalmers">Sean Chalmers</a>
  </h4>
  <p>Likes riding motorcycles, lenses, text editors, software that works, and writing documentation. Hates not having errors as values, not being able to use lenses, and writing bios.</p>
</div>

      
    </div>
  </div>
  
</div>

          </div>
          <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li role="presentation"><a href="../../../">Home</a></li>
                  <li role="presentation"><a href="../../../location">Location</a></li>
                  <li role="presentation"><a href="../../../people">People</a></li>
                  <li role="presentation"><a href="../../../projects">Projects</a></li>
                  <li role="presentation"><a href="../../../talks">Talks</a></li>
                  <li role="presentation"><a href="../../../contact">Contact</a></li>
                  <li role="presentation"><a href="../../../archive">Archive</a></li>
              </ul>
          </div>
        </main>

        <footer class="page-footer data61-green">
            <div class="container">
              <div class="row justify-content-centre">
                <div class="col">
                  <a href="https://github.com/qfpl"><i class="text-white fa fa-github fa-2x"></i></a>
                  <a href="https://twitter.com/queenslandfplab"><i class="text-white fa fa-twitter fa-2x"></i></a>
                  <a href="mailto:contact@qfpl.io"><i class="text-white fa fa-envelope-o fa-2x"></i></a>
                </div>
                <div class="col">
                  <a class="float-right" rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
                  </a>
                </div>
              </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>

        <script src="../../../js/bootstrap.min.js"></script>

        
          
        
    </body>
</html>
