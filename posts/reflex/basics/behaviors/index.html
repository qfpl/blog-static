<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Queensland FP Lab - Behaviors</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../../../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../../../../css/custom.css">
        <link rel="stylesheet" href="../../../../css/syntax.css">
        
          
            <link rel="stylesheet" href="../../../../css/reflex/basics/grid-light.css">
          
        
        <link rel="alternate" type="application/atom+xml" title="Queensland Functional Programming Lab Blog" href="../../../../atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Queensland Functional Programming Lab Blog" href="../../../../rss.xml">
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106321204-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments)};
          gtag('js', new Date());

          gtag('config', 'UA-106321204-1', { 'anonymize_ip': true });
        </script>
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg data61-green">
              <a class="navbar-brand text-white" href="../../../../">
                <div class="card brand-logo">
                  <div class="card-body">
                    <div class="brand-anim-block">
                      <img id="qfpl-brand-data61-logo" height="55" alt="Data61 logo" src="../../../../images/DATA61-CSIRO_MONO.png" />
                    </div>
                  </div>
                </div>
              </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <i class="fa fa-lg fa-fw fa-bars text-white"></i>
              </button>

              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../../">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../../location">Location</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../../people">People</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../../projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../../talks">Talks</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../../contact">Contact</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../../archive">Archive</a>
                    </li>
                  </ul>
                </div>
            </nav>
        </header>

        <main>
          <div class="container-fluid">
              <div class="container">
  <div class="row">
    <div class="col my-2">
      <h2>Behaviors</h2>
      <small>Posted on September 22, 2017</small>
    </div>
  </div>

  <div class="row">
    <div class="col my-2">
      <div id="grid-setup">

</div>
<p><a href="../events/">Previously</a> we had a look at <code>Event</code>s, which describe values occurring at particular instants in time. Now we’ll have a look at the other half of the core FRP types - <code>Behavior</code>s.</p>
<h2 id="what-is-a-behavior">What is a <code>Behavior</code>?</h2>
<p>A <code>Behavior</code> is something that has a value at <em>all</em> points in time.</p>
<p>A <code>Behavior</code> in <code>reflex</code> looks like this:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb1-1"><a href="#cb1-1"></a><span class="kw">data</span> <span class="dt">Behavior</span> t a</span></code></pre></div>
<p>and we can think of it as being like a function from time to values:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb2-1"><a href="#cb2-1"></a>t <span class="ot">-&gt;</span> a</span></code></pre></div>
<p>We are working with a discrete-time FRP system and so this function is going to be piecewise-linear, with any transitions happening when <code>Event</code>s occur. We are already using <code>Event</code>s as our logical clock, and so we will use <code>Event</code>s to build up our time-varying <code>Behavior</code>s and we will use <code>Event</code>s to sample <code>Behavior</code>s.</p>
<p>We’re going to be using <code>Behavior</code>s for working with state in the system. They are a very different way of handling state than the <code>State</code> monad, but you get used to them pretty quickly once you start writing some code with them.</p>
<h2 id="getting-started-with-behaviors">Getting started with <code>Behavior</code>s</h2>
<p>Let us start by creating a <code>Behavior</code>.</p>
<p>We can do this with <code>hold</code>, which is a method from the <code>MonadHold</code> typeclass:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb3-1"><a href="#cb3-1"></a><span class="ot">hold ::</span> <span class="dt">MonadHold</span> t m</span>
<span id="cb3-2"><a href="#cb3-2"></a>     <span class="ot">=&gt;</span> a</span>
<span id="cb3-3"><a href="#cb3-3"></a>     <span class="ot">-&gt;</span> <span class="dt">Event</span> t a</span>
<span id="cb3-4"><a href="#cb3-4"></a>     <span class="ot">-&gt;</span> m (<span class="dt">Behavior</span> t a)</span></code></pre></div>
<p>This takes an initial value and an <code>Event</code> as input. The <code>Behavior</code> uses the initial value until the first firing of the input <code>Event</code>. After that, the <code>Behavior</code> has the value the <code>Event</code> had at the last time it fired.</p>
<p>(We’ll look at precisely what we mean by “after” in a moment)</p>
<p>The use of the <code>MonadHold</code> typeclass constraint indicates that we’re doing something that will have an effect on the behavior of the FRP network in future frames. Under the hood, <code>hold</code> is modifying the FRP network in order to add some state, so we can think of the <code>MonadHold</code> context as a builder for an FRP network.</p>
<p>We could use this to keep hold of the last colour that was clicked on:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb4-1"><a href="#cb4-1"></a><span class="ot">sampleBlue ::</span> <span class="dt">MonadHold</span> t m </span>
<span id="cb4-2"><a href="#cb4-2"></a>           <span class="ot">=&gt;</span> <span class="dt">Event</span> t <span class="dt">Colour</span> </span>
<span id="cb4-3"><a href="#cb4-3"></a>           <span class="ot">-&gt;</span> <span class="op">?</span></span>
<span id="cb4-4"><a href="#cb4-4"></a>sampleBlue eInput         <span class="ot">=</span> <span class="kw">do</span></span>
<span id="cb4-5"><a href="#cb4-5"></a>  <span class="co">-- bColour :: Behavior t Colour</span></span>
<span id="cb4-6"><a href="#cb4-6"></a>  bColour <span class="ot">&lt;-</span> hold <span class="dt">Blue</span> eInput</span>
<span id="cb4-7"><a href="#cb4-7"></a> </span></code></pre></div>
<p>We now need a way to poke and prod at <code>bColour</code> so that we can see what is going on.</p>
<p>To get something we can display, we can use <code>tag</code> to sample from it using other <code>Event</code>s:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb5-1"><a href="#cb5-1"></a><span class="ot">tag ::</span> <span class="dt">Reflex</span> t </span>
<span id="cb5-2"><a href="#cb5-2"></a>    <span class="ot">=&gt;</span> <span class="dt">Behavior</span> t a</span>
<span id="cb5-3"><a href="#cb5-3"></a>    <span class="ot">-&gt;</span> <span class="dt">Event</span> t b</span>
<span id="cb5-4"><a href="#cb5-4"></a>    <span class="ot">-&gt;</span> <span class="dt">Event</span> t a</span></code></pre></div>
<p>Here we are taking a <code>Behavior</code> and an <code>Event</code> that we’re using just to get hold of reference points of time. We massage things so that the reference <code>Event</code> fires whenever we are interested in the value of the <code>Behavior</code>. The output <code>Event</code> will fire at the same time but with the value of the <code>Behavior</code> at the times the <code>Event</code> is firing.</p>
<p>If we are viewing <code>Behavior t a</code> as being equivalent to <code>t -&gt; a</code> and <code>Event t b</code> as being equivalent to <code>[(t, b)]</code>, then we have something like</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb6-1"><a href="#cb6-1"></a><span class="ot">pseudotag ::</span> <span class="dt">Reflex</span> t </span>
<span id="cb6-2"><a href="#cb6-2"></a>          <span class="ot">=&gt;</span> (t <span class="ot">-&gt;</span> a)</span>
<span id="cb6-3"><a href="#cb6-3"></a>          <span class="ot">-&gt;</span> [(t, b)]</span>
<span id="cb6-4"><a href="#cb6-4"></a>          <span class="ot">-&gt;</span> [(t, a)]</span>
<span id="cb6-5"><a href="#cb6-5"></a>pseudotag b e <span class="ot">=</span> </span>
<span id="cb6-6"><a href="#cb6-6"></a>  <span class="fu">fmap</span> (\(t, _) <span class="ot">-&gt;</span> (t, b t)) e</span></code></pre></div>
<p>As an aside: notice that the <code>tag</code> function doesn’t have a <code>MonadHold</code> typeclass constraint. Since <code>Behavior</code>s have values at all points of time this will behave the same way - but with different values - in every frame where the input <code>Event</code> fires. We don’t need to modify the FRP network, or to read from a frame-specific value, or anything like that, which means <code>tag</code> is a pure function.</p>
<p>We can use <code>tag</code> to query the value of the <code>Behavior</code> we built up before:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb7-1"><a href="#cb7-1"></a><span class="ot">sampleBlue ::</span> (<span class="dt">Reflex</span> t, <span class="dt">MonadHold</span> t m)</span>
<span id="cb7-2"><a href="#cb7-2"></a>           <span class="ot">=&gt;</span> <span class="dt">Event</span> t <span class="dt">Colour</span> </span>
<span id="cb7-3"><a href="#cb7-3"></a>           <span class="ot">-&gt;</span> <span class="dt">Event</span> t () </span>
<span id="cb7-4"><a href="#cb7-4"></a>           <span class="ot">-&gt;</span> m (<span class="dt">Event</span> t <span class="dt">Colour</span>)</span>
<span id="cb7-5"><a href="#cb7-5"></a>sampleBlue eInput eSample <span class="ot">=</span> <span class="kw">do</span></span>
<span id="cb7-6"><a href="#cb7-6"></a>  bColour <span class="ot">&lt;-</span> hold <span class="dt">Blue</span> eInput</span>
<span id="cb7-7"><a href="#cb7-7"></a>  <span class="fu">pure</span> <span class="op">$</span> tag bColour eSample</span></code></pre></div>
Let’s have a play around with this:
<div id="basics-behaviors-sampleBlue1">

</div>
<p>If we start clicking “Sample”, we’ll see that it starts out <code>Blue</code> as we would expect. If we then start clicking on combinations of “Red”, “Blue”, and “Sample”, we’ll see that it tracks whatever the user has clicked on.</p>
<p>The state doesn’t change until the frame <em>after</em> the firing of the <code>Event</code>s in <code>hold</code>. We can see that by sampling from the <code>Behavior</code> when <em>any</em> of the buttons are pressed:</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb8-1"><a href="#cb8-1"></a><span class="ot">sampleBlue ::</span> (<span class="dt">Reflex</span> t, <span class="dt">MonadHold</span> t m)</span>
<span id="cb8-2"><a href="#cb8-2"></a>           <span class="ot">=&gt;</span> <span class="dt">Event</span> t <span class="dt">Colour</span> </span>
<span id="cb8-3"><a href="#cb8-3"></a>           <span class="ot">-&gt;</span> <span class="dt">Event</span> t () </span>
<span id="cb8-4"><a href="#cb8-4"></a>           <span class="ot">-&gt;</span> m (<span class="dt">Event</span> t <span class="dt">Colour</span>)</span>
<span id="cb8-5"><a href="#cb8-5"></a>sampleBlue eInput eSample <span class="ot">=</span> <span class="kw">do</span></span>
<span id="cb8-6"><a href="#cb8-6"></a>  bColour <span class="ot">&lt;-</span> hold <span class="dt">Blue</span> eInput</span>
<span id="cb8-7"><a href="#cb8-7"></a>  <span class="kw">let</span> eAny <span class="ot">=</span> leftmost [() <span class="op">&lt;$</span> eInput, eSample]</span>
<span id="cb8-8"><a href="#cb8-8"></a>  <span class="fu">pure</span> <span class="op">$</span> tag bColour eAny</span></code></pre></div>
<div id="basics-behaviors-sampleBlue2">

</div>
<p>This is why frames are sometimes referred to as transactions. The values of the <code>Behavior</code>s are fixed for the duration of each frame, so that every <code>Event</code> in the frame has a consistent view of the state of the <code>Behavior</code>s. Any updates to the <code>Behavior</code> that are triggered during the <code>Event</code> processing for the frame are carried out after the <code>Event</code> processing has finished, so that the new state will be available from the start of the next frame. If it weren’t for this, we’d have to worry about the order in which <code>Event</code>s were processed within a frame, and we’d be back to square one as far as state management was concerned.</p>
<p>On the topic of state management, there are some parallels with the <code>State</code> monad here. Inside of the monad, there is a value for the state at all points in time. Any modifications to the state, or reads from the state, happen at discrete points of time.</p>
<p>We’re a lot less explicit about time when working with the <code>State</code> monad, so it might take some squinting to see the parallels there. It’s also not as straightforward to compose computations working with <code>State s1 m</code> and <code>State s2 m</code> into a computation working with <code>State (s1, s2) m</code>, or to decompose a computation that works with <code>State (s1, s2) m</code> into computations that work with <code>State s1 m</code> and <code>State s2 m</code>.</p>
<p>It’s also worth mentioning the <code>MonadSample</code> class at this point, although it may not do what you expect and we won’t be using it for a little while yet.</p>
<p>It gives us the <code>sample</code> function:</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb9-1"><a href="#cb9-1"></a><span class="ot">sample ::</span> <span class="dt">MonadSample</span> t m </span>
<span id="cb9-2"><a href="#cb9-2"></a>       <span class="ot">=&gt;</span> <span class="dt">Behavior</span> t a </span>
<span id="cb9-3"><a href="#cb9-3"></a>       <span class="ot">-&gt;</span> m a</span></code></pre></div>
<p>This gives us the value of the <code>Behavior</code> in the current frame. We were able to use <code>tag</code> without a monadic context since it was reading the value of a <code>Behavior</code> in whichever frames the input <code>Event</code>s were firing in. We need a monadic context for <code>sample</code> because it is reading from the <code>Behavior</code> at the point we are up to in the construction of the FRP network.</p>
<p>Let’s take a look:</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb10-1"><a href="#cb10-1"></a><span class="ot">sampleBlue ::</span> (<span class="dt">Reflex</span> t, <span class="dt">MonadSample</span> t m, <span class="dt">MonadHold</span> t m)</span>
<span id="cb10-2"><a href="#cb10-2"></a>           <span class="ot">=&gt;</span> <span class="dt">Event</span> t <span class="dt">Colour</span></span>
<span id="cb10-3"><a href="#cb10-3"></a>           <span class="ot">-&gt;</span> <span class="dt">Event</span> t ()</span>
<span id="cb10-4"><a href="#cb10-4"></a>           <span class="ot">-&gt;</span> m (<span class="dt">Event</span> t <span class="dt">Colour</span>)</span>
<span id="cb10-5"><a href="#cb10-5"></a>sampleBlue eColour eSample <span class="ot">=</span> <span class="kw">do</span></span>
<span id="cb10-6"><a href="#cb10-6"></a>  bColour <span class="ot">&lt;-</span> hold <span class="dt">Blue</span> eColour</span>
<span id="cb10-7"><a href="#cb10-7"></a>  colour  <span class="ot">&lt;-</span> sample bColour</span>
<span id="cb10-8"><a href="#cb10-8"></a>  <span class="fu">pure</span> <span class="op">$</span> colour <span class="op">&lt;$</span> eSample</span></code></pre></div>
<p>(Although the <code>MonadSample</code> constraint is redundant here, since <code>MonadSample</code> is a superclass of <code>MonadHold</code>)</p>
If we have a click around on this, we’ll see how different it is to what we had before:
<div id="basics-behaviors-sample">

</div>
<p>At the point we called <code>sample</code>, <code>bColour</code> could only have the value <code>Blue</code>, and so that’s what we get as the output. Later on we’ll come across components with similarities to <code>hold</code>, that work with an initial pure value and an <code>Event</code> for tracking updates. If we want these things to synchronize with a <code>Behavior</code>, then <code>sample</code> is one way to grab an appropriate initial value.</p>
<p>There a few other functions for reading from <code>Behavior</code>s that are worth knowing about:</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb11-1"><a href="#cb11-1"></a><span class="ot">attach          ::</span> <span class="dt">Reflex</span> t </span>
<span id="cb11-2"><a href="#cb11-2"></a>                <span class="ot">=&gt;</span> <span class="dt">Behavior</span> t a </span>
<span id="cb11-3"><a href="#cb11-3"></a>                <span class="ot">-&gt;</span> <span class="dt">Event</span> t b </span>
<span id="cb11-4"><a href="#cb11-4"></a>                <span class="ot">-&gt;</span> <span class="dt">Event</span> t (a, b)</span>
<span id="cb11-5"><a href="#cb11-5"></a></span>
<span id="cb11-6"><a href="#cb11-6"></a><span class="ot">attachWith      ::</span> <span class="dt">Reflex</span> t </span>
<span id="cb11-7"><a href="#cb11-7"></a>                <span class="ot">=&gt;</span> (a <span class="ot">-&gt;</span> b <span class="ot">-&gt;</span> c) </span>
<span id="cb11-8"><a href="#cb11-8"></a>                <span class="ot">-&gt;</span> <span class="dt">Behavior</span> t a</span>
<span id="cb11-9"><a href="#cb11-9"></a>                <span class="ot">-&gt;</span> <span class="dt">Event</span> t b</span>
<span id="cb11-10"><a href="#cb11-10"></a>                <span class="ot">-&gt;</span> <span class="dt">Event</span> t c</span>
<span id="cb11-11"><a href="#cb11-11"></a></span>
<span id="cb11-12"><a href="#cb11-12"></a><span class="ot">attachWithMaybe ::</span> <span class="dt">Reflex</span> t</span>
<span id="cb11-13"><a href="#cb11-13"></a>                <span class="ot">=&gt;</span> (a <span class="ot">-&gt;</span> b <span class="ot">-&gt;</span> <span class="dt">Maybe</span> c) </span>
<span id="cb11-14"><a href="#cb11-14"></a>                <span class="ot">-&gt;</span> <span class="dt">Behavior</span> t a</span>
<span id="cb11-15"><a href="#cb11-15"></a>                <span class="ot">-&gt;</span> <span class="dt">Event</span> t b</span>
<span id="cb11-16"><a href="#cb11-16"></a>                <span class="ot">-&gt;</span> <span class="dt">Event</span> t c</span></code></pre></div>
<p>These are all easy to use and you can probably work out how to use them from their type signatures. We won’t be using them all that much.</p>
<p>A function that we will be using quite a bit is <code>gate</code>:</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb12-1"><a href="#cb12-1"></a><span class="ot">gate            ::</span> <span class="dt">Reflex</span> t </span>
<span id="cb12-2"><a href="#cb12-2"></a>                <span class="ot">=&gt;</span> <span class="dt">Behavior</span> t <span class="dt">Bool</span></span>
<span id="cb12-3"><a href="#cb12-3"></a>                <span class="ot">-&gt;</span> <span class="dt">Event</span> t a</span>
<span id="cb12-4"><a href="#cb12-4"></a>                <span class="ot">-&gt;</span> <span class="dt">Event</span> t a</span></code></pre></div>
It can be pretty handy to create a <code>Behavior</code> somewhere in your application:
<div id="basics-behaviors-gateOut">

</div>
and pass that value around through your application until it is used somewhere else in your application to filter <code>Event</code>s:
<div id="basics-behaviors-gateIn">

</div>
<p>This is our first demonstration of <code>Behavior</code>s as first-class values for managing state We can pass in and out of functions, we can store them in data types, we can do what we like with them.</p>
<p>This is half of what I think makes <code>Behavior</code>s exciting as a method of state management.</p>
<h2 id="interesting-instances">Interesting instances</h2>
<p>The other half comes from the typeclass instances.</p>
<p>There is a <code>Functor</code> instance:</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb13-1"><a href="#cb13-1"></a><span class="kw">instance</span> <span class="dt">Reflex</span> t <span class="ot">=&gt;</span> <span class="dt">Functor</span> (<span class="dt">Behavior</span> t) <span class="kw">where</span> <span class="op">..</span></span></code></pre></div>
<p>that can be used to transform the <code>Behavior</code> at all points of time.</p>
<p>It behaves as you would probably expect:</p>
<div class="sourceCode" id="cb14"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb14-1"><a href="#cb14-1"></a><span class="ot">sampleFlipBlue ::</span> <span class="dt">MonadHold</span> t m</span>
<span id="cb14-2"><a href="#cb14-2"></a>               <span class="ot">=&gt;</span> <span class="dt">Event</span> t <span class="dt">Colour</span></span>
<span id="cb14-3"><a href="#cb14-3"></a>               <span class="ot">-&gt;</span> <span class="dt">Event</span> t ()</span>
<span id="cb14-4"><a href="#cb14-4"></a>               <span class="ot">-&gt;</span> m (<span class="dt">Event</span> t <span class="dt">Colour</span>)</span>
<span id="cb14-5"><a href="#cb14-5"></a>sampleFlipBlue eInput eSample <span class="ot">=</span> <span class="kw">do</span></span>
<span id="cb14-6"><a href="#cb14-6"></a>  bColour <span class="ot">&lt;-</span> hold <span class="dt">Blue</span> eInput</span>
<span id="cb14-7"><a href="#cb14-7"></a>  <span class="kw">let</span> bFlippedColour <span class="ot">=</span> flipColour <span class="op">&lt;$&gt;</span> bColour</span>
<span id="cb14-8"><a href="#cb14-8"></a>  <span class="fu">pure</span> <span class="op">$</span> tag bFlippedColour eSample</span></code></pre></div>
<div id="basics-behaviors-sampleFlipBlue">

</div>
<p>The <code>Applicative</code> instances step things up a notch:</p>
<div class="sourceCode" id="cb15"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb15-1"><a href="#cb15-1"></a><span class="kw">instance</span> <span class="dt">Reflex</span> t <span class="ot">=&gt;</span> <span class="dt">Applicative</span> (<span class="dt">Behavior</span> t) <span class="kw">where</span> <span class="op">..</span></span></code></pre></div>
<p>We can use <code>pure</code> to create a constant <code>Behavior</code>:</p>
<div class="sourceCode" id="cb16"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb16-1"><a href="#cb16-1"></a><span class="ot">sampleAlwaysBlue ::</span> <span class="dt">Reflex</span> t</span>
<span id="cb16-2"><a href="#cb16-2"></a>                 <span class="ot">=&gt;</span> <span class="dt">Event</span> t <span class="dt">Colour</span></span>
<span id="cb16-3"><a href="#cb16-3"></a>                 <span class="ot">-&gt;</span> <span class="dt">Event</span> t ()</span>
<span id="cb16-4"><a href="#cb16-4"></a>                 <span class="ot">-&gt;</span> <span class="dt">Event</span> t <span class="dt">Colour</span></span>
<span id="cb16-5"><a href="#cb16-5"></a>sampleAlwaysBlue eInput eSample <span class="ot">=</span></span>
<span id="cb16-6"><a href="#cb16-6"></a>  tag (<span class="fu">pure</span> <span class="dt">Blue</span>) eSample</span></code></pre></div>
<div id="basics-behaviors-sampleAlwaysBlue">

</div>
<p>We can use <code>&lt;*&gt;</code> to compose <code>Behavior</code>s, which is almost as exciting as the fact that they are first-class values.</p>
<p>Our first brush with this idea is pretty simple:</p>
<div class="sourceCode" id="cb17"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb17-1"><a href="#cb17-1"></a><span class="ot">samplePair ::</span> (<span class="dt">Reflex</span> t, <span class="dt">MonadHold</span> t m)</span>
<span id="cb17-2"><a href="#cb17-2"></a>           <span class="ot">=&gt;</span> <span class="dt">Event</span> t <span class="dt">Colour</span></span>
<span id="cb17-3"><a href="#cb17-3"></a>           <span class="ot">-&gt;</span> <span class="dt">Event</span> t <span class="dt">Colour</span></span>
<span id="cb17-4"><a href="#cb17-4"></a>           <span class="ot">-&gt;</span> <span class="dt">Event</span> t ()</span>
<span id="cb17-5"><a href="#cb17-5"></a>           <span class="ot">-&gt;</span> m (<span class="dt">Event</span> t (<span class="dt">Colour</span>, <span class="dt">Colour</span>))</span>
<span id="cb17-6"><a href="#cb17-6"></a>samplePair eInput1 eInput2 eSample <span class="ot">=</span> <span class="kw">do</span></span>
<span id="cb17-7"><a href="#cb17-7"></a>  bColour1 <span class="ot">&lt;-</span> hold <span class="dt">Blue</span> eInput1</span>
<span id="cb17-8"><a href="#cb17-8"></a>  bColour2 <span class="ot">&lt;-</span> hold <span class="dt">Blue</span> eInput2</span>
<span id="cb17-9"><a href="#cb17-9"></a>  <span class="kw">let</span> bPair <span class="ot">=</span> (,) <span class="op">&lt;$&gt;</span> bColour1 <span class="op">&lt;*&gt;</span> bColour2</span>
<span id="cb17-10"><a href="#cb17-10"></a>  <span class="fu">pure</span> <span class="op">$</span> tag bPair eSample</span></code></pre></div>
<div id="basics-behaviors-samplePair">

</div>
<p>but we can do so much more with this idea.</p>
<p>We’ll cover some of that as we go, but as a teaser it might be worth thinking about the things you could do with something like:</p>
<div class="sourceCode" id="cb18"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb18-1"><a href="#cb18-1"></a><span class="fu">sequence</span><span class="ot"> ::</span> <span class="dt">Map</span> k (<span class="dt">Behavior</span> t v) <span class="ot">-&gt;</span> <span class="dt">Behavior</span> t (<span class="dt">Map</span> k v)</span></code></pre></div>
<p>If you had a <code>Behavior t Bool</code> that was tracking the state of every checkbox on a settings page, you could use <code>sequence</code> to gather them all into a single <code>Behavior</code>.</p>
<p>In addition to this, <code>reflex</code> wants to be your friend and so provides all kinds of other instances that you might find a use for:</p>
<div class="sourceCode" id="cb19"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb19-1"><a href="#cb19-1"></a><span class="kw">instance</span>  <span class="dt">Reflex</span> t              <span class="ot">=&gt;</span> <span class="dt">Monad</span>    (<span class="dt">Behavior</span> t  ) <span class="kw">where</span> <span class="op">...</span></span>
<span id="cb19-2"><a href="#cb19-2"></a><span class="kw">instance</span> (<span class="dt">Reflex</span> t, <span class="dt">Monoid</span> a)   <span class="ot">=&gt;</span> <span class="dt">Monoid</span>   (<span class="dt">Behavior</span> t a) <span class="kw">where</span> <span class="op">...</span></span>
<span id="cb19-3"><a href="#cb19-3"></a><span class="kw">instance</span> (<span class="dt">Reflex</span> t, <span class="dt">Num</span> a)      <span class="ot">=&gt;</span> <span class="dt">Num</span>      (<span class="dt">Behavior</span> t a) <span class="kw">where</span> <span class="op">...</span></span>
<span id="cb19-4"><a href="#cb19-4"></a><span class="kw">instance</span> (<span class="dt">Reflex</span> t, <span class="dt">IsString</span> a) <span class="ot">=&gt;</span> <span class="dt">IsString</span> (<span class="dt">Behavior</span> t a) <span class="kw">where</span> <span class="op">...</span></span></code></pre></div>
<h3 id="aside-some-handy-helpers-for-the-applicative-instance">Aside: some handy helpers for the <code>Applicative</code> instance</h3>
<p>There are a couple of operators that come from <code>reactive-banana</code>:</p>
<div class="sourceCode" id="cb20"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb20-1"><a href="#cb20-1"></a><span class="ot">(&lt;@&gt;) ::</span> <span class="dt">Reflex</span> t <span class="ot">=&gt;</span> <span class="dt">Behavior</span> t (a <span class="ot">-&gt;</span> b) <span class="ot">-&gt;</span> <span class="dt">Event</span> t a <span class="ot">-&gt;</span> <span class="dt">Event</span> t b</span>
<span id="cb20-2"><a href="#cb20-2"></a><span class="ot">(&lt;@)  ::</span> <span class="dt">Reflex</span> t <span class="ot">=&gt;</span> <span class="dt">Behavior</span> t       b  <span class="ot">-&gt;</span> <span class="dt">Event</span> t a <span class="ot">-&gt;</span> <span class="dt">Event</span> t b</span></code></pre></div>
<p>which we can use to restart the last example a little more succinctly:</p>
<div class="sourceCode" id="cb21"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb21-1"><a href="#cb21-1"></a>samplePair eInput1 eInput2 eSample <span class="ot">=</span> <span class="kw">do</span></span>
<span id="cb21-2"><a href="#cb21-2"></a>  bColour1 <span class="ot">&lt;-</span> hold <span class="dt">Blue</span> eInput1</span>
<span id="cb21-3"><a href="#cb21-3"></a>  bColour2 <span class="ot">&lt;-</span> hold <span class="dt">Blue</span> eInput2</span>
<span id="cb21-4"><a href="#cb21-4"></a>  <span class="fu">pure</span> <span class="op">$</span> (,) <span class="op">&lt;$&gt;</span> bColour1 <span class="op">&lt;*&gt;</span> bColour2 <span class="op">&lt;@</span> eSample</span></code></pre></div>
<p>These are similar to the <code>&lt;*&gt;</code>:</p>
<div class="sourceCode" id="cb22"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb22-1"><a href="#cb22-1"></a><span class="ot">(&lt;*&gt;) ::</span> <span class="dt">Applicative</span> f <span class="ot">=&gt;</span> f          (a <span class="ot">-&gt;</span> b) <span class="ot">-&gt;</span> f       a <span class="ot">-&gt;</span> f       b </span>
<span id="cb22-2"><a href="#cb22-2"></a><span class="ot">(&lt;@&gt;) ::</span> <span class="dt">Reflex</span> t      <span class="ot">=&gt;</span> <span class="dt">Behavior</span> t (a <span class="ot">-&gt;</span> b) <span class="ot">-&gt;</span> <span class="dt">Event</span> t a <span class="ot">-&gt;</span> <span class="dt">Event</span> t b</span></code></pre></div>
<p>and <code>&lt;*</code>:</p>
<div class="sourceCode" id="cb23"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb23-1"><a href="#cb23-1"></a><span class="ot">(&lt;*)  ::</span> <span class="dt">Applicative</span> f <span class="ot">=&gt;</span> f                b  <span class="ot">-&gt;</span> f       a <span class="ot">-&gt;</span> f       b</span>
<span id="cb23-2"><a href="#cb23-2"></a><span class="ot">(&lt;@)  ::</span> <span class="dt">Reflex</span> t      <span class="ot">=&gt;</span> <span class="dt">Behavior</span> t       b  <span class="ot">-&gt;</span> <span class="dt">Event</span> t a <span class="ot">-&gt;</span> <span class="dt">Event</span> t b</span></code></pre></div>
<p>operators from <code>Applicative</code>.</p>
<p>The usual pattern is to chain several <code>Behavior</code>s together with <code>&lt;*&gt;</code> and to end the chain with <code>&lt;@&gt;</code> or <code>&lt;@</code> and an <code>Event</code>.</p>
<p>If we have</p>
<div class="sourceCode" id="cb24"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb24-1"><a href="#cb24-1"></a><span class="ot">f  ::</span> a <span class="ot">-&gt;</span> b <span class="ot">-&gt;</span> c <span class="ot">-&gt;</span> d</span>
<span id="cb24-2"><a href="#cb24-2"></a><span class="ot">g  ::</span> a <span class="ot">-&gt;</span> b <span class="ot">-&gt;</span> d</span>
<span id="cb24-3"><a href="#cb24-3"></a><span class="ot">b1 ::</span> <span class="dt">Behavior</span> t a</span>
<span id="cb24-4"><a href="#cb24-4"></a><span class="ot">b2 ::</span> <span class="dt">Behavior</span> t b</span>
<span id="cb24-5"><a href="#cb24-5"></a><span class="ot">e3 ::</span> <span class="dt">Event</span> t c</span></code></pre></div>
<p>we can combine the values from the <code>Behavior</code> at the time of the <code>Event</code> along with the value of the <code>Event</code> with:</p>
<div class="sourceCode" id="cb25"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb25-1"><a href="#cb25-1"></a>f <span class="op">&lt;$&gt;</span> b1 <span class="op">&lt;*&gt;</span> b2 <span class="op">&lt;@&gt;</span><span class="ot"> e3 ::</span> <span class="dt">Event</span> t d</span></code></pre></div>
<p>or, if we don’t care about the value of the <code>Event</code> we can do:</p>
<div class="sourceCode" id="cb26"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb26-1"><a href="#cb26-1"></a>g <span class="op">&lt;$&gt;</span> b1 <span class="op">&lt;*&gt;</span> b2 <span class="op">&lt;@</span><span class="ot"> e3 ::</span> <span class="dt">Event</span> t d</span></code></pre></div>
<p>This becomes pretty handy when you have a few <code>Behavior</code>s in flight at the same time.</p>
<h2 id="playing-along-at-home">Playing along at home</h2>
<p>If you want to test out your understanding of <code>Behavior</code>s, there are <code>Behavior</code>-themed exercises <a href="../exercises/behaviors/">here</a>. The exercises continue from where the <a href="../exercises/events/"><code>Event</code> exercises</a> left off, so it’s probably worth doing them first.</p>
<h2 id="next-up">Next up</h2>
<p><code>Event</code>s and <code>Behavior</code>s are the core types of FRP.</p>
<p>The <code>reflex</code> library also has a type that combines the two together. This is done for performance reasons, but also nicely encapsulates a pattern from FRP folklore.</p>
<p>In the <a href="../dynamics/">next post</a> we’ll look at the combination of the two - <code>Dynamic</code>s.</p>
    </div>
  </div>

  
  <div class="row">
    <div class="col my-2">
      <div class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title">
      <a href="../../../../projects/reflex">Functional Reactive Programming with `reflex`</a>
    </h2>
  </div>
  <div class="panel-body">
    <p>We’re preparing educational materials about the <a href="https://github.com/reflex-frp"><code>reflex</code></a> library, and using it to see what exciting things we can do with FRP.</p>
  </div>
</div>

    </div>
  </div>
  

  
  <div class="row">
    <div class="col my-2">
      
        <div class="p-1">
  <h4 class="data61-bottom-line">
    > <a href="../../../../people/dlaing">Dave Laing</a>
  </h4>
  <p>Dave is a programmer working at the Queensland Functional Programming Lab.</p>
</div>

      
    </div>
  </div>
  
</div>

          </div>
          <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li role="presentation"><a href="../../../../">Home</a></li>
                  <li role="presentation"><a href="../../../../location">Location</a></li>
                  <li role="presentation"><a href="../../../../people">People</a></li>
                  <li role="presentation"><a href="../../../../projects">Projects</a></li>
                  <li role="presentation"><a href="../../../../talks">Talks</a></li>
                  <li role="presentation"><a href="../../../../contact">Contact</a></li>
                  <li role="presentation"><a href="../../../../archive">Archive</a></li>
              </ul>
          </div>
        </main>

        <footer class="page-footer data61-green">
            <div class="container">
              <div class="row justify-content-centre">
                <div class="col">
                  <a href="https://github.com/qfpl"><i class="text-white fa fa-github fa-2x"></i></a>
                  <a href="https://twitter.com/queenslandfplab"><i class="text-white fa fa-twitter fa-2x"></i></a>
                  <a href="mailto:contact@qfpl.io"><i class="text-white fa fa-envelope-o fa-2x"></i></a>
                </div>
                <div class="col">
                  <a class="float-right" rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
                  </a>
                </div>
              </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>

        <script src="../../../../js/bootstrap.min.js"></script>

        
          
            <script src="../../../../js/reflex/basics/reflex-basics.min.js" defer></script>
          
        
    </body>
</html>
