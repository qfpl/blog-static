<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Queensland FP Lab - An introduction to reflex</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../../../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../../../../css/custom.css">
        <link rel="stylesheet" href="../../../../css/syntax.css">
        
          
        
        <link rel="alternate" type="application/atom+xml" title="Queensland Functional Programming Lab Blog" href="../../../../atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Queensland Functional Programming Lab Blog" href="../../../../rss.xml">
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106321204-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments)};
          gtag('js', new Date());

          gtag('config', 'UA-106321204-1', { 'anonymize_ip': true });
        </script>
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg data61-green">
              <a class="navbar-brand text-white" href="../../../../">
                <div class="card brand-logo">
                  <div class="card-body">
                    <div class="brand-anim-block">
                      <img id="qfpl-brand-data61-logo" height="55" alt="Data61 logo" src="../../../../images/DATA61-CSIRO_MONO.png" />
                    </div>
                  </div>
                </div>
              </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <i class="fa fa-lg fa-fw fa-bars text-white"></i>
              </button>

              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../../">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../../location">Location</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../../people">People</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../../projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../../talks">Talks</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../../contact">Contact</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../../archive">Archive</a>
                    </li>
                  </ul>
                </div>
            </nav>
        </header>

        <main>
          <div class="container-fluid">
              <div class="container">
  <div class="row">
    <div class="col my-2">
      <h2>An introduction to reflex</h2>
      <small>Posted on September 18, 2017</small>
    </div>
  </div>

  <div class="row">
    <div class="col my-2">
      <p>This is a series of posts aimed at getting people started with Functional Reactive Programming using the <code>reflex</code> library.</p>
<p>It assumes that you’re comfortable with the Haskell language, up to and including the <code>Applicative</code> and <code>Monad</code> typeclasses. If you’re not there yet, I usually recommend the <a href="http://www.seas.upenn.edu/~cis194/spring13/lectures.html">CIS194 course</a>. Some of us relayed these lectures to our local meetup group, so we have you covered if you have a preference for <a href="https://github.com/bfpg/cis194-yorgey-lectures">videos of lectures</a>. I’ve also heard glowing praise for <a href="http://haskellbook.com/">The Haskell Book</a>.</p>
<p>There is also a book on <a href="https://www.manning.com/books/functional-reactive-programming">Functional Reactive Programming</a> by Stephen Blackheath and Anthony Jones, which I also highly recommend. It does a great job of motivating and explaining FRP. It uses the <code>sodium</code> library, which has bindings for a lot of languages, and most of the examples in the book are in Java.</p>
<h2 id="what-is-functional-reactive-programming-frp">What is Functional Reactive Programming (FRP)?</h2>
<p>There has been a lot of interest in FRP over the last few years, and there are now a lot of different meanings of FRP that are in use.</p>
<p>My claim is that the FRP implementations that are closer to the original idea are much more useful and powerful than the others. We’ll have to go a little way down the rabbit hole with the ideas and concepts before I can make a solid case for that.</p>
<p>The original definition of FRP had:</p>
<ul>
<li><code>Event</code> and <code>Behavior</code> types</li>
</ul>
<p>and required:</p>
<ul>
<li>clear denotational semantics</li>
<li>support for continuous time</li>
</ul>
<p>The <code>Event</code> and <code>Behavior</code> types are at the heart of these systems. The <code>Event</code> values model changes that happen at single points of time, like a user clicking a button or a timeout expiring. The <code>Behavior</code> values model state that have values at all points of time, like the position of a mouse or whether a user has admin permissions on a site. The <code>Behavior</code>s are created, changed, and queried by <code>Event</code>s, and they have an <code>Applicative</code> instance so that we can combine them.</p>
<p>This leads to a very different way of dealing with state than most people are used to. It’s very powerful, as we’ll soon see.</p>
<p>The implementations that I prefer usually:</p>
<ul>
<li>have <code>Event</code> and <code>Behavior</code> types</li>
<li>have clear denotational semantics</li>
<li>have the ability to do higher-order FRP, so that the network of <code>Event</code>s and <code>Behavior</code>s can be changed over time.</li>
<li>have come up with APIs to greatly reduce the risk of introducing space or time leaks</li>
</ul>
<p>These systems give you first-class values that model mutable state, with a built-in and souped-up version of observers.</p>
<p>The implementations that I have used provide discrete-time FRP rather than continuous-time FRP. The difference is similar to the difference between a digital and analog circuit. Discrete-time FRP uses <code>Event</code>s as the clock that drive the circuit, where continuous-time FRP does not. This means that with continuous-time FRP we can manipulate sampling and update rates explicitly, which is very handy when you are working with multimedia. It also leads to much simpler denotational semantics than the discrete-time FRP systems have</p>
<p>While this means there are some thoughts we can’t express in discrete-time FRP system, I’ve been finding them very useful nonetheless. If someone has a library that supports continuous-time FRP as well as the above points, I’d be very keen to take a look so I can discover what I’ve been missing out on.</p>
<p>The other interesting thing about the implementations that I tend to like is that they have a kind of phase separation. The API that is presented to the user is typically building up a data structure that describes a FRP network and how it will change over time. Given that, the library will effectively compile this into an efficient graph of switches and latches, along with all of the memory management required to have it play nicely with the garbage collector of their chosen language. This means there are opportunities to increase the performance or decrease the memory usage of the system while having relative stable user facing APIs. I’m excited about where that might lead.</p>
<p>Documentation is probably the biggest gap with these systems at the moment, although I have a lot of things I’d like to write which I hope will help on that front.</p>
<h2 id="what-is-reflex">What is <code>reflex</code>?</h2>
<p>The <code>reflex</code> library is an implementation of an FRP library that satisfies all of the preferences that I listed above. It is also highly performant and has had some great work done around its usability.</p>
<p>You can use <code>reflex</code> to write code via either GHC and GHCJS, although there has been a focus on using it to create web front-ends via GHCJS and the <code>reflex-dom</code> companion library.</p>
<p>There is support for writing new back-ends as well, and so you can use it in other places where having a better model of mutable state might be handy, such as:</p>
<ul>
<li>command line applications</li>
<li>servers</li>
<li>SDL applications</li>
</ul>
<p>It is worth noting that the <code>reflex</code> implementation has been written so that it could be extended to support continuous-time FRP. My understanding is that there haven’t been any pressing use cases presented to motivate that work.</p>
<p>We’ll take a look at that support later on, but for now we’ll be focusing on using <code>reflex</code> to create web front-ends, where it is used to create and manipulate the DOM. The approach that <code>reflex</code> and <code>reflex-dom</code> uses here is different enough from the currently popular approaches that it’s worth saying a little bit about those before we continue.</p>
<h3 id="aside-what-is-the-virtual-dom">Aside: What is the virtual DOM?</h3>
<p>Inside the browser, the Document Object Model (DOM) is a mutable tree with mutable state at the branches and at the leaves. That doesn’t sound ideal to a lot of folks. It can also be slow to search and to modify.</p>
<p>To work around that, some clever people worked out that they could build a data structure to model the DOM - the virtual or shadow DOM - and operate on that instead. The trick to doing that is to have efficient procedures to find differences between DOMs and to patch them, so that you can translate changes in the virtual DOM into changes in the actual DOM while minimizing the amount of work done.</p>
<p>The <code>react</code> library and many other libraries use this idea, and lots of people are quite enthused by it. It seems a bit like a functional approach to the problem, which is great. If you’re struggling to deal with a mutable tree with mutable state at the branches and at the leaves, you’ll reach for whatever seems like it’ll work.</p>
<p>Some folks have tried to use an even more functional approach, and work with the DOM like they are working with a pure function:</p>
<pre><code>DOM -&gt; DOM</code></pre>
<p>The problem there is that it is built on the assumption that the DOM is stateless. That assumption is incorrect. We can look at a text input to see pieces of state that aren’t captured in the DOM, like the text in the input and the position of the cursor.</p>
<p>There are two ways libraries tend to work with those kind of inputs.</p>
<p>You can have local state, like <code>react</code> does. This means you have a mutable tree with mutable state at the branches and at the leaves, but the tree has a different type and granularity to what you had with the DOM.</p>
<p>The alternative is to extract all of the state that you are interested in from the DOM tree, manage and change it, and then use it to restore the state during the next update to the DOM tree. This gives you another mutable tree with mutable state at the branches and at the leaves which you layer over your DOM in your event loop.</p>
<p>This might be a big global object, or you might be able to scope it a little more tightly to where you are using the state. You can see approaches like these with <code>redux</code> and with <code>halogen</code>.</p>
<p>My claim is that once you’re comfortable with <code>reflex</code>:</p>
<ul>
<li>you’ll be able to work with mutable trees with mutable state at the branches and at the leaves, in a functional way, with tools that allow your code and your reasoning about the code to compose well</li>
<li>you’ll be able to make the same alterations to the DOM that the virtual DOM approach does, but without having to do the diffing or patching steps</li>
</ul>
<p>In the Haskell community we periodically hear from people who work with non-functional programming languages and have dived into Haskell, and start to like their work language much less afterwards. There’s a bit of a risk of something happening if you’re working with some of the other popular libraries and frameworks, but if you have enough Haskell under your belt for this series you have probably accepted and dealt with that risk already.</p>
<h2 id="getting-started">Getting started</h2>
<p>The easiest way to install the <code>reflex</code> library is via the <code>reflex-platform</code>:</p>
<pre><code>&gt; git clone https://github.com/reflex-frp/reflex-platform
&gt; cd reflex-platform
&gt; ./try-reflex</code></pre>
<p>although you should make sure to read the OS compatibility notes in the <code>reflex-platform</code> README if they apply to you.</p>
<p>The <code>reflex-platform</code> uses Nix behind the scenes to set things up, so we don’t have to jump through any hoops to set up GHCJS and things like that. The <code>./try-reflex</code> script should install Nix and setup the binary caches for you, which will save you lots of time (although it may not feel like it the first time this runs).</p>
<p>For the most part we don’t need to know any Nix to make use of this, because <code>reflex-platform</code> comes with some handy shell scripts.</p>
<p>If we have a <code>cabal</code> based project lying around that we want to compile with GHCJS, we can use:</p>
<pre><code>&gt; cd my-cabal-project
&gt; PATH_TO_REFLEX_PLATFORM/work-on ghcjs ./.
&gt; cabal configure --ghcjs</code></pre>
<p>and if we want to compile it with GHC:</p>
<pre><code>&gt; cd my-cabal-project
&gt; PATH_TO_REFLEX_PLATFORM/work-on ghc ./.
&gt; cabal configure</code></pre>
<p>We’ll look at using Nix to do much fancier things later on in this series.</p>
<p>If you want to test out this setup and some associated tooling, we have an <a href="../exercises/introduction/">exercise</a> for just that purpose.</p>
<h2 id="acknowledgements">Acknowledgements</h2>
<p>This series had a dramatic increase in quality after I shared it around with some folks and asked for some feedback.</p>
<p>George Wilson and Sean Chalmers from QFPL and Jack Kelly from Data61 helped a lot with the proof reading.</p>
<p>I asked the #reflex-frp IRC channel on Freenode if anyone would like to provide feedback, and Isto Aho answered the call in a big way. He provided help of high quantity and quality; with proof reading, with suggestions for improving the flow of the writing, with suggestions for the higher level narrative.</p>
<p>Ryan Trinkle, Cale Gibbard, Doug Beardsley and many others have also helped me to understand much more of <code>reflex</code> from questions answered and discussions had in that room.</p>
<p>The lesson is: don’t hesitate to ask for help on #reflex-frp.</p>
<h2 id="next-up">Next up</h2>
<p>As I’ve mentioned, there are two main types at the heart of the more powerful FRP libraries: <code>Event</code>s and <code>Behavior</code>s.</p>
<p>In the <a href="../events/">next post</a> we will start by looking at <code>Event</code>s.</p>
    </div>
  </div>

  
  <div class="row">
    <div class="col my-2">
      <div class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title">
      <a href="../../../../projects/reflex">Functional Reactive Programming with `reflex`</a>
    </h2>
  </div>
  <div class="panel-body">
    <p>We’re preparing educational materials about the <a href="https://github.com/reflex-frp"><code>reflex</code></a> library, and using it to see what exciting things we can do with FRP.</p>
  </div>
</div>

    </div>
  </div>
  

  
  <div class="row">
    <div class="col my-2">
      
        <div class="p-1">
  <h4 class="data61-bottom-line">
    > <a href="../../../../people/dlaing">Dave Laing</a>
  </h4>
  <p>Dave is a programmer working at the Queensland Functional Programming Lab.</p>
</div>

      
    </div>
  </div>
  
</div>

          </div>
          <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li role="presentation"><a href="../../../../">Home</a></li>
                  <li role="presentation"><a href="../../../../location">Location</a></li>
                  <li role="presentation"><a href="../../../../people">People</a></li>
                  <li role="presentation"><a href="../../../../projects">Projects</a></li>
                  <li role="presentation"><a href="../../../../talks">Talks</a></li>
                  <li role="presentation"><a href="../../../../contact">Contact</a></li>
                  <li role="presentation"><a href="../../../../archive">Archive</a></li>
              </ul>
          </div>
        </main>

        <footer class="page-footer data61-green">
            <div class="container">
              <div class="row justify-content-centre">
                <div class="col">
                  <a href="https://github.com/qfpl"><i class="text-white fa fa-github fa-2x"></i></a>
                  <a href="https://twitter.com/queenslandfplab"><i class="text-white fa fa-twitter fa-2x"></i></a>
                  <a href="mailto:contact@qfpl.io"><i class="text-white fa fa-envelope-o fa-2x"></i></a>
                </div>
                <div class="col">
                  <a class="float-right" rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
                  </a>
                </div>
              </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>

        <script src="../../../../js/bootstrap.min.js"></script>

        
          
        
    </body>
</html>
