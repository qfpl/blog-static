<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Queensland FP Lab - Optimising free monad programs using Plated</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../../css/custom.css">
        <link rel="stylesheet" href="../../css/syntax.css">
        
          
        
        <link rel="alternate" type="application/atom+xml" title="Queensland Functional Programming Lab Blog" href="../../atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Queensland Functional Programming Lab Blog" href="../../rss.xml">
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106321204-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments)};
          gtag('js', new Date());

          gtag('config', 'UA-106321204-1', { 'anonymize_ip': true });
        </script>
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg data61-green">
              <a class="navbar-brand text-white" href="../../">
                <div class="card brand-logo">
                  <div class="card-body">
                    <div class="brand-anim-block">
                      <img id="qfpl-brand-data61-logo" height="55" alt="Data61 logo" src="../../images/DATA61-CSIRO_MONO.png" />
                    </div>
                  </div>
                </div>
              </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <i class="fa fa-lg fa-fw fa-bars text-white"></i>
              </button>

              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../location">Location</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../people">People</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../talks">Talks</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../contact">Contact</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../archive">Archive</a>
                    </li>
                  </ul>
                </div>
            </nav>
        </header>

        <main>
          <div class="container-fluid">
              <div class="container">
  <div class="row">
    <div class="col my-2">
      <h2>Optimising free monad programs using Plated</h2>
      <small>Posted on October 26, 2017</small>
    </div>
  </div>

  <div class="row">
    <div class="col my-2">
      <p>This document is a Literate Haskell file, available <a href="https://github.com/qfpl/blog/blob/master/content/posts/optimising-free-with-plated.lhs">here</a></p>
<hr />
<p>In this article I demonstrate how to use classy prisms and <code>Plated</code> to write and apply optimisations to programs written in a free monad DSL.</p>
<p><a href="https://hackage.haskell.org/package/lens-4.15.4/docs/Control-Lens-Plated.html#t:Plated">Plated</a> is a class in <a href="https://hackage.haskell.org/package/lens">lens</a> that provides powerful tools to work with self-recursive data structures. One such tool is <a href="https://hackage.haskell.org/package/lens-4.15.4/docs/Control-Lens-Plated.html#v:rewrite">recursive bottom-up rewriting</a>, which repeatedly applies a transformation everywhere in a <code>Plated</code> structure until it can no longer be applied.</p>
<p><a href="https://hackage.haskell.org/package/free-4.12.4/docs/Control-Monad-Free.html#t:Free">The free monad</a> has an instance of <code>Plated</code>:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb1-1"><a href="#cb1-1"></a><span class="dt">Traversable</span> f <span class="ot">=&gt;</span> <span class="dt">Plated</span> (<span class="dt">Free</span> f a)</span></code></pre></div>
<p>so if you derive <code>Foldable</code> and <code>Traversable</code> for the underlying functor, you can use the <code>Plated</code> combinators on your free monad DSL.</p>
<p>Defining <a href="https://hackage.haskell.org/package/lens-4.15.4/docs/Control-Lens-TH.html#v:makeClassyPrisms">classy prisms</a> for the base functor provides pattern matching on free monad programs for free. When coupled with <code>rewrite</code>, we get a system for applying optimisations to our free monad programs with minimal effort.</p>
<hr />
<p>Let’s get into it.</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb2-1"><a href="#cb2-1"></a><span class="ot">{-# language DeriveFunctor #-}</span></span>
<span id="cb2-2"><a href="#cb2-2"></a><span class="ot">{-# language DeriveFoldable #-}</span></span>
<span id="cb2-3"><a href="#cb2-3"></a><span class="ot">{-# language DeriveTraversable #-}</span></span>
<span id="cb2-4"><a href="#cb2-4"></a><span class="ot">{-# language FlexibleInstances #-}</span></span>
<span id="cb2-5"><a href="#cb2-5"></a><span class="ot">{-# language FunctionalDependencies #-}</span></span>
<span id="cb2-6"><a href="#cb2-6"></a><span class="ot">{-# language MultiParamTypeClasses #-}</span></span>
<span id="cb2-7"><a href="#cb2-7"></a><span class="ot">{-# language TemplateHaskell #-}</span></span>
<span id="cb2-8"><a href="#cb2-8"></a></span>
<span id="cb2-9"><a href="#cb2-9"></a><span class="kw">module</span> <span class="dt">FreePlated</span> <span class="kw">where</span></span>
<span id="cb2-10"><a href="#cb2-10"></a></span>
<span id="cb2-11"><a href="#cb2-11"></a><span class="kw">import</span> <span class="dt">Control.Lens.Fold</span>   ((^?))</span>
<span id="cb2-12"><a href="#cb2-12"></a><span class="kw">import</span> <span class="dt">Control.Lens.Plated</span> (<span class="dt">Plated</span>, rewrite)</span>
<span id="cb2-13"><a href="#cb2-13"></a><span class="kw">import</span> <span class="dt">Control.Lens.Prism</span>  (aside)</span>
<span id="cb2-14"><a href="#cb2-14"></a><span class="kw">import</span> <span class="dt">Control.Lens.Review</span> ((#))</span>
<span id="cb2-15"><a href="#cb2-15"></a><span class="kw">import</span> <span class="dt">Control.Lens.TH</span>     (makeClassyPrisms)</span>
<span id="cb2-16"><a href="#cb2-16"></a><span class="kw">import</span> <span class="dt">Control.Monad.Free</span>  (<span class="dt">Free</span>, liftF, _<span class="dt">Free</span>)</span>
<span id="cb2-17"><a href="#cb2-17"></a><span class="kw">import</span> <span class="dt">Data.Monoid</span>         (<span class="dt">First</span>(..))</span></code></pre></div>
<p> </p>
<p>First, define the DSL. This is a bit of a contrived one:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb3-1"><a href="#cb3-1"></a><span class="kw">data</span> <span class="dt">InstF</span> a</span>
<span id="cb3-2"><a href="#cb3-2"></a>  <span class="ot">=</span> <span class="dt">One</span> a</span>
<span id="cb3-3"><a href="#cb3-3"></a>  <span class="op">|</span> <span class="dt">Many</span> <span class="dt">Int</span> a</span>
<span id="cb3-4"><a href="#cb3-4"></a>  <span class="op">|</span> <span class="dt">Pause</span> a</span>
<span id="cb3-5"><a href="#cb3-5"></a>  <span class="kw">deriving</span> (<span class="dt">Functor</span>, <span class="dt">Foldable</span>, <span class="dt">Traversable</span>, <span class="dt">Eq</span>, <span class="dt">Show</span>)</span>
<span id="cb3-6"><a href="#cb3-6"></a></span>
<span id="cb3-7"><a href="#cb3-7"></a><span class="kw">type</span> <span class="dt">Inst</span> <span class="ot">=</span> <span class="dt">Free</span> <span class="dt">InstF</span></span>
<span id="cb3-8"><a href="#cb3-8"></a></span>
<span id="cb3-9"><a href="#cb3-9"></a><span class="ot">one ::</span> <span class="dt">Inst</span> ()</span>
<span id="cb3-10"><a href="#cb3-10"></a>one <span class="ot">=</span> liftF <span class="op">$</span> <span class="dt">One</span> ()</span>
<span id="cb3-11"><a href="#cb3-11"></a></span>
<span id="cb3-12"><a href="#cb3-12"></a><span class="ot">many ::</span> <span class="dt">Int</span> <span class="ot">-&gt;</span> <span class="dt">Inst</span> ()</span>
<span id="cb3-13"><a href="#cb3-13"></a>many n <span class="ot">=</span> liftF <span class="op">$</span> <span class="dt">Many</span> n ()</span>
<span id="cb3-14"><a href="#cb3-14"></a></span>
<span id="cb3-15"><a href="#cb3-15"></a><span class="ot">pause ::</span> <span class="dt">Inst</span> ()</span>
<span id="cb3-16"><a href="#cb3-16"></a>pause <span class="ot">=</span> liftF <span class="op">$</span> <span class="dt">Pause</span> ()</span></code></pre></div>
<ul>
<li><code>one</code> - “do something once”</li>
<li><code>many n</code> - “do something <code>n</code> times”</li>
<li><code>pause</code> - “take a break”</li>
</ul>
<p>In this DSL, we are going impose the property that <code>many n</code> should be equivalent to <code>replicateM_ n one</code>.</p>
<p> </p>
<p>Next, generate classy prisms for the functor.</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb4-1"><a href="#cb4-1"></a>makeClassyPrisms '<span class="dt">'InstF</span></span></code></pre></div>
<p><code>makeClassyPrisms ''InstF</code> generates the following prisms:</p>
<ul>
<li><div class="sourceCode" id="cb5"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb5-1"><a href="#cb5-1"></a><span class="ot">_InstF ::</span> <span class="dt">AsInstF</span> s a <span class="ot">=&gt;</span> <span class="dt">Prism'</span> s (<span class="dt">InstF</span> a)<span class="ot">`</span></span></code></pre></div></li>
<li><div class="sourceCode" id="cb6"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb6-1"><a href="#cb6-1"></a><span class="ot">_One ::</span> <span class="dt">AsInstF</span> s a <span class="ot">=&gt;</span> <span class="dt">Prism'</span> s a</span></code></pre></div></li>
<li><div class="sourceCode" id="cb7"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb7-1"><a href="#cb7-1"></a><span class="ot">_Many ::</span> <span class="dt">AsInstF</span> s a <span class="ot">=&gt;</span> <span class="dt">Prism'</span> s (<span class="dt">Int</span>, a)<span class="ot">`</span></span></code></pre></div></li>
<li><div class="sourceCode" id="cb8"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb8-1"><a href="#cb8-1"></a><span class="ot">_Pause ::</span> <span class="dt">AsInstF</span> s a <span class="ot">=&gt;</span> <span class="dt">Prism'</span> s a<span class="ot">`</span></span></code></pre></div></li>
</ul>
<p> </p>
<p>Lift the classy prisms into the free monad:</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb9-1"><a href="#cb9-1"></a><span class="kw">instance</span> <span class="dt">AsInstF</span> (<span class="dt">Inst</span> a) (<span class="dt">Inst</span> a) <span class="kw">where</span></span>
<span id="cb9-2"><a href="#cb9-2"></a>  _InstF <span class="ot">=</span> _Free</span></code></pre></div>
<p>We can now use the prisms as if they had these types:</p>
<ul>
<li><div class="sourceCode" id="cb10"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb10-1"><a href="#cb10-1"></a><span class="ot">_One ::</span> <span class="dt">Prism'</span> (<span class="dt">Inst</span> a) (<span class="dt">Inst</span> a)</span></code></pre></div></li>
<li><div class="sourceCode" id="cb11"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb11-1"><a href="#cb11-1"></a><span class="ot">_Many ::</span> <span class="dt">Prism'</span> (<span class="dt">Inst</span> a) (<span class="dt">Int</span>, <span class="dt">Inst</span> a)</span></code></pre></div></li>
<li><div class="sourceCode" id="cb12"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb12-1"><a href="#cb12-1"></a><span class="ot">_Pause ::</span> <span class="dt">Prism'</span> (<span class="dt">Inst</span> a) (<span class="dt">Inst</span> a)</span></code></pre></div></li>
</ul>
<p>If one of these prisms match, it means the program begins with that particular instruction, and the <code>Inst a</code> returned is the tail of the program.</p>
<p> </p>
<p>Now it’s time to write optimisations over the free monad structure. A rewrite rule has the type <code>a -&gt; Maybe a</code>- if the function returns a <code>Just</code>, the input will be replaced with the contents of the <code>Just</code>. If it returns <code>Nothing</code> then no rewriting will occur.</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb13-1"><a href="#cb13-1"></a><span class="ot">optimisations ::</span> <span class="dt">AsInstF</span> s s <span class="ot">=&gt;</span> [s <span class="ot">-&gt;</span> <span class="dt">Maybe</span> s]</span>
<span id="cb13-2"><a href="#cb13-2"></a>optimisations <span class="ot">=</span> [onesToMany, oneAndMany, manyAndOne]</span>
<span id="cb13-3"><a href="#cb13-3"></a>  <span class="kw">where</span></span></code></pre></div>
<p>Rule 1: <code>one</code> followed by <code>one</code> is equivalent to <code>many 2</code></p>
<div class="sourceCode" id="cb14"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb14-1"><a href="#cb14-1"></a>    onesToMany s <span class="ot">=</span> <span class="kw">do</span></span>
<span id="cb14-2"><a href="#cb14-2"></a>      s' <span class="ot">&lt;-</span> s <span class="op">^?</span> _One<span class="op">.</span>_One</span>
<span id="cb14-3"><a href="#cb14-3"></a>      <span class="fu">pure</span> <span class="op">$</span> _Many <span class="op">#</span> (<span class="dv">2</span>, s')</span></code></pre></div>
<p>Rule 2: <code>one</code> followed by <code>many n</code> is equivalent to <code>many (n+1)</code></p>
<div class="sourceCode" id="cb15"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb15-1"><a href="#cb15-1"></a>    oneAndMany s <span class="ot">=</span> <span class="kw">do</span></span>
<span id="cb15-2"><a href="#cb15-2"></a>      (n, s') <span class="ot">&lt;-</span> s <span class="op">^?</span> _One<span class="op">.</span>_Many</span>
<span id="cb15-3"><a href="#cb15-3"></a>      <span class="fu">pure</span> <span class="op">$</span> _Many <span class="op">#</span> (n<span class="op">+</span><span class="dv">1</span>, s')</span></code></pre></div>
<p>Rule 3: <code>many n</code> followed by <code>one</code> is equivalent to <code>many (n+1)</code></p>
<div class="sourceCode" id="cb16"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb16-1"><a href="#cb16-1"></a>    manyAndOne s <span class="ot">=</span> <span class="kw">do</span></span>
<span id="cb16-2"><a href="#cb16-2"></a>      (n, s') <span class="ot">&lt;-</span> s <span class="op">^?</span> _Many<span class="op">.</span>aside _One</span>
<span id="cb16-3"><a href="#cb16-3"></a>      <span class="fu">pure</span> <span class="op">$</span> _Many <span class="op">#</span> (n<span class="op">+</span><span class="dv">1</span>, s')</span></code></pre></div>
<p> </p>
<p>The last step is to write a function that applies all the optimisations to a program.</p>
<div class="sourceCode" id="cb17"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb17-1"><a href="#cb17-1"></a><span class="ot">optimise ::</span> (<span class="dt">Plated</span> s, <span class="dt">AsInstF</span> s s) <span class="ot">=&gt;</span> s <span class="ot">-&gt;</span> s</span>
<span id="cb17-2"><a href="#cb17-2"></a>optimise <span class="ot">=</span> rewrite <span class="op">$</span> getFirst <span class="op">.</span> <span class="fu">foldMap</span> (<span class="dt">First</span> <span class="op">.</span>) optimisations</span></code></pre></div>
<p><code>getFirst . foldMap (First .)</code> has type <code>[a -&gt; Maybe a] -&gt; a -&gt; Maybe a</code>. It combines all the rewrite rules into a single rule that picks the first rule to succeed for the input.</p>
<p> </p>
<p>Now we can optimise a program:</p>
<div class="sourceCode" id="cb18"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb18-1"><a href="#cb18-1"></a>program <span class="ot">=</span> <span class="kw">do</span></span>
<span id="cb18-2"><a href="#cb18-2"></a>  one</span>
<span id="cb18-3"><a href="#cb18-3"></a>  one</span>
<span id="cb18-4"><a href="#cb18-4"></a>  one</span>
<span id="cb18-5"><a href="#cb18-5"></a>  pause</span>
<span id="cb18-6"><a href="#cb18-6"></a>  one</span>
<span id="cb18-7"><a href="#cb18-7"></a>  one</span>
<span id="cb18-8"><a href="#cb18-8"></a>  many <span class="dv">3</span></span>
<span id="cb18-9"><a href="#cb18-9"></a>  one</span></code></pre></div>
<p>The <code>one</code>s before the <code>pause</code> should collapse into <code>many 3</code>, and the instructions after the <code>pause</code> should collapse into <code>many 6</code>.</p>
<pre><code>ghci&gt; optimise program == (many 3 *&gt; pause *&gt; many 6)
True</code></pre>
<p>:)</p>
    </div>
  </div>

  
  <div class="row">
    <div class="col my-2">
      
    </div>
  </div>
  

  
  <div class="row">
    <div class="col my-2">
      
        <div class="p-1">
  <h4 class="data61-bottom-line">
    > <a href="../../people/ielliott">Isaac Elliott</a>
  </h4>
  <p>Isaac <em>really</em> likes types</p>
</div>

      
    </div>
  </div>
  
</div>

          </div>
          <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li role="presentation"><a href="../../">Home</a></li>
                  <li role="presentation"><a href="../../location">Location</a></li>
                  <li role="presentation"><a href="../../people">People</a></li>
                  <li role="presentation"><a href="../../projects">Projects</a></li>
                  <li role="presentation"><a href="../../talks">Talks</a></li>
                  <li role="presentation"><a href="../../contact">Contact</a></li>
                  <li role="presentation"><a href="../../archive">Archive</a></li>
              </ul>
          </div>
        </main>

        <footer class="page-footer data61-green">
            <div class="container">
              <div class="row justify-content-centre">
                <div class="col">
                  <a href="https://github.com/qfpl"><i class="text-white fa fa-github fa-2x"></i></a>
                  <a href="https://twitter.com/queenslandfplab"><i class="text-white fa fa-twitter fa-2x"></i></a>
                  <a href="mailto:contact@qfpl.io"><i class="text-white fa fa-envelope-o fa-2x"></i></a>
                </div>
                <div class="col">
                  <a class="float-right" rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
                  </a>
                </div>
              </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>

        <script src="../../js/bootstrap.min.js"></script>

        
          
        
    </body>
</html>
