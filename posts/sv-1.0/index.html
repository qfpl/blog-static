<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Queensland FP Lab - Announcing sv 1.0</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../../css/custom.css">
        <link rel="stylesheet" href="../../css/syntax.css">
        
          
        
        <link rel="alternate" type="application/atom+xml" title="Queensland Functional Programming Lab Blog" href="../../atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Queensland Functional Programming Lab Blog" href="../../rss.xml">
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106321204-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments)};
          gtag('js', new Date());

          gtag('config', 'UA-106321204-1', { 'anonymize_ip': true });
        </script>
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg data61-green">
              <a class="navbar-brand text-white" href="../../">
                <div class="card brand-logo">
                  <div class="card-body">
                    <div class="brand-anim-block">
                      <img id="qfpl-brand-data61-logo" height="55" alt="Data61 logo" src="../../images/DATA61-CSIRO_MONO.png" />
                    </div>
                  </div>
                </div>
              </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <i class="fa fa-lg fa-fw fa-bars text-white"></i>
              </button>

              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../location">Location</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../people">People</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../talks">Talks</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../contact">Contact</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../archive">Archive</a>
                    </li>
                  </ul>
                </div>
            </nav>
        </header>

        <main>
          <div class="container-fluid">
              <div class="container">
  <div class="row">
    <div class="col my-2">
      <h2>Announcing sv 1.0</h2>
      <small>Posted on July 20, 2018</small>
    </div>
  </div>

  <div class="row">
    <div class="col my-2">
      <p><code>sv</code>, a CSV library for Haskell by the Queensland Functional Programming Lab, was released over four months ago. Since then, we’ve had feedback on what potential users want from the library, and we have altered the design to the point where we are now releasing a new version: <code>sv 1.0</code></p>
<h2 id="the-story-thus-far">The Story Thus Far</h2>
<p>With <code>sv</code>, QFPL wanted a library that could parse a CSV file and build a data structure which preserved all syntactic information about the input document. This allowed users to manipulate the document using lenses and output a new file that was minimally different from the input. With this, a user could write custom sanitisation tools and linters for their data.</p>
<p>We wrote such a data structure, and gave it a parser with the <a href="https://hackage.haskell.org/package/parsers">parsers</a> package. <code>parsers</code> abstracts over the common parsing primitives and combinators, allowing someone to instantiate a <code>parsers</code> parser to other parsing libraries like <code>attoparsec</code>, <code>trifecta</code>, or <code>parsec</code>. <code>sv</code> exposed its parser so that a user could use it as a component of some larger parser. Since it used <code>parsers</code>, our parser was likely to work with whatever parser library the user is already using.</p>
<p><code>sv</code> was not only for writing custom linters and sanitisation tools. It was also for writing decoders to extract values from CSV documents into Haskell programs. Importantly, <code>sv</code> did not use type classes for this, but rather an Applicative combinator-based approach. Users could pass around and manipulate decoders as values, and create multiple decoders of each type.</p>
<p>All was not well. The performance of <code>sv</code>’s parser was <a href="https://github.com/haskell-perf/csv/tree/25493c61733f6b2b69a4378313af2801f1cceb3b">very slow</a>, mainly due to extravagant memory usage. There was also <a href="https://github.com/qfpl/sv/issues/10">keen interest</a> in streaming support, meaning to parse and decode a file without keeping the whole thing in memory at once. This appeared very difficult to integrate into <code>sv</code>’s design without significant changes to its syntax tree.</p>
<h2 id="the-times-they-are-a-changin">The Times They Are a-Changin’</h2>
<p>At YOW! Lambda Jam 2018 in Sydney, I (George Wilson) gave <a href="https://www.youtube.com/watch?v=zP-DgjIcCho">a talk</a> about the design of <code>sv</code>. This prompted many useful conversations about <code>sv</code> throughout and following the rest of the conference. The key insight was this: <code>sv</code>’s two broad goals were at odds with each other. From a decoding library, users demand speed and streaming support. By giving <code>sv</code> a syntax-preserving representation and making its parser as generic as possible, it can also serve as a toolkit for building custom linting and sanitisation tools, but it is much harder to provide decoding users the speed they crave. I have decided that a sensible way forward is to split up <code>sv</code> into a handful of libraries. The decoding will be paired with a parser of much higher performance that does not keep all syntax information. Syntax-preserving manipulation will be available separately and for now will retain its performance problems.</p>
<p>Edward Kmett suggested I speak to John Ky about John’s high performance CSV parser, which has now been released to hackage as <a href="https://hackage.haskell.org/package/hw-dsv"><code>hw-dsv</code></a>. This library uses rank-select data structures to index into CSV documents and offers both a strict and a lazy (streaming) interface. After Edward explained succinct data structures to me, and after further conversations with John, I was very keen to play around with this library as a new parser for <code>sv</code>. And that is what I have done. <code>sv</code>’s decoding layer now sits atop <code>hw-dsv</code>.</p>
<p>The release of <code>sv 1.0</code> will be structured as follows:</p>
<ul>
<li><strong><code>sv-core</code></strong>: The Decoding/Encoding of <code>sv</code>, agnostic of any parsing</li>
<li><strong><code>sv</code></strong>: <code>sv-core</code> atop its new default parser (<code>hw-dsv</code>)</li>
<li><strong><code>sv-cassava</code></strong>: <code>sv-core</code> atop <code>cassava</code>’s parser instead</li>
<li><strong><code>svfactor</code></strong>: The syntax-preserving parsing/printing/manipulation of <code>sv 0.1</code>, packaged as its own library, with no dependency on any other <code>sv</code> package.</li>
<li><strong><code>sv-svfactor</code></strong>: <code>sv-core</code> atop svfactor, for those wanting the behaviour of the earlier <code>sv</code> version.</li>
</ul>
<p>Most users interested in decoding and encoding should use <code>sv</code>, being careful to set the right cabal flags as explained in the README and cabal package description in order to get the best performance. Those running GHC versions below 8.4 will get the better performance by using <code>sv-cassava</code> instead. Those interested not in decoding nor encoding, but interested in a syntax-preserving CSV datastructure can use <code>svfactor</code>. Finally, anyone interested in writing decoders which depend on structural information of the CSV, as was the case in the first release of <code>sv</code>, can use <code>sv-svfactor</code>.</p>
<p>Alternatively, you can use <code>sv</code>’s decoders atop <code>cassava</code>’s parser by using <a href="https://hackage.haskell.org/package/sv-cassava">sv-cassava</a> instead of sv.</p>
<h2 id="future-work">Future work</h2>
<p><code>sv</code> is still missing key features, such as column-name-based decoding. Although the parser is streaming, <code>sv</code> itself still is not. Furthermore, encoding data as CSV is likely to still be slow. I intend to work on these features and performance concerns.</p>
<p><code>svfactor</code>’s parser still has performance problems, so it should be altered or rewritten. When I get around to this, I intend to blog about it. I also intend to blog about benchmarking <code>sv</code> soon.</p>
<h2 id="links-and-further-reading">Links and Further Reading</h2>
<ul>
<li><a href="https://github.com/qfpl/sv/blob/master/examples/src/Data/Sv/Example/Species.lhs">An example</a> of using sv to decode a real CSV file</li>
<li><a href="https://hackage.haskell.org/package/hw-dsv">hw-dsv</a></li>
<li><a href="https://hackage.haskell.org/package/sv">sv</a></li>
<li><a href="https://hackage.haskell.org/package/svfactor">svfactor</a></li>
<li><a href="https://hackage.haskell.org/package/sv-cassava">sv-cassava</a></li>
</ul>
    </div>
  </div>

  
  <div class="row">
    <div class="col my-2">
      <div class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title">
      <a href="../../projects/sv">sv</a>
    </h2>
  </div>
  <div class="panel-body">
    <p>sv is QFPL’s new CSV library. It can be found <a href="https://github.com/qfpl/sv">on Github</a></p>
  </div>
</div>

    </div>
  </div>
  

  
  <div class="row">
    <div class="col my-2">
      
        <div class="p-1">
  <h4 class="data61-bottom-line">
    > <a href="../../people/gwilson">George Wilson</a>
  </h4>
  <p>George Wilson is an enthusiastic programmer at the Queensland Functional Programming Lab. George enjoys writing code, and speaking and writing about Haskell and functional programming.</p>
</div>

      
    </div>
  </div>
  
</div>

          </div>
          <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li role="presentation"><a href="../../">Home</a></li>
                  <li role="presentation"><a href="../../location">Location</a></li>
                  <li role="presentation"><a href="../../people">People</a></li>
                  <li role="presentation"><a href="../../projects">Projects</a></li>
                  <li role="presentation"><a href="../../talks">Talks</a></li>
                  <li role="presentation"><a href="../../contact">Contact</a></li>
                  <li role="presentation"><a href="../../archive">Archive</a></li>
              </ul>
          </div>
        </main>

        <footer class="page-footer data61-green">
            <div class="container">
              <div class="row justify-content-centre">
                <div class="col">
                  <a href="https://github.com/qfpl"><i class="text-white fa fa-github fa-2x"></i></a>
                  <a href="https://twitter.com/queenslandfplab"><i class="text-white fa fa-twitter fa-2x"></i></a>
                  <a href="mailto:contact@qfpl.io"><i class="text-white fa fa-envelope-o fa-2x"></i></a>
                </div>
                <div class="col">
                  <a class="float-right" rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
                  </a>
                </div>
              </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>

        <script src="../../js/bootstrap.min.js"></script>

        
          
        
    </body>
</html>
