<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Queensland FP Lab - Multi-Parameter Type Classes and their Orphan Rules</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../../css/custom.css">
        <link rel="stylesheet" href="../../css/syntax.css">
        
          
        
        <link rel="alternate" type="application/atom+xml" title="Queensland Functional Programming Lab Blog" href="../../atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Queensland Functional Programming Lab Blog" href="../../rss.xml">
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106321204-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments)};
          gtag('js', new Date());

          gtag('config', 'UA-106321204-1', { 'anonymize_ip': true });
        </script>
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg data61-green">
              <a class="navbar-brand text-white" href="../../">
                <div class="card brand-logo">
                  <div class="card-body">
                    <div class="brand-anim-block">
                      <img id="qfpl-brand-data61-logo" height="55" alt="Data61 logo" src="../../images/DATA61-CSIRO_MONO.png" />
                    </div>
                  </div>
                </div>
              </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <i class="fa fa-lg fa-fw fa-bars text-white"></i>
              </button>

              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../location">Location</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../people">People</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../talks">Talks</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../contact">Contact</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../archive">Archive</a>
                    </li>
                  </ul>
                </div>
            </nav>
        </header>

        <main>
          <div class="container-fluid">
              <div class="container">
  <div class="row">
    <div class="col my-2">
      <h2>Multi-Parameter Type Classes and their Orphan Rules</h2>
      <small>Posted on November 27, 2017</small>
    </div>
  </div>

  <div class="row">
    <div class="col my-2">
      <p><em>If you are already familiar with multi-parameter type classes, functional dependencies, and orphan instances, you can skip to the final section for the orphan rules.</em></p>
<h2 id="type-classes-and-orphan-instances">Type classes and orphan instances</h2>
<p>Haskell’s type classes are a powerful and practical means of ad-hoc polymorphism. One aspect of this, the global coherence of type class instances, enables us to reason about the behaviour of type classes effectively. If you’re interested in an in-depth exploration of that topic, I’d recommend <a href="https://www.youtube.com/watch?v=hIZxTQP1ifo">this talk</a>.</p>
<p>One of the requirements for the coherence property is the absence of <em>orphan instances</em>. An instance of a type class for a type is not an orphan if it is defined in the same module as that type class, or in the same module as that type. It is an orphan instance if it is defined in any module other than these two. So for example, the <code>Eq</code> instance for <code>Int</code> could be defined in the same module as <code>Eq</code> is defined (<code>GHC.Classes</code>), or in the same module as <code>Int</code> is defined (<code>GHC.Types</code>). The instance would be an orphan instance if it were defined in, for example, <code>Data.String</code>, because that module neither defines <code>Eq</code> nor <code>Int</code>.</p>
<h2 id="multi-parameter-type-classes-and-functional-dependencies">Multi-Parameter Type classes and functional dependencies</h2>
<p>Over the years, there have been many GHC language extensions extending the type class mechanism in different ways. I recommend the <a href="https://ocharles.org.uk/blog/posts/2014-12-01-24-days-of-ghc-extensions.html">24 Days of GHC Extensions</a> blog series as an accessible introduction to several of them, among other commonly-used GHC language extensions. One such extension which pops up fairly often is <em>multi-parameter type classes</em>, which is enabled with a language pragma like so:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb1-1"><a href="#cb1-1"></a><span class="ot">{-# LANGUAGE MultiParamTypeClasses #-}</span></span></code></pre></div>
<p>Multiparameter type classes are quite useful, but they introduce ambiguity that tends to cause problems with type inference, often requiring annotations in inconvenient locations. To remedy this problem, the <em>functional dependencies</em> language extension was added, allowing us to specify that one or several type parameters uniquely determine some other type parameter. The type inferencer can use this information to resolve the ambiguity and carry on with its work.</p>
<p>Here’s an example of a multi-parameter type class with functional dependencies, the <code>Measured</code> type from the <a href="https://hackage.haskell.org/package/fingertree">fingertree package</a><a href="#fn1" class="footnote-ref" id="fnref1" role="doc-noteref"><sup>1</sup></a>.</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb2-1"><a href="#cb2-1"></a><span class="kw">class</span> <span class="dt">Monoid</span> v <span class="ot">=&gt;</span> <span class="dt">Measured</span> v a <span class="op">|</span> a <span class="ot">-&gt;</span> v <span class="kw">where</span></span>
<span id="cb2-2"><a href="#cb2-2"></a><span class="ot">  measure ::</span> a <span class="ot">-&gt;</span> v</span></code></pre></div>
<p>This class says that values of type <code>a</code> are measured by some monoid <code>v</code>, whose monoidal structure will let us combine these measurements. The functional dependency <code>a -&gt; v</code> on the first line tells us that the type <code>a</code> uniquely determines the type <code>v</code>.</p>
<p>Here’s an instance of <code>Measured</code> for lists.</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb3-1"><a href="#cb3-1"></a><span class="kw">instance</span> <span class="dt">Measured</span> (<span class="dt">Sum</span> <span class="dt">Int</span>) [x] <span class="kw">where</span></span>
<span id="cb3-2"><a href="#cb3-2"></a>  measure xs <span class="ot">=</span> <span class="dt">Sum</span> (<span class="fu">length</span> xs)</span></code></pre></div>
<p>When the type inferencer learns what type <code>a</code> is (in this case <code>[x]</code>), it will learn what <code>v</code> is (in this case <code>Sum Int</code>). This greatly helps reduce the ambiguity multi-parameter type classes introduce, meaning GHC can infer the types of expressions like the following.</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb4-1"><a href="#cb4-1"></a>measure [<span class="ch">'a'</span>, <span class="ch">'b'</span>, <span class="ch">'c'</span>] <span class="op">&lt;&gt;</span> measure [<span class="ch">'d'</span>, <span class="ch">'e'</span>]</span></code></pre></div>
<p>Without the functional dependency <code>a -&gt; v</code> in the type class declaration, that expression would not compile, as the intended measure type <code>v</code> would be ambiguous in that expression.</p>
<p>But there is a penalty to functional dependencies. To continue with our <code>Measured</code> example: we can’t have several different measures for the same type. This is precisely what functional dependencies enforce; the type <code>a</code> must <em>uniquely</em> determine the choice of <code>v</code>. So we can’t make another instance with list in the <code>a</code> position. That would violate the uniqueness. We can still make as many instances with <code>Sum Int</code> in the <code>v</code> position as we’d like, however, because <code>v</code> does <em>not</em> uniquely determine <code>a</code>. For example we could make an instance that measures <code>Set</code>s using <code>Sum Int</code>:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb5-1"><a href="#cb5-1"></a><span class="kw">import</span> <span class="kw">qualified</span> <span class="dt">Data.Set</span> <span class="kw">as</span> <span class="dt">S</span></span>
<span id="cb5-2"><a href="#cb5-2"></a></span>
<span id="cb5-3"><a href="#cb5-3"></a><span class="kw">instance</span> <span class="dt">Measured</span> (<span class="dt">Sum</span> <span class="dt">Int</span>) (<span class="dt">S.Set</span> a) <span class="kw">where</span></span>
<span id="cb5-4"><a href="#cb5-4"></a>  measure set <span class="ot">=</span> <span class="dt">Sum</span> (S.size set)</span></code></pre></div>
<h2 id="orphan-rules">Orphan Rules</h2>
<p>I recently came up against an unexpected orphan instance warning in my code, which caused me to ask the question eventually leading to this article: When is an instance of a multi-parameter type class considered an orphan instance? After some digging, I found the answer <a href="https://ghc.haskell.org/trac/ghc/ticket/11999#comment:1">here</a>.</p>
<p>To summarise the above link: in order to not be an orphan, a multi-parameter type class instance involving functional dependencies can occur in the module where the type class is defined, or in a module which defines a type that is <em>not determined</em> by any of the functional dependencies.</p>
<p>For our <code>Measured</code> example above and its instance <code>Measured (Sum Int) [a]</code>, that instance could occur in the module that defines <code>Measured</code>, or the module that defines list. That instance would be an orphan if it occurred in the module where <code>Sum</code> is defined, since <code>Sum Int</code> is determined by <code>[a]</code> according to the functional dependency.</p>
<p>This has a consequence that I’ve hit recently. Suppose you have something like:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb6-1"><a href="#cb6-1"></a><span class="kw">class</span> <span class="dt">Wibble</span> a b <span class="op">|</span> a <span class="ot">-&gt;</span> b, b <span class="ot">-&gt;</span> a <span class="kw">where</span></span>
<span id="cb6-2"><a href="#cb6-2"></a><span class="ot">  wobble ::</span> a <span class="ot">-&gt;</span> b</span></code></pre></div>
<p>Here we have a pair of functional dependencies which together describe a bidirectional relationship. In this case, where could our instance be? Remember that <code>a</code> is determined by <code>b</code>, and <code>b</code> is determined by <code>a</code>. So we can’t put our instance in either of their modules according to the rules above, unless they happen to be defined in the same module. Often they are not. Hence the only place this instance can live is in the module that defines the type class.</p>
<p>Another solution to this is to define two newtypes in the same module - one for each of the types - and then give this instance to the newtypes.</p>
<section class="footnotes" role="doc-endnotes">
<hr />
<ol>
<li id="fn1" role="doc-endnote"><p>Finger trees are an interesting concept on their own, so you might like to read about them <a href="http://www.staff.city.ac.uk/~ross/papers/FingerTree.html">here</a>.)<a href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
</section>
    </div>
  </div>

  
  <div class="row">
    <div class="col my-2">
      
    </div>
  </div>
  

  
  <div class="row">
    <div class="col my-2">
      
        <div class="p-1">
  <h4 class="data61-bottom-line">
    > <a href="../../people/gwilson">George Wilson</a>
  </h4>
  <p>George Wilson is an enthusiastic programmer at the Queensland Functional Programming Lab. George enjoys writing code, and speaking and writing about Haskell and functional programming.</p>
</div>

      
    </div>
  </div>
  
</div>

          </div>
          <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li role="presentation"><a href="../../">Home</a></li>
                  <li role="presentation"><a href="../../location">Location</a></li>
                  <li role="presentation"><a href="../../people">People</a></li>
                  <li role="presentation"><a href="../../projects">Projects</a></li>
                  <li role="presentation"><a href="../../talks">Talks</a></li>
                  <li role="presentation"><a href="../../contact">Contact</a></li>
                  <li role="presentation"><a href="../../archive">Archive</a></li>
              </ul>
          </div>
        </main>

        <footer class="page-footer data61-green">
            <div class="container">
              <div class="row justify-content-centre">
                <div class="col">
                  <a href="https://github.com/qfpl"><i class="text-white fa fa-github fa-2x"></i></a>
                  <a href="https://twitter.com/queenslandfplab"><i class="text-white fa fa-twitter fa-2x"></i></a>
                  <a href="mailto:contact@qfpl.io"><i class="text-white fa fa-envelope-o fa-2x"></i></a>
                </div>
                <div class="col">
                  <a class="float-right" rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
                  </a>
                </div>
              </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>

        <script src="../../js/bootstrap.min.js"></script>

        
          
        
    </body>
</html>
