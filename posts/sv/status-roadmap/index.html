<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Queensland FP Lab - sv - Introduction, Status, and Road-map</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../../../css/custom.css">
        <link rel="stylesheet" href="../../../css/syntax.css">
        
          
        
        <link rel="alternate" type="application/atom+xml" title="Queensland Functional Programming Lab Blog" href="../../../atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Queensland Functional Programming Lab Blog" href="../../../rss.xml">
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106321204-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments)};
          gtag('js', new Date());

          gtag('config', 'UA-106321204-1', { 'anonymize_ip': true });
        </script>
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg data61-green">
              <a class="navbar-brand text-white" href="../../../">
                <div class="card brand-logo">
                  <div class="card-body">
                    <div class="brand-anim-block">
                      <img id="qfpl-brand-data61-logo" height="55" alt="Data61 logo" src="../../../images/DATA61-CSIRO_MONO.png" />
                    </div>
                  </div>
                </div>
              </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <i class="fa fa-lg fa-fw fa-bars text-white"></i>
              </button>

              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../location">Location</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../people">People</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../talks">Talks</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../contact">Contact</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../archive">Archive</a>
                    </li>
                  </ul>
                </div>
            </nav>
        </header>

        <main>
          <div class="container-fluid">
              <div class="container">
  <div class="row">
    <div class="col my-2">
      <h2>sv - Introduction, Status, and Road-map</h2>
      <small>Posted on March 29, 2018</small>
    </div>
  </div>

  <div class="row">
    <div class="col my-2">
      <h2 id="what-is-sv">What is sv?</h2>
<p><code>sv</code> is QFPL’s new CSV library for Haskell.</p>
<p>The core data structure of <code>sv</code> is a syntax tree for CSV that preserves white-space, quoting information, newline style, and other data that is usually thrown away. This gives the property that parsing a file followed by printing it out will return the original file unaltered: <code>print . parse = id</code></p>
<p><code>sv</code> gives you tools to work with this data structure in a number of ways. Primarily, <code>sv</code> offers four things: parsing, decoding, encoding, and printing.</p>
<ul>
<li>Parsing is process of reading a textual representation of a CSV document into the <code>Sv</code> data structure.</li>
<li>Decoding is extracting from a value of type <code>Sv</code> into domain-specific types defined by the user.</li>
<li>Encoding is constructing a value of type <code>Sv</code> from domain-specific types.</li>
<li>Printing is serialising a value of type <code>Sv</code> as textual data.</li>
</ul>
<p>This terminology should be used consistently in the documentation for <code>sv</code>, as well as in its identifier names, and in these blog posts.</p>
<p><image src="../../../images/posts/sv/parsedecodeencodeprint.png" alt="parse, decode, encode, print"></image></p>
<p>A key feature of the <code>sv</code> library is its separation of these phases. You can use any of these phases independently of one another. The consequences of this are many and exciting.</p>
<p>The parser in the <code>sv</code> library will parse <a href="https://tools.ietf.org/html/rfc4180">RFC 4180</a> CSV files, but it is much more flexible in what it allows. For example, <code>sv</code> is tolerant of spacing around fields. This means that files which use spacing to horizontally align their data will be accurately handled in <code>sv</code>. <code>sv</code> parses non-rectangular data sets, and gives users tools for decoding them. By “non-rectangular”, we mean that each row may not have the same number of fields.</p>
<p><code>sv</code> notably does not use type classes for decoding nor encoding. We use an <code>Applicative</code> domain-specific-language for describing how to decode data. This means we can have multiple decoders for the same type. <code>Decode</code> being a first-class value means we can write interesting transformations and combinators on them that we couldn’t do with a type class instance. We also avoid the dreaded orphan instance problem.</p>
<p>We are planning more blog posts that will explore some of the benefits of the design choices we’ve described here.</p>
<h2 id="status">Status</h2>
<p><a href="https://hackage.haskell.org/package/sv-0.1"><code>sv-0.1</code></a> is on hackage, so you can use <code>sv</code> today.</p>
<p>The Parse and Decode aspects of <code>sv</code> are considered ready for real-world use. I believe they are tested and benchmarked sufficiently. By building its parser with <code>parsers</code>, <code>sv</code> lets you use your favourite parser library for the parsing phase. <code>trifecta</code> is the default parsing library due to its helpful error messages, but others can be used, such as <code>attoparsec</code>. Whether with trifecta or with attoparsec, the parser in sv is considerably slower than the parser in the <code>cassava</code> library. Exploring this difference will likely be the basis of a future blog post.</p>
<p>The Encode and Print phases are tested but have not been benchmarked, so it is not known whether they perform reasonably.</p>
<p><code>sv</code> is new, and its API is likely to change. The changelog will be kept up to date as the library changes. Releases will follow the <a href="https://pvp.haskell.org">PVP</a>, so you can keep yourself safe from breaking change by specifying appropriate version bounds in your cabal file like so: <code>sv &gt;= 0.1 &amp;&amp; &lt; 0.2</code>.</p>
<p><code>sv</code> does not yet offer any kind of streaming, which means there’s a limit on the size of files that it can practically be used with. This limit depends on your system memory.</p>
<h2 id="where-to-from-here">Where to from here?</h2>
<p><code>sv</code> is a young project. The following is a collection of my thoughts on where to take <code>sv</code> from here. If something is missing from this list, please <a href="https://github.com/qfpl/sv/issues/new">open an issue</a> on Github.</p>
<p>Based on my conversations so far with CSV library users, the very next feature needed is <a href="https://github.com/qfpl/sv/issues/6">column-name-based decoding</a>. Currently <code>sv</code> only has position-based decoding, where we decode a field based on its horizontal position. Users of cassava or similar libraries also like to decode based on column names as they appear in the header of the document. An advantage of this style is that your <code>Decode</code>s are resilient to changes in column ordering.</p>
<p><code>sv</code> needs <a href="https://github.com/qfpl/sv/issues/10">streaming</a> to support very large files without eating all of the system’s memory.</p>
<p>It would be preferable to have better performance in <code>sv</code>, particularly in the parsing phase. To lessen this pain for now, you can get the benefit of <code>sv</code>’s decoding without paying for its parser’s performance by <a href="https://hackage.haskell.org/package/sv-cassava">substituting cassava’s parser</a>. That means you can trade off between <code>sv</code>’s flexibility and cassava’s speed. It is preferable to have a faster parser in <code>sv</code> itself, but other future plans have a higher priority.</p>
<p><code>sv</code>’s encoding is powered by <code>Contravariant</code> and its relatives <code>Divisible</code> and <code>Decidable</code>, but I think these aren’t particularly well known. I’d like to write and talk about them, and I’m interested in how they could be made easier to work with; for example by giving them good infix operator support.</p>
<p><code>sv</code> collects all errors that occur during decoding; not just the first one. Although they’re collected, these currently aren’t very useful because they don’t include the <a href="https://github.com/qfpl/sv/issues/5">source location</a> at which they occurred. This should definitely be fixed. It would also be good to fold together multiple errors of the same sort. For example, if the same error occurred on ten different lines, you only really need to see its message once, along with all the lines on which it occurred.</p>
<p>There are other things already listed on the <a href="https://github.com/qfpl/sv/issues/">issue tracker</a>, and you’re welcome to add your own.</p>
<p>In the longer term, I’d like to build libraries on top of the <code>sv</code> library for higher-level functionality. I think we could take advantage of <code>sv</code>’s error aggregation to build tooling to detect common problems with data sets and recommend actions to take.</p>
    </div>
  </div>

  
  <div class="row">
    <div class="col my-2">
      <div class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title">
      <a href="../../../projects/sv">sv</a>
    </h2>
  </div>
  <div class="panel-body">
    <p>sv is QFPL’s new CSV library. It can be found <a href="https://github.com/qfpl/sv">on Github</a></p>
  </div>
</div>

    </div>
  </div>
  

  
  <div class="row">
    <div class="col my-2">
      
        <div class="p-1">
  <h4 class="data61-bottom-line">
    > <a href="../../../people/gwilson">George Wilson</a>
  </h4>
  <p>George Wilson is an enthusiastic programmer at the Queensland Functional Programming Lab. George enjoys writing code, and speaking and writing about Haskell and functional programming.</p>
</div>

      
    </div>
  </div>
  
</div>

          </div>
          <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li role="presentation"><a href="../../../">Home</a></li>
                  <li role="presentation"><a href="../../../location">Location</a></li>
                  <li role="presentation"><a href="../../../people">People</a></li>
                  <li role="presentation"><a href="../../../projects">Projects</a></li>
                  <li role="presentation"><a href="../../../talks">Talks</a></li>
                  <li role="presentation"><a href="../../../contact">Contact</a></li>
                  <li role="presentation"><a href="../../../archive">Archive</a></li>
              </ul>
          </div>
        </main>

        <footer class="page-footer data61-green">
            <div class="container">
              <div class="row justify-content-centre">
                <div class="col">
                  <a href="https://github.com/qfpl"><i class="text-white fa fa-github fa-2x"></i></a>
                  <a href="https://twitter.com/queenslandfplab"><i class="text-white fa fa-twitter fa-2x"></i></a>
                  <a href="mailto:contact@qfpl.io"><i class="text-white fa fa-envelope-o fa-2x"></i></a>
                </div>
                <div class="col">
                  <a class="float-right" rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
                  </a>
                </div>
              </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>

        <script src="../../../js/bootstrap.min.js"></script>

        
          
        
    </body>
</html>
