<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Queensland FP Lab - Managing GHC versions with ghcup</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../../css/custom.css">
        <link rel="stylesheet" href="../../css/syntax.css">
        
          
        
        <link rel="alternate" type="application/atom+xml" title="Queensland Functional Programming Lab Blog" href="../../atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Queensland Functional Programming Lab Blog" href="../../rss.xml">
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106321204-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments)};
          gtag('js', new Date());

          gtag('config', 'UA-106321204-1', { 'anonymize_ip': true });
        </script>
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg data61-green">
              <a class="navbar-brand text-white" href="../../">
                <div class="card brand-logo">
                  <div class="card-body">
                    <div class="brand-anim-block">
                      <img id="qfpl-brand-data61-logo" height="55" alt="Data61 logo" src="../../images/DATA61-CSIRO_MONO.png" />
                    </div>
                  </div>
                </div>
              </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <i class="fa fa-lg fa-fw fa-bars text-white"></i>
              </button>

              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../location">Location</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../people">People</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../talks">Talks</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../contact">Contact</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../archive">Archive</a>
                    </li>
                  </ul>
                </div>
            </nav>
        </header>

        <main>
          <div class="container-fluid">
              <div class="container">
  <div class="row">
    <div class="col my-2">
      <h2>Managing GHC versions with ghcup</h2>
      <small>Posted on September 18, 2019</small>
    </div>
  </div>

  <div class="row">
    <div class="col my-2">
      <p>If you use Haskell, you probably use the Glasgow Haskell Compiler. GHC releases once every six months. When using <code>stack</code> or <code>nix</code>, the GHC version is managed for you. When using <code>cabal</code>, it is up to you to manage your GHC installation(s). I have <a href="../multiple-ghcs">previously posted</a> about how I manage multiple installations of GHC using <code>nix</code> or <code>apt</code>. More recently I have mostly been using the <code>ghcup</code> tool, which lets you install and manage multiple GHC versions on most Linux distributions and macOS.</p>
<h2 id="why-ghcup">Why <code>ghcup</code>?</h2>
<p>I like <code>ghcup</code> because it’s simple and works across multiple operating systems. For example, it’s my favourite option for installing GHC on my home Fedora machine and my Ubuntu VMs (although Herbert’s <a href="https://launchpad.net/~hvr/+archive/ubuntu/ghc">Ubuntu PPA</a> and <a href="https://downloads.haskell.org/debian/">Debian packages</a> should be preferred on those systems). On NixOS I still use nix.</p>
<p><code>ghcup</code> is great for multiple getting GHCs happening, almost wherever you are, but you should consider using your operating system’s package manager when possible.</p>
<h2 id="using-ghcup">Using <code>ghcup</code></h2>
<p>Install <code>ghcup</code> by following <a href="https://www.haskell.org/ghcup/">the instructions here</a>. This will also install the recommended versions of <code>cabal</code> and GHC. Make sure to follow the script’s instructions about adding the appropriate line to your <code>~/.bashrc</code> or similar.</p>
<p>With <code>ghcup</code> installed, you can install the extra GHC versions you care about:</p>
<pre><code>$ ghcup install 8.0.2
$ ghcup install 8.2.2
$ ghcup install 8.4.4
$ ghcup install 8.6.5</code></pre>
<p>$ You can also use <code>ghcup</code> to install the latest <code>cabal</code> as needed:</p>
<pre><code>$ ghcup install-cabal</code></pre>
<p>You will need to add <code>ghcup</code>’s binary directory <code>~/.ghcup/bin</code> to your <code>PATH</code> to use these executables.</p>
<p><code>ghcup</code> will fetch binaries for your system, or if there aren’t any available, it will try to build GHC from source. I’ve found the binaries work for me on all the Linux distributions I’ve tried. I’ve heard that <code>ghcup</code> works well on macOS too, but I haven’t personally tested it. There are not many modern GHC binaries available for FreeBSD, so if you’re using that operating system I recommend getting GHC from <code>pkg</code> instead.</p>
<h2 id="specifying-a-ghc-version-to-be-used">Specifying a GHC version to be used</h2>
<p>You can select which GHC version is <code>ghc</code> on the <code>PATH</code> by using</p>
<pre><code>$ ghcup set 8.8.1</code></pre>
<p>which is very helpful.</p>
<p>If you want to use a different version for a particular build, you can use <code>cabal</code>’s <code>--with-compiler</code> flag or its short form <code>-w</code>. This flag lets you specify a Haskell compiler on your <code>PATH</code> to use, for example, you can test your project with GHC <code>8.4.4</code> by running</p>
<pre><code>$ cabal v2-test -w ghc-8.4.4</code></pre>
<p>I often use this when I’m testing my packages against multiple GHC versions.</p>
<p>That’s it! Installing and managing multiple GHC versions has never been easier.</p>
<h2 id="bonus-information">Bonus information</h2>
<h3 id="setting-a-ghc-per-project">Setting a GHC per project</h3>
<p>If you want a certain project to always use a particular GHC version, you can do so by adding a line to your <code>cabal.project</code> file. Here’s a sample <code>cabal.project</code> file you could drop into any cabal project to make sure it always builds with GHC <code>8.4.4</code>:</p>
<pre><code>packages: .
with-compiler: ghc-8.4.4</code></pre>
<p>Now GHC <code>8.4.4</code> will be used regardless of which GHC is <code>ghc</code> on the <code>PATH</code>. This saves us having to use <code>-w</code> for every command.</p>
<h3 id="what-about-windows">What about Windows?</h3>
<p><code>ghcup</code> does not work on Windows, and it is not planned to. So how should we manage our GHC versions on Windows? I have done some Haskell development on Windows. Most of the time I got away with WSL; I opened a bash shell backed by Ubuntu and just used <code>ghcup</code>.</p>
<p>On actual Windows, I have heard great things about Chocolatey. <a href="https://hub.zhox.com/posts/introducing-haskell-dev/">See this blog post</a> for more information on getting GHC and cabal up and running on Windows.</p>
<h3 id="curl-into-bash-is-evil"><code>curl</code> into <code>bash</code> is evil!</h3>
<p>I agree. Here’s how I actually use <code>ghcup</code>. Instead of curling into bash, clone the repo:</p>
<pre><code>$ git clone git@github.com:haskell/ghcup.git
$ cd ghcup</code></pre>
<p>Next, get yourself set up by running the script you would have fetched with curl:</p>
<pre><code>$ ./bootstrap-haskell</code></pre>
<p>The rest of the process is as above.</p>
    </div>
  </div>

  
  <div class="row">
    <div class="col my-2">
      <div class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title">
      <a href="../../projects/infra">The Functional Infrastructure Project</a>
    </h2>
  </div>
  <div class="panel-body">
    <p>We’re using functional programming to set up the infrastructure that we use from day to day.</p>
  </div>
</div>

    </div>
  </div>
  

  
  <div class="row">
    <div class="col my-2">
      
        <div class="p-1">
  <h4 class="data61-bottom-line">
    > <a href="../../people/gwilson">George Wilson</a>
  </h4>
  <p>George Wilson is an enthusiastic programmer at the Queensland Functional Programming Lab. George enjoys writing code, and speaking and writing about Haskell and functional programming.</p>
</div>

      
    </div>
  </div>
  
</div>

          </div>
          <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li role="presentation"><a href="../../">Home</a></li>
                  <li role="presentation"><a href="../../location">Location</a></li>
                  <li role="presentation"><a href="../../people">People</a></li>
                  <li role="presentation"><a href="../../projects">Projects</a></li>
                  <li role="presentation"><a href="../../talks">Talks</a></li>
                  <li role="presentation"><a href="../../contact">Contact</a></li>
                  <li role="presentation"><a href="../../archive">Archive</a></li>
              </ul>
          </div>
        </main>

        <footer class="page-footer data61-green">
            <div class="container">
              <div class="row justify-content-centre">
                <div class="col">
                  <a href="https://github.com/qfpl"><i class="text-white fa fa-github fa-2x"></i></a>
                  <a href="https://twitter.com/queenslandfplab"><i class="text-white fa fa-twitter fa-2x"></i></a>
                  <a href="mailto:contact@qfpl.io"><i class="text-white fa fa-envelope-o fa-2x"></i></a>
                </div>
                <div class="col">
                  <a class="float-right" rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
                  </a>
                </div>
              </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>

        <script src="../../js/bootstrap.min.js"></script>

        
          
        
    </body>
</html>
