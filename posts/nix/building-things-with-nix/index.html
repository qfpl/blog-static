<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Queensland FP Lab - Building things with Nix</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../../../css/custom.css">
        <link rel="stylesheet" href="../../../css/syntax.css">
        
          
        
        <link rel="alternate" type="application/atom+xml" title="Queensland Functional Programming Lab Blog" href="../../../atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Queensland Functional Programming Lab Blog" href="../../../rss.xml">
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106321204-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments)};
          gtag('js', new Date());

          gtag('config', 'UA-106321204-1', { 'anonymize_ip': true });
        </script>
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg data61-green">
              <a class="navbar-brand text-white" href="../../../">
                <div class="card brand-logo">
                  <div class="card-body">
                    <div class="brand-anim-block">
                      <img id="qfpl-brand-data61-logo" height="55" alt="Data61 logo" src="../../../images/DATA61-CSIRO_MONO.png" />
                    </div>
                  </div>
                </div>
              </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <i class="fa fa-lg fa-fw fa-bars text-white"></i>
              </button>

              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../location">Location</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../people">People</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../talks">Talks</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../contact">Contact</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../archive">Archive</a>
                    </li>
                  </ul>
                </div>
            </nav>
        </header>

        <main>
          <div class="container-fluid">
              <div class="container">
  <div class="row">
    <div class="col my-2">
      <h2>Building things with Nix</h2>
      <small>Posted on August 21, 2017</small>
    </div>
  </div>

  <div class="row">
    <div class="col my-2">
      <p>In the <a href="../getting-started-with-nix/">previous post</a>, we got comfortable with using Nix as a package manager. Now we’re going to get a feel for what it’s like to build a package.</p>
<p>Most of this information is available in the <a href="https://nixos.org/nix/manual/#chap-writing-nix-expressions">‘Writing Nix expressions’ chapter</a> of the <a href="https://nixos.org/nix/manual/">Nix manual</a>, with more detail coming from the <a href="https://nixos.org/nixpkgs/manual/#chap-stdenv">‘Standard Environment’ chapter</a> of the <a href="https://nixos.org/nixpkgs/manual/">Nixpkgs manual</a>. This is another of the Nix manuals that you’ll get to know and love. In particular, the Nixpkgs manual has chapters on specific languages and frameworks that can be extremely helpful.</p>
<p>Just like last time, you should be able to play along with the examples as you are reading this post.</p>
<h2 id="an-overview-of-building-things-with-nix">An overview of building things with Nix</h2>
<p>Let’s create a package for the GNU hello utility. We’re going to travel quickly and gloss over some things but we’ll dig into the details immediately afterwards. For now we’re going to set up an example package and try to get a sense of what happens when Nix builds something successfully.</p>
<p>To that end, we’ll write <code>hello.nix</code>:</p>
<pre><code>{ stdenv, fetchurl }:

stdenv.mkDerivation rec {
  name = &quot;hello-2.10&quot;;

  src = fetchurl {
    url = &quot;mirror://gnu/hello/${name}.tar.gz&quot;;
    sha256 = &quot;0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i&quot;;
  };
}</code></pre>
<p>to package up the GNU hello utility.</p>
<p>This is a function that takes two arguments and produces a derivation - and the derivation is what we are after.</p>
<p>There is a function in the Nix package set named <code>callPackage</code> that is helpful here. The <code>callPackage</code> function will fill in missing arguments to functions, provided that the argument names match the names of packages from the Nix package set.</p>
<p>We can write <code>default.nix</code> to demonstrate this:</p>
<pre><code>let
  nixpkgs = import &lt;nixpkgs&gt; {};
in
  nixpkgs.callPackage ./hello.nix {}</code></pre>
<p>The missing arguments to <code>hello.nix</code> have been filled in by <code>callPackage</code>, and so <code>default.nix</code> gives us a derivation.</p>
<p>The GNU hello utility is built with <code>autotools</code> - <code>configure</code>, <code>make</code> and friends - and Nix will try to use those if you don’t give it any other information about how to build a derivation.</p>
<p>This means we can build the derivation with:</p>
<pre><code>nix-build default.nix
...
/nix/store/g0fw64zf7n0hr1dx7yl9n8qgqbhdngrm-hello-2.10</code></pre>
<p>If no file is given to <code>nix-build</code> it will look for <code>default.nix</code> in the current directory, so we can do:</p>
<pre><code>nix-build</code></pre>
<p>and get the same result.</p>
<p>The output of the build is installed into the Nix store, but will also appear in the symbolic link named <code>result</code>:</p>
<pre><code>&gt; ls result
bin 
share</code></pre>
<p>This symbolic link is set up as a garbage collection root, so your package and its dependencies will stick around until you remove <code>result</code> and a garbage collection occurs.</p>
<p>We could run the executable:</p>
<pre><code>&gt; result/bin/hello
Hello, world!</code></pre>
<p>but we’d feel a bit bad for whoever had to muck about with autotools to package something so simple, so maybe we shouldn’t.</p>
<p>We’re about to look at all of this in greater detail, but before we do that we should clean up after ourselves:</p>
<pre><code>&gt; rm result
&gt; nix-collect-garbage -d</code></pre>
<h2 id="what-is-going-on-under-the-hood">What is going on under the hood</h2>
<h3 id="getting-hold-of-our-sources">Getting hold of our sources</h3>
<p>In order to make use of the tarball containing the GNU hello source code, we need to know the hash of the sources.</p>
<p>Thankfully there are some helpful scripts for this, which we’ll install:</p>
<pre><code>nix-env -i nix-prefetch-scripts</code></pre>
<p>We can use these scripts to download things, add them to the nix store, and to print their hashes and other metadata that we might need.</p>
<p>Let’s grab a hold of those sources now:</p>
<pre><code>&gt; nix-prefetch-url mirror://gnu/hello/hello-2.10.tar.gz
downloading ‘http://ftpmirror.gnu.org/hello/hello-2.10.tar.gz’... [0/0 KiB, 0.0 KiB/s]
path is ‘/nix/store/3x7dwzq014bblazs7kq20p9hyzz0qh8g-hello-2.10.tar.gz’
0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i</code></pre>
<p>We can see that there is a copy in the Nix store, and the script prints out the hash of the tarball as well.</p>
<p>At the moment the prefetch scripts contain:</p>
<ul>
<li>nix-prefetch-bzr</li>
<li>nix-prefetch-cvs</li>
<li>nix-prefetch-git</li>
<li>nix-prefetch-hg</li>
<li>nix-prefetch-svn</li>
<li>nix-prefetch-url</li>
</ul>
<p>These have corresponding functions which we use inside our Nix packages:</p>
<pre><code>src = fetchurl {
  url = &quot;mirror://gnu/hello/${name}.tar.gz&quot;;
  sha256 = &quot;0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i&quot;;
};</code></pre>
<p>so that other people can use them without having to do any prefetching.</p>
<p>Once you know the hash of the sources, you are good to go. That is why <code>default.nix</code> was able to download and build the sources at the start of this post, before we did the prefetch.</p>
<p>There is one small optimization available for GitHub users which is worth pointing out. Fetching from git will checkout the whole repository in order to get hold of a specific revision:</p>
<pre><code>exitcode = pkgs.fetchgit {
  url = &quot;https://github.com/qfpl/exitcode&quot;;
  rev = &quot;e56313946fdfe77eed91c54d791b7be8aa73c495&quot;;
  sha256 = &quot;0a2nnk9ifaln0v9dq0wgdazqxika0j32vv8p6s4qsrpfs2kxaqar&quot;;
};</code></pre>
<p>but f the project we are interested in is hosted on GitHub, there is a function that can use the GitHub APIs to fetch the code for a specific revision:</p>
<pre><code>exitcode = pkgs.fetchFromGitHub {
  owner = &quot;qfpl&quot;;
  repo = &quot;exitcode&quot;;
  rev = &quot;e56313946fdfe77eed91c54d791b7be8aa73c495&quot;;
  sha256 = &quot;0a2nnk9ifaln0v9dq0wgdazqxika0j32vv8p6s4qsrpfs2kxaqar&quot;;
};</code></pre>
<p>which can save us some time and data.</p>
<h3 id="building-the-project-from-nix-shell">Building the project from <code>nix-shell</code></h3>
<p>We have <code>nix-build</code> available to us for building Nix derivations.</p>
<p>While we’re working on writing our own derivations, we can use <code>nix-shell</code></p>
<pre><code>&gt; nix-shell default.nix
nix-shell &gt;</code></pre>
<p>If no file is given to <code>nix-shell</code> it will look for <code>shell.nix</code> in the current directory, and then <code>default.nix</code>, so we can do:</p>
<pre><code>&gt; nix-shell
nix-shell &gt;</code></pre>
<p>By default we’ll have access to everything from our host environment while we’re inside of the shell, so that we can edit files and so on. If we want to make sure that we have specified all of the required dependencies to build our package, we should use:</p>
<pre><code>&gt; nix-shell --pure</code></pre>
<p>so that the only things we have access to are the things mentioned in our Nix files.</p>
<p>We would normally use separate <code>shell.nix</code> and <code>default.nix</code> files if we wanted to tweak the packages and environment that is available while we’re developing a package. This might include things like adding a debugger or an editor as a dependency in <code>shell.nix</code>, or setting an environment variable that leads to more verbose output as things build.</p>
<p>Let’s have a look at what we have at our disposal once we’re inside the shell. Along with a few standard build tools, we have access to the attributes from <code>hello.nix</code> as environment variables.</p>
<p>The <code>name</code> attribute is present:</p>
<pre><code>nix-shell&gt; echo $name
hello-2.10</code></pre>
<p>The <code>src</code> attribute is present, and is a path to the sources in the Nix store:</p>
<pre><code>nix-shell&gt; echo $src
/nix/store/3x7dwzq014bblazs7kq20p9hyzz0qh8g-hello-2.10.tar.gz
nix-shell&gt; ls $src
/nix/store/3x7dwzq014bblazs7kq20p9hyzz0qh8g-hello-2.10.tar.gz</code></pre>
<p>The hash of this derivation has already been calculated at this point, so Nix has added an environment variable pointing to the directory where we should put our results:</p>
<pre><code>nix-shell&gt; echo $out
/nix/store/g0fw64zf7n0hr1dx7yl9n8qgqbhdngrm-hello-2.10</code></pre>
<p>but it hasn’t been created yet:</p>
<pre><code>nix-shell&gt; ls $out
ls: cannot access '/nix/store/g0fw64zf7n0hr1dx7yl9n8qgqbhdngrm-hello-2.10': No such file or directory</code></pre>
<p>Give all of that information, we can use <code>autotools</code> to build the GNU hello utility:</p>
<pre><code>nix-shell&gt; tar zxf $src
nix-shell&gt; cd hello-*
nix-shell&gt; ./configure --prefix=$out
nix-shell&gt; make
nix-shell&gt; make install</code></pre>
<p>and then run it directly from the Nix store:</p>
<pre><code>nix-shell&gt; exit
&gt; /nix/store/g0fw64zf7n0hr1dx7yl9n8qgqbhdngrm-hello-2.10/bin/hello
Hello, world!</code></pre>
<p>Let us clean this up for our next little adventure:</p>
<pre><code>&gt; nix-collect-garbage -d</code></pre>
<h3 id="building-the-project-with-builder.sh">Building the project with <code>builder.sh</code></h3>
<p>We can collect these build steps into a bash script, which we’ll name <code>builder.sh</code>:</p>
<pre><code>source $stdenv/setup

tar zxvf $src
cd hello-*
./configure --prefix=$out
make
make install

echo &quot;The script actually ran&quot;</code></pre>
<p>The <code>$stdenv/setup</code> step on the first line is setting up the Nix environment for us. The echo on the last line is so that we can distinguish this build from the automatic build steps that Nix did for us during the overview.</p>
<p>If we reference this script from the <code>builder</code> attribute in our Nix package:</p>
<pre><code>{ stdenv, fetchurl }:

stdenv.mkDerivation rec {
  name = &quot;hello-2.10&quot;;
  
  builder = ./builder.sh;

  src = fetchurl {
    url = &quot;mirror://gnu/hello/${name}.tar.gz&quot;;
    sha256 = &quot;0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i&quot;;
  };
}</code></pre>
<p>then Nix will use it build the package:</p>
<pre><code>&gt; nix-build
...
The script actually ran
/nix/store/p945zy2qhfbk6rb5gvyqclymyvwx5z7q-hello-2.10</code></pre>
<p>This gives us a different hash to what we had before, which is expected since the inputs have changed. If we changed the builder script:</p>
<pre><code>source $stdenv/setup

echo &quot;A different builder&quot;

tar zxvf $src
cd hello-*
./configure --prefix=$out
make
make install</code></pre>
<p>we can see that the builder script takes part in the hash computation as well:</p>
<pre><code>nix-shell&gt; echo $out
/nix/store/7hd6g7hpdg21h5nrzijicim6lik9qsjr-hello-2.10</code></pre>
<p>We can also inline the builder script by making use of some of the utility functions that Nix provides along with the multi-line string literals:</p>
<pre><code>{ pkgs, stdenv, fetchurl }:

stdenv.mkDerivation rec {
  name = &quot;hello-2.10&quot;;
  
  builder = pkgs.writeText &quot;builder.sh&quot; ''
    source $stdenv/setup

    tar zxvf $src
    cd hello-*
    ./configure --prefix=$out
    make
    make install

    echo &quot;The script actually ran&quot;
  '';

  src = fetchurl {
    url = &quot;mirror://gnu/hello/${name}.tar.gz&quot;;
    sha256 = &quot;0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i&quot;;
  };
}</code></pre>
<h3 id="debugging-a-nix-build">Debugging a nix build</h3>
<p>We’re now going to sabotage our builder, to see what we can see what we do to correct things that have gone awry.</p>
<p>We’ll deliberately change into the wrong directory:</p>
<pre><code>source $stdenv/setup

tar zxvf $src
cd hello
./configure --prefix=$out
make
make install

echo &quot;The script actually ran&quot;</code></pre>
<p>resulting in</p>
<pre><code>&gt; nix-build
...
/nix/store/s143n1fws6lb0ngnk6bm6ggrdxkxg8c8-builder.sh: line 4: cd: hello: No such file or directory
builder for ‘/nix/store/p9lvjiik8pwv4rlpxsl29as151vrwp2z-hello-2.10.drv’ failed with exit code 1
error: build of ‘/nix/store/p9lvjiik8pwv4rlpxsl29as151vrwp2z-hello-2.10.drv’ failed</code></pre>
<p>What can we do?</p>
<p>We can indicate that we want to <em>keep</em> the temporary build directory in the event of failures:</p>
<pre><code>&gt; nix-build -K
/nix/store/s143n1fws6lb0ngnk6bm6ggrdxkxg8c8-builder.sh: line 4: cd: hello: No such file or directory
note: keeping build directory ‘/tmp/nix-build-hello-2.10.drv-0’
builder for ‘/nix/store/p9lvjiik8pwv4rlpxsl29as151vrwp2z-hello-2.10.drv’ failed with exit code 1
error: build of ‘/nix/store/p9lvjiik8pwv4rlpxsl29as151vrwp2z-hello-2.10.drv’ failed</code></pre>
<p>We can have a look in this directory:</p>
<pre><code>&gt; cd /tmp/nix-build-hello-2.10.drv-0
&gt; ls
env-vars hello-2.10</code></pre>
<p>and we’ll see that we have the unpacked sources for the GNU hello utility, along with a file naamed <code>env-vars</code>.</p>
<p>This file contains the environment variables at the point of the failure, so we can source that file:</p>
<pre><code>&gt; source env-vars</code></pre>
<p>and then try to carry out our build steps like we were in a <code>nix-shell</code> to see what went wrong:</p>
<pre><code>&gt; cd hello-2.10
&gt; configure --prefix=$out
....</code></pre>
<h3 id="building-the-project-with-the-generic-builder">Building the project with the generic builder</h3>
<p>Instead of specifying a build script, we can let Nix’s generic builder do some of the work for us.</p>
<p>The generic builder proceeds through a number of phases, which we can overload if we want:</p>
<pre><code>{ stdenv, fetchurl }:

stdenv.mkDerivation rec {
  name = &quot;hello-2.10&quot;;

  src = fetchurl {
    url = &quot;mirror://gnu/hello/${name}.tar.gz&quot;;
    sha256 = &quot;0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i&quot;;
  };

  unpackPhase = ''
    tar zxvf $src
    cd hello-*
  '';

  configurePhase = ''
    ./configure --prefix=$out
  '';

  buildPhase = ''
    make
  '';

  installPhase = ''
    make install
  '';
}</code></pre>
<p>There is a default set of phases that get run, and there are default activities that get run in each phase. We can edit the list of phases to run, either to add new phases, to reorder them, or to skip some of them:</p>
<pre><code>  phases = [installPhase];</code></pre>
<p>Each of these phases has a default implementation that is usually pretty sensible. We can see that by removing our <code>unpackPhase</code> and letting the generic build functionality take over:</p>
<pre><code>{ stdenv, fetchurl }:

stdenv.mkDerivation rec {
  name = &quot;hello-2.10&quot;;

  src = fetchurl {
    url = &quot;mirror://gnu/hello/${name}.tar.gz&quot;;
    sha256 = &quot;0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i&quot;;
  };

  configurePhase = ''
    ./configure --prefix=$out
  ''';

  buildPhase = ''
    make
  '';

  installPhase = ''
    make install
  '';
}</code></pre>
<p>which will give us the same result as what we had before.</p>
<p>We can chip away at that, verifying that the output is the same every time we remove one of our phases. Eventually we’ll end up where we started at the beginning of this post:</p>
<pre><code>{ stdenv, fetchurl }:

stdenv.mkDerivation rec {
  name = &quot;hello-2.10&quot;;

  src = fetchurl {
    url = &quot;mirror://gnu/hello/${name}.tar.gz&quot;;
    sha256 = &quot;0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i&quot;;
  };
}</code></pre>
<p>If you look through the Nixpkgs package set you’ll see that there are a lot of packages that aren’t much more complicated than this.</p>
<p>The details of the phases and their related settings are in the <a href="https://nixos.org/nixpkgs/manual">Nixpkgs manual</a>. These settings allow you to avoid adding your own <code>configurePhase</code> just to slightly amend what the generic build functionality was doing for you:</p>
<pre><code>{ stdenv, fetchurl }:

stdenv.mkDerivation rec {
  name = &quot;hello-2.10&quot;;

  src = fetchurl {
    url = &quot;mirror://gnu/hello/${name}.tar.gz&quot;;
    sha256 = &quot;0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i&quot;;
  };
  
  configureFlags = [&quot;--enable-extra-awesomeness&quot;];
}</code></pre>
<p>(This is not a real configuration flag - don’t do this)</p>
<h2 id="dealing-with-dependencies">Dealing with dependencies</h2>
<p>Nix makes a distinction between build-time and run-time dependencies.</p>
<p>Anything added to the <code>buildInputs</code> list of a package will be available at both build-time and at run-time. This will mean that there will be a link between the derivation of the package and the derivation of the dependency, which prevents the garbage collector from cleaning up dependencies that are required at run-time.</p>
<p>Anything added to the <code>nativeBuildInputs</code> list is available only at build-time.</p>
<p>If we’re build C or C++ projects, Nix will be using the <code>NIX_CFLAGS_COMPILE</code> environment variable to track include directories of dependencies and the <code>NIX_LDFLAGS</code> environment variable to track library directories of dependencies. In the common cases this is taken care of for us.</p>
<p>We’re not going to get to play with this using GNU hello, so we’re going to step things up and package GNU bc.</p>
<p>We can do some prefetching:</p>
<pre><code>&gt; nix-prefetch-url mirror://gnu/bc/bc-1.07.1.tar.gz
</code></pre>
<p>and then write <code>bc.nix</code>:</p>
<pre><code>{stdenv, fetchurl}:

stdenv.mkDerivation rec {
  name = &quot;bc-1.07.1&quot;;
  src = fetchurl {
    url = &quot;mirror://gnu/bc/${name}.tar.gz&quot;;
    sha256 = &quot;0amh9ik44jfg66csyvf4zz1l878c4755kjndq9j0270akflgrbb2&quot;;
  };
}</code></pre>
<p>and <code>default.nix</code>:</p>
<pre><code>let
  nixpkgs = import &lt;nixpkgs&gt; {};
in
  nixpkgs.callPackage ./bc.nix {}</code></pre>
<p>Let’s see how that goes:</p>
<pre><code>&gt; nix-build
...
./fix-libmath_h: line 1: ed: command not found
make[2]: *** [Makefile:632: libmath.h] Error 127
make[2]: Leaving directory '/tmp/nix-build-bc-1.07.1.drv-0/bc-1.07.1/bc'
make[1]: *** [Makefile:357: all-recursive] Error 1
make[1]: Leaving directory '/tmp/nix-build-bc-1.07.1.drv-0/bc-1.07.1'
make: *** [Makefile:297: all] Error 2
builder for ‘/nix/store/6v0ikgpdmmkr6cy2gp523lanjd5chwzb-bc-1.07.1.drv’ failed with exit code 2
error: build of ‘/nix/store/6v0ikgpdmmkr6cy2gp523lanjd5chwzb-bc-1.07.1.drv’ failed</code></pre>
<p>Ouch. We’re missing the standard text editor, which appears to be being used during build time.</p>
<p>Let’s add that in:</p>
<pre><code>{stdenv, fetchurl, ed}:

stdenv.mkDerivation rec {
  name = &quot;bc-1.07.1&quot;;
  src = fetchurl {
    url = &quot;mirror://gnu/bc/${name}.tar.gz&quot;;
    sha256 = &quot;0amh9ik44jfg66csyvf4zz1l878c4755kjndq9j0270akflgrbb2&quot;;
  };
  
  nativeBuildInputs = [ ed ];
}</code></pre>
<p>and see how much further we get:</p>
<pre><code>&gt; nix-build
...
/nix/store/1vcp949ka9qnyp6dfv4s9pgjda57vk4x-bash-4.4-p12/bin/bash: line 9: makeinfo: command not found
make[2]: *** [Makefile:320: bc.info] Error 127
make[2]: Leaving directory '/tmp/nix-build-bc-1.07.1.drv-0/bc-1.07.1/doc'
make[1]: *** [Makefile:357: all-recursive] Error 1
make[1]: Leaving directory '/tmp/nix-build-bc-1.07.1.drv-0/bc-1.07.1'
make: *** [Makefile:297: all] Error 2
builder for ‘/nix/store/8rz3nbf74ghpg3xvzs61mdh0xr131q7w-bc-1.07.1.drv’ failed with exit code 2
error: build of ‘/nix/store/8rz3nbf74ghpg3xvzs61mdh0xr131q7w-bc-1.07.1.drv’ failed</code></pre>
<p>Still no dice.</p>
<p>We can fix that up by adding <code>texinfo</code> to our build dependencies:</p>
<pre><code>{stdenv, fetchurl, ed, texinfo}:

stdenv.mkDerivation rec {
  name = &quot;bc-1.07.1&quot;;
  src = fetchurl {
    url = &quot;mirror://gnu/bc/${name}.tar.gz&quot;;
    sha256 = &quot;0amh9ik44jfg66csyvf4zz1l878c4755kjndq9j0270akflgrbb2&quot;;
  };
  
  nativeBuildInputs = [ ed texinfo ];
}</code></pre>
<p>and the result is a great success:</p>
<pre><code>&gt; nix-build
/nix/store/6axbha3n5ny261x7wms6ggsnv7p3qzc9-bc-1.07.1</code></pre>
<p>If we were curious while we were packing this up, we might have ducked into the nix-shell to have a look for any configuration options:</p>
<pre><code>&gt; nix-shell
nix-shell &gt; tar zxcf $src
nix-shell &gt; cd bc-*
nix-shell &gt; configure --help
...
Optional Packages:
  --with-readline         support fancy command input editing
...</code></pre>
<p>We can add that into the mix with the appropriate configure flag and a run-time dependency:</p>
<pre><code>{stdenv, fetchurl, ed, texinfo, readline}:

stdenv.mkDerivation rec {
  name = &quot;bc-1.07.1&quot;;
  src = fetchurl {
    url = &quot;mirror://gnu/bc/${name}.tar.gz&quot;;
    sha256 = &quot;0amh9ik44jfg66csyvf4zz1l878c4755kjndq9j0270akflgrbb2&quot;;
  };

  configureFlags = [ &quot;--with-readline&quot; ];

  nativeBuildInputs = [ ed texinfo ];
  buildInputs = [ readline ];
}</code></pre>
<p>This will give us an error during the configure step:</p>
<pre><code>&gt; nix-build
...
Using the readline library.
configure: error: readline works only with flex.
builder for ‘/nix/store/pmbdl260i2fbj0q31p36ksk8fs299pg8-bc-1.07.1.drv’ failed with exit code 1
error: build of ‘/nix/store/pmbdl260i2fbj0q31p36ksk8fs299pg8-bc-1.07.1.drv’ failed</code></pre>
<p>which we can fix by adding <code>flex</code> alongside <code>readline</code> in our run-time dependencies:</p>
<pre><code>{stdenv, fetchurl, ed, texinfo, readline, flex}:

stdenv.mkDerivation rec {
  name = &quot;bc-1.07.1&quot;;
  src = fetchurl {
    url = &quot;mirror://gnu/bc/${name}.tar.gz&quot;;
    sha256 = &quot;0amh9ik44jfg66csyvf4zz1l878c4755kjndq9j0270akflgrbb2&quot;;
  };

  configureFlags = [ &quot;--with-readline&quot; ];

  nativeBuildInputs = [ ed texinfo ];
  buildInputs = [ readline flex ];
}</code></pre>
<p>Now we have a successful build with our shiny new build of <code>bc</code>:</p>
<pre><code>&gt; nix-build
...
/nix/store/46cnc5j02pvzpcwnbdfzrzv63p91fk6w-bc-1.07.1</code></pre>
<h2 id="tidying-up">Tidying up</h2>
<p>As an aside, we can turn <code>default.nix</code> into a function with a default argument:</p>
<pre><code>{ nixpkgs ? import &lt;nixpgkgs&gt; {} }:

nixpkgs.callPackage ./hello.nix {}</code></pre>
<p>and it will behave the same way, but we now have the option to use it with different package sets if we need to.</p>
<h2 id="what-about-things-other-than-c-and-c">What about things other than C and C++ ?</h2>
<p>There are all kinds of other languages and frameworks mentioned in the Nixpgs manual other than C and C++.</p>
<p>Because this is coming from the QFPL blog, it should be no surprise that that the next language we’re going to look at is Haskell.</p>
    </div>
  </div>

  
  <div class="row">
    <div class="col my-2">
      <div class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title">
      <a href="../../../projects/infra">The Functional Infrastructure Project</a>
    </h2>
  </div>
  <div class="panel-body">
    <p>We’re using functional programming to set up the infrastructure that we use from day to day.</p>
  </div>
</div>

    </div>
  </div>
  

  
  <div class="row">
    <div class="col my-2">
      
        <div class="p-1">
  <h4 class="data61-bottom-line">
    > <a href="../../../people/dlaing">Dave Laing</a>
  </h4>
  <p>Dave is a programmer working at the Queensland Functional Programming Lab.</p>
</div>

      
    </div>
  </div>
  
</div>

          </div>
          <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li role="presentation"><a href="../../../">Home</a></li>
                  <li role="presentation"><a href="../../../location">Location</a></li>
                  <li role="presentation"><a href="../../../people">People</a></li>
                  <li role="presentation"><a href="../../../projects">Projects</a></li>
                  <li role="presentation"><a href="../../../talks">Talks</a></li>
                  <li role="presentation"><a href="../../../contact">Contact</a></li>
                  <li role="presentation"><a href="../../../archive">Archive</a></li>
              </ul>
          </div>
        </main>

        <footer class="page-footer data61-green">
            <div class="container">
              <div class="row justify-content-centre">
                <div class="col">
                  <a href="https://github.com/qfpl"><i class="text-white fa fa-github fa-2x"></i></a>
                  <a href="https://twitter.com/queenslandfplab"><i class="text-white fa fa-twitter fa-2x"></i></a>
                  <a href="mailto:contact@qfpl.io"><i class="text-white fa fa-envelope-o fa-2x"></i></a>
                </div>
                <div class="col">
                  <a class="float-right" rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
                  </a>
                </div>
              </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>

        <script src="../../../js/bootstrap.min.js"></script>

        
          
        
    </body>
</html>
