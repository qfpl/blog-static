<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Queensland FP Lab - Your First Hydra</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../../../css/custom.css">
        <link rel="stylesheet" href="../../../css/syntax.css">
        
          
        
        <link rel="alternate" type="application/atom+xml" title="Queensland Functional Programming Lab Blog" href="../../../atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Queensland Functional Programming Lab Blog" href="../../../rss.xml">
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106321204-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments)};
          gtag('js', new Date());

          gtag('config', 'UA-106321204-1', { 'anonymize_ip': true });
        </script>
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg data61-green">
              <a class="navbar-brand text-white" href="../../../">
                <div class="card brand-logo">
                  <div class="card-body">
                    <div class="brand-anim-block">
                      <img id="qfpl-brand-data61-logo" height="55" alt="Data61 logo" src="../../../images/DATA61-CSIRO_MONO.png" />
                    </div>
                  </div>
                </div>
              </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <i class="fa fa-lg fa-fw fa-bars text-white"></i>
              </button>

              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../location">Location</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../people">People</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../talks">Talks</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../contact">Contact</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../../archive">Archive</a>
                    </li>
                  </ul>
                </div>
            </nav>
        </header>

        <main>
          <div class="container-fluid">
              <div class="container">
  <div class="row">
    <div class="col my-2">
      <h2>Your First Hydra</h2>
      <small>Posted on October 10, 2018</small>
    </div>
  </div>

  <div class="row">
    <div class="col my-2">
      <p>So you’ve started using <a href="https://nixos.org/nix"><code>Nix</code></a> for your projects. The reproducible builds and dependency management are predictable and declarative. For the first time in a long time the ground at your feet feels firm, you’re no longer treading quicksand. You size up your nemesis, the Continuous Integration server, long has it defied your attempts to build as you do… No longer.</p>
<p>This post is a guide for setting up a small local instance of the <code>Nix</code> powered CI system known as <a href="https://nixos.org/hydra/">Hydra</a>:</p>
<div class="card mb-2">
<div class="card-body">
<p>
<i class="fa fa-quote-left fa-fw fa-lg"></i> Hydra is a Nix-based continuous build system, released under the terms of the GNU GPLv3 or (at your option) any later version. It continuously checks out sources of software projects from version management systems to build, test and release them. The build tasks are described using Nix expressions. This allows a Hydra build task to specify all the dependencies needed to build or test a project. It supports a number of operating systems, such as various GNU/Linux flavours, Mac OS X, and Windows. <i class="fa fa-quote-right fa-fw fa-lg"></i>
</p>
<p class="mb-0">
— <a href="https://nixos.org/hydra/">Hydra Homepage</a>
</p>
</div>
</div>
<p>Hydra is a very powerful system, and like most powerful systems it can be difficult to know where to start. This post aims to help by walking through the process of setting up a local Hydra instance on a virtual machine. This will be a stand-alone Hydra instance, only a single machine without build slaves.</p>
<p>‘Why did it need to have eight heads?’, you ponder briefly before deciding you’d rather not know…</p>
<h3 id="requirements">Requirements</h3>
<p>This tutorial was tested on a machine running <code>NixOS</code>, which is not required, but you <em>will</em> need:</p>
<ul>
<li><a href="https://nixos.org/nix"><code>Nix</code></a>,</li>
<li><a href="https://nixos.org/nixpkgs"><code>NixPkgs</code></a>,</li>
<li>and <a href="https://nixos.org/nixops"><code>NixOps</code></a>.</li>
</ul>
<p>To install <code>nixops</code>:</p>
<pre><code>$ nix-env -i nixops</code></pre>
<p>You should be comfortable writing your own <code>Nix</code> expressions and debugging failures. This is still somewhat of a bleeding edge part of the <code>Nix</code> ecosystem, so things don’t always go according to plan.</p>
<h3 id="nixops">NixOps</h3>
<p>To manage the Hydra machine, we’re going to leverage <code>NixOps</code> to create, start, run, and update our virtual machine. This lets us have a declarative defintion of our build machine, as well as a consistent interface for managing the machine(s).</p>
<p>We’re going to create two Nix expressions that together define our machine:</p>
<ol type="1">
<li>The virtual machine environment: <code>simple_hydra_vbox.nix</code></li>
<li>The configuration of the Hydra machine: <code>simple_hydra.nix</code></li>
</ol>
<h3 id="the-virtual-machine">The virtual machine</h3>
<p>This will be a ‘headless’ VirtualBox machine, with some settings to keep the overall size of the machine down, and control access.</p>
<p>We must create a <code>Nix</code> <code>attrset</code> that contains a function that defines our machine, this function is attached to a name that will be used as the <code>&lt;HOSTNAME&gt;</code> of the machine within <code>NixOps</code>. We use the name <code>my-hydra</code> here, and we’ll need to use that name again when we write our machine configuration.</p>
<pre><code># START simple_hydra_vbox.nix
{
  my-hydra =
    { config, pkgs, ... }: {</code></pre>
<p><code>NixOps</code> lets you choose your deployment target, ours being VirtualBox. <code>NixOps</code> supports quite a few different environemnts. Check out the <a href="https://nixos.org/nixops/manual/">NixOps Manual</a> for more information on supported target environments.</p>
<pre><code>      deployment.targetEnv = &quot;virtualbox&quot;;</code></pre>
<p>Click here to find out more about why <code>deployment</code> and similar attributes are used this way: <a class="btn btn-info" role="button" data-toggle="collapse" href="#attrsetAside" aria-expanded="false" aria-controls="attrsetAside"> Combining <em><em>attrset</em></em> values </a></p>
<div id="attrsetAside" class="collapse mb-3">
<div class="card card-body">
<p>Note that <code>deployment</code> is not passed in as an argument to our configuration. This is intentional and forms part of an idiomatic <code>Nix</code> technique for building and overriding attributes. We build a set of attributes with a specific predefined structure, such that <code>NixOps</code> can union them together, then act upon those settings to create the machines we’ve specified.</p>
<p>As an example, say we have some “<em>default</em>” settings in an <code>attrset</code> somewhere:</p>
<pre><code>defaultAttrs = { fieldA = &quot;generic_name&quot;;
, fieldB = &quot;foo&quot;;
, fieldC = 3000;
}</code></pre>
<p>To override only <code>fieldB</code>, we create the following <code>attrset</code> in our expression:</p>
<pre><code>myAttrs = { fieldB = &quot;bar&quot;; }</code></pre>
<p>Then when the <code>attrset</code> values are combined, the original <em>default</em> values can be overridden like so:</p>
<pre><code># (//) is a built in Nix function to union two attrsets together
defaultAttrs // myAttrs
=
{ fieldA = &quot;generic_name&quot;;
, fieldB = &quot;bar&quot;;
, fieldC = 3000;
}</code></pre>
<p>The ordering of the union matters, <code>myAttrs // defaultAttrs</code> would override the values in the other direction.</p>
<p>This is action is recursive, nested properties are combined as part of this process.</p>
<pre><code>({ fieldA = 3000; fieldB = { nestedA = &quot;foo&quot;; }; }) // ({ fieldB.nestedA = &quot;bar&quot;; })
=
{ fieldA = 3000; fieldB = { nestedA = &quot;bar&quot;; }; }</code></pre>
<p>This capability allows you to have all of the settings you want and you need only ever specify the precise ones you need to alter. This can then be combined with other <code>attrset</code>s that alter other settings for a long chain of overrides to end up with a final set. Such as this contrived example:</p>
<pre><code>defaultMachineConf
// ({ machine.cpus = 2; })          # &quot;hardware&quot; settings in one file
// ({ machine.os.user = &quot;Sally&quot;; }) # &quot;software&quot; settings in a different file</code></pre>
</div>
</div>
<p>Next we tweak the settings for our VirtualBox machine. The machine will need a fair chunk of memory, as evaluating the <code>nixpkgs</code> tree to build the dependencies can be expensive. Also, as with any CI system, you need to make sure your target has sufficient resources to actually run the required builds.</p>
<pre><code>      deployment.virtualbox.memorySize = 4096;
      deployment.virtualbox.vcpu = 2;</code></pre>
<p>Our machine doesn’t require a GUI so we enable <code>headless</code> mode. Note that these are <code>virtualbox</code> specific settings, each target will have their own collection of options, check the manual for more information.</p>
<pre><code>      deployment.virtualbox.headless = true;</code></pre>
<p>We’ll also turn off the manual from appearing on the machine, this saves a bit more space. Enable a time daemon, <code>ntp</code>. Then set the access controls for SSH, disallowing SFTP and password based authentication.</p>
<pre><code>      services.nixosManual.showManual         = false;
      services.ntp.enable                     = true;
      services.openssh.allowSFTP              = false;
      services.openssh.passwordAuthentication = false;</code></pre>
<p>Provide some settings for the users on the machine. The <code>mutableUsers</code> option means that every time the system is deployed the user information will be completely replaced by the static definition from this file. The next setting for <code>keyFiles</code> will copy the listed SSH key files to the target machine. These files are expected to exist on the machine where the <code>nixops</code> command is being executed.</p>
<p>In this example we’re going to copy across an RSA public key from a pair. This allows us to use <code>nixops ssh -d &lt;deployment-name&gt; &lt;machine-name&gt;</code> to access the machine via SSH.</p>
<pre><code>      users = {
        mutableUsers = false;
        users.root.openssh.authorizedKeys.keyFiles = [ ~/.ssh/id_rsa.pub ];
      };
    };
}
# END: simple_hydra_vbox.nix</code></pre>
<p>We put all of the above in a file named <code>simple_hydra_vbox.nix</code>, this will form part of the input for our <code>nixops</code> command later on. This is the machine, but that isn’t much good without a definition for what will actually be <em>running</em> on the machine.</p>
<p>Next we will define the <code>NixOS</code> configuration that will be running on our virtual machine. This will include our Hydra service, as well as its dependencies, with some caveats because we’re running on a local, virtualised machine.</p>
<h3 id="a-basic-hydra">A basic Hydra</h3>
<p>What follows is a <code>NixOps</code> expression for a stand-alone Hydra build machine. It is enough to start your own Hydra instance with a binary cache. There are sections which are required for running this machine in a virtual machine, but we’ll cover those as we go. This machine will not define any build slaves, so it will build and cache everything locally.</p>
<p>As before, we write a <code>nix</code> expression that is a function that produces an <code>attrset</code> that is our machine configuration. Note that the name we assign matches the virualisation configuration. This must match otherwise there will be an error from <code>NixOps</code>. This attribute is also used as the machine hostname, so any where you see <code>&lt;HOSTNAME&gt;</code> in the configuration, replace that with this name.</p>
<pre><code># START: simple_hydra.nix
{
  my-hydra =
    { config, pkgs, ...}: {</code></pre>
<p>As Hydra is able to send emails for various events, so our machine will need some sort of email capability. We enable the <code>postfix</code> service like so:</p>
<pre><code>services.postfix = {
    enable = true;
    setSendmail = true;
};</code></pre>
<p>The build queues and parts of the jobset configuration are all stored in a postgres database on the Hydra machine, so we need to enable the <code>postgresql</code> service, and provide a mapping from <code>NixOS</code> users to <code>PostgreSQL</code> users. Otherwise Hydra won’t be able to access the database!</p>
<pre><code>services.postgresql = {
    enable = true;
    package = pkgs.postgresql;
    identMap =
      ''
        hydra-users hydra hydra
        hydra-users hydra-queue-runner hydra
        hydra-users hydra-www hydra
        hydra-users root postgres
        hydra-users postgres postgres
      '';
};</code></pre>
<p>This next setting is to tell <code>NixOS</code> to open the TCP port for accessing the web interface for Hydra. We don’t need to add the <code>postfix</code> or <code>ssh</code> ports in this instance because the <code>postfix</code> and <code>virtualisation</code> configuration will add them on our behalf.</p>
<pre><code>    networking.firewall.allowedTCPPorts = [ config.services.hydra.port ];</code></pre>
<p>Now onto the meaty part of creating our Hydra configuration, enabling and configuring the <code>hydra</code> service.</p>
<pre><code>services.hydra = {
    enable = true;
    useSubstitutes = true;
    hydraURL = &quot;https://hydra.example.org&quot;;
    notificationSender = &quot;hydra@example.org&quot;;</code></pre>
<p>The <code>hydraURL</code> setting is the page for the Hydra web interface, it is also used in emails as the root path for any links back to this Hydra system. The <code>notificationSender</code> setting is the <strong><code>from</code></strong> address of emails from this Hydra system.</p>
<p>The <code>useSubstitutes</code> setting is a bit more complicated, it’s best to refer to the manual for this one:</p>
<div class="card mb-2">
<div class="card-body">
<p>
<i class="fa fa-quote-left fa-fw fa-lg"></i> Whether to use binary caches for downloading store paths. Note that binary substitutions trigger (a potentially large number of) additional HTTP requests that slow down the queue monitor thread significantly. Also, this Hydra instance will serve those downloaded store paths to its users with its own signature attached as if it had built them itself, so don’t enable this feature unless your active binary caches are absolute trustworthy. <i class="fa fa-quote-right fa-fw fa-lg"></i>
</p>
<p class="mb-0">
— From <a href="https://nixos.org/nixos/options.html#services.hydra.usesubstitutes" class="uri">https://nixos.org/nixos/options.html#services.hydra.usesubstitutes</a>
</p>
</div>
</div>
<p>With this enabled you will reap the benefits of cached build dependencies, and spare precious cycles by not constantly rebuilding things unnecessarily. The trade-off is a higher number of HTTP requests that may affect the running of the Hydra system. Depending on how active your build queue is, this may be a cause for concern.</p>
<p>The following setting is important and required (even though it is empty). Because we’re building a stand-alone Hydra instance, we have to explicitly tell Hydra that there are no additional build machines.</p>
<p>Without this setting, Hydra will ignore our <code>machine</code> definition and the Hydra instance will be not function, as the expected build machine configuration does not match reality.</p>
<pre><code>buildMachinesFiles = [];</code></pre>
<p>We provide some <code>extraConfig</code> to tell Hydra where to place the binary cache and its secret key.</p>
<pre><code>    extraConfig = ''
      store_uri = file:///var/lib/hydra/cache?secret-key=/etc/nix/&lt;HOSTNAME&gt;/secret
      binary_cache_secret_key_file = /etc/nix/&lt;HOSTNAME&gt;/secret
      binary_cache_dir = /var/lib/hydra/cache
    '';
};</code></pre>
<p>See here (<a href="https://nixos.org/nixos/options.html#services.hydra" class="uri">https://nixos.org/nixos/options.html#services.hydra</a>) for a full list of the options available for the Hydra service in <code>NixOS</code>.</p>
<p>The following is optional, but here for example purposes about using <code>nginx</code> to serve the Hydra web interface. This Hydra instance was running locally in a VM so this isn’t needed. The full scope of Nginx configuration and what is available via <code>NixOS</code> is well beyond this post.</p>
<pre><code>services.nginx = {
  enable = true;
  recommendedProxySettings = true;
  virtualHosts.&quot;hydra.example.org&quot; = {
    forceSSL = true;
    enableACME = true;
    locations.&quot;/&quot;.proxyPass = &quot;http://localhost:3000&quot;;
  };
};</code></pre>
<p>Hydra requires manual steps beyond the configuration of the <code>NixOS</code> service. This is only a one time requirement however. We leverage a <code>systemd</code> service to manage the timing. This provides some additional guarantees that the final configuration will run <em>after</em> Hydra is installed and running.</p>
<p>The initial options describe our <code>systemd</code> service.</p>
<pre><code>systemd.services.hydra-manual-setup = {
  description = &quot;Create Admin User for Hydra&quot;;
  serviceConfig.Type = &quot;oneshot&quot;;
  serviceConfig.RemainAfterExit = true;
  wantedBy = [ &quot;multi-user.target&quot; ];
  requires = [ &quot;hydra-init.service&quot; ];
  after = [ &quot;hydra-init.service&quot; ];
  environment = builtins.removeAttrs (config.systemd.services.hydra-init.environment) [&quot;PATH&quot;];</code></pre>
<p>This is the script that is run by the <code>systemd</code> service, we predicate its execution on the lack of existence of a <code>.setup-is-complete</code> file.</p>
<pre><code>  script = ''
    if [ ! -e ~hydra/.setup-is-complete ]; then
      # create signing keys
      /run/current-system/sw/bin/install -d -m 551 /etc/nix/&lt;HOSTNAME&gt;
      /run/current-system/sw/bin/nix-store --generate-binary-cache-key &lt;HOSTNAME&gt; /etc/nix/&lt;HOSTNAME&gt;/secret /etc/nix/&lt;HOSTNAME&gt;/public
      /run/current-system/sw/bin/chown -R hydra:hydra /etc/nix/&lt;HOSTNAME&gt;
      /run/current-system/sw/bin/chmod 440 /etc/nix/&lt;HOSTNAME&gt;/secret
      /run/current-system/sw/bin/chmod 444 /etc/nix/&lt;HOSTNAME&gt;/public
      # create cache
      /run/current-system/sw/bin/install -d -m 755 /var/lib/hydra/cache
      /run/current-system/sw/bin/chown -R hydra-queue-runner:hydra /var/lib/hydra/cache
      # done
      touch ~hydra/.setup-is-complete
    fi
  '';
};</code></pre>
<p>As we indicated in the <code>extraConfig</code> section of Hydra, we’re going to have a binary store on this machine. This is where we generate the secret keys and create the cache itself.</p>
<p>Next we set some explict <code>nix</code> settings to schedule the garbage collection of the store, and also the automatically optimise the store. Which means that <code>nix</code> will remove duplicate files from the store, replacing them with hard links to a single copy.</p>
<pre><code>nix.gc = {
  automatic = true;
  dates = &quot;15 3 * * *&quot;; # [1]
};

nix.autoOptimiseStore = true;</code></pre>
<p>[1]: This is a <a href="https://crontab.guru/#15_3_*_*_*">CRON</a> schedule. To find out more you can read the <a href="http://man7.org/linux/man-pages/man5/crontab.5.html"><code>crontab</code> manual</a> or the <a href="https://en.wikipedia.org/wiki/Cron">Wikipedia page</a>.</p>
<p>This tells <code>NixOS</code> which users have privileged access to the <code>nix</code> store. These users can add additional binary caches.</p>
<pre><code>nix.trustedUsers = [&quot;hydra&quot; &quot;hydra-evaluator&quot; &quot;hydra-queue-runner&quot;];</code></pre>
<p>Lastly, we define a build machine so that Hydra has a ‘builder’ to orchestrate. In this example we’re building locally so our <code>hostName</code> is “localhost”. Use the <code>systems</code> setting to indicate the systems or architectures that we able to build for, and <code>maxJobs</code> to set the maximum number of build processes.</p>
<p>The <code>supportedFeatures</code> property is a list of “Features” that this build machine supports. If a derivation is submitted that requires features that are not supported on a given build machine then it will not be built.</p>
<pre><code>    nix.buildMachines = [
      {
        hostName = &quot;localhost&quot;;
        systems = [ &quot;x86_64-linux&quot; &quot;i686-linux&quot; ];
        maxJobs = 6;
        # for building VirtualBox VMs as build artifacts, you might need other
        # features depending on what you are doing
        supportedFeatures = [ ];
      }
    ];
  };
}
# END: simple_hydra.nix</code></pre>
<p>Place all of that configuration in a file called <code>simple_hydra.nix</code> and we’re ready to start powering up.</p>
<h4 id="powering-up">Powering up</h4>
<p>Now to tell <code>NixOps</code> to create and prepare our Hydra instance.</p>
<pre><code>$ cd &lt;path to 'simple_hydra' nix files&gt;
$ ls
simple_hydra.nix simple_hydra_vbox.nix</code></pre>
<h5 id="create-the-virtual-machine-within-nixops">Create the virtual machine within NixOps</h5>
<p>Before you can initiate the deployment of your machine, you need to make <code>NixOps</code> aware of it:</p>
<pre><code>$ nixops create ./simple_hydra.nix ./simple_hydra_vbox.nix -d simple_hydra
created deployment ‘5537e9ba-cabf-11e8-80d0-d481d7517abf’ ### This hash will differ for your machine
5537e9ba-cabf-11e8-80d0-d481d7517abf</code></pre>
<p>If that completes without error, you can then issue the following command to <code>NixOps</code>:</p>
<pre><code>$ nixops info -d simple_hydra
Network name: simple_hydra
Network UUID: 5537e9ba-cabf-11e8-80d0-d481d7517abf
Network description: Unnamed NixOps network
Nix expressions: /path/to/simple_hydra.nix /path/to/simple_hydra_vbox.nix

+----------|---------------|------------|-------------|------------+
| Name     |     Status    | Type       | Resource Id | IP address |
+----------|---------------|------------|-------------|------------+
| my-hydra | Missing / New | virtualbox |             |            |
+----------|---------------|------------|-------------|------------+</code></pre>
<p>The <code>info</code> command for <code>nixops</code> provides feedback about the status of the machine(s) you have deployed, their IP addresses, and other things. This information is stored local to the machine that issued this <code>nixops</code> command, it is not shared between different deployment machines. So even if you deploy to AWS using <code>nixops</code>, someone else that has the same <code>nix</code> expressions for your machines will not have any knowledge of their deployment status.</p>
<h5 id="trigger-deployment-of-the-virtual-machine">Trigger deployment of the virtual machine</h5>
<p>We can now instruct <code>nixops</code> to deploy the configuration to the virtual machines, this part may take a while. Once you issue the following command you might like to get up, go for a walk, stretch, or fetch a cup of your favourite beverage.</p>
<p>The <code>--force-reboot</code> is required to ensure the Hydra services are started correctly. If you do not include it then the deployment phase will complete successfully, but it will report an error as not all of the services will have been initiated correctly.</p>
<p>This flag is also required to ensure everything ticks over as required, as there are some <code>nixops</code> errors that can be avoided or resolved by simply power cycling the machine.</p>
<p>The <code>-d</code> flag that we give to <code>nixops</code> is to specify the name of the deployment we want to use. If you don’t provide this information then <code>nixops</code> will return this error:</p>
<pre><code>error: state file contains multiple deployments, so you should specify which one to use using ‘-d’, or set the environment variable NIXOPS_DEPLOYMENT</code></pre>
<p>You can set the environment variable <code>NIXOPS_DEPLOYMENT</code> to <code>simple_hydra</code> if you don’t want to include the <code>-d simple_hydra</code> information every time.</p>
<pre><code>$ nixops deploy -d simple_hydra --force-reboot</code></pre>
<p><code>NixOps</code> will then set about the task of creating the virtual machine, initialising all the disks and network settings etc. Then create the configuration prior to copying to the newly minted machine. After which, barring any errors you should see the following text:</p>
<pre><code>my-hydra&gt; waiting for the machine to finish rebooting...[down]......................[up]
my-hydra&gt; activation finished successfully
simple_hydra&gt; deployment finished successfully</code></pre>
<p>Once the machine is deployed, you can check its status using <code>$ nixops info -d simple_hydra</code>.</p>
<pre><code>Network name: simple_hydra
Network UUID: c58245b3-cac2-11e8-9346-d481d7517abf
Network description: Unnamed NixOps network
Nix expressions: /path/to/simple_hydra.nix /path/to/simple_hydra_vbox.nix

+----------|-----------------|------------|------------------------------------------------------|----------------+
| Name     |      Status     | Type       | Resource Id                                          | IP address     |
+----------|-----------------|------------|------------------------------------------------------|----------------+
| my-hydra | Up / Up-to-date | virtualbox | nixops-c58245b3-cac2-11e8-9346-d481d7517abf-my-hydra | 192.168.56.101 |
+----------|-----------------|------------|------------------------------------------------------|----------------+</code></pre>
<p>This deployment depends on the files containing the nix expressions that were used to create it, if those files disappear then you will not be able to destroy or update the machine via <code>NixOps</code>.</p>
<p>If you want to make changes to these machines or configuration, such as adding a CPU, or changing the port for the Hydra web UI. You make the change in those files, then you only have to issue the <code>deploy</code> command again and <code>NixOps</code> will take care of updating the machine to the new configuration.</p>
<p>If things have gone according to plan then you should be able to point a browser at the Hydra web UI. The address will be the default Hydra port of <code>3000</code> at the IP address listed in the output from the <code>nixops info -d simple_hydra</code> command. Using the above output as an example the address for the Hydra page is: <code>http://192.168.56.101:3000</code>.</p>
<p>We’re ready to start adding some <code>jobsets</code> to our Hydra instance, but there is a bit of a problem as we can’t login to our Hydra UI.</p>
<h3 id="add-the-hydra-admin-user">Add the Hydra admin user</h3>
<p>To add an admin user to Hydra, we use the <code>hydra-create-user</code> program that is included with Hydra. This must be run on the Hydra machine itself, so we need to SSH in or send the instruction over SSH.</p>
<p>The <code>nixops ssh</code> command lets you do both:</p>
<p>To run the command on the machine by connecting first, run the following:</p>
<pre><code>$ nixops ssh -d simple_hydra my-hydra</code></pre>
<p>This will put you on the machine at a prompt as <code>root</code> on the <code>my-hydra</code> machine from the <code>simple_hydra</code> deployment. You can then run the create user command.</p>
<p>Alternatively you can simply send the single instruction over SSH:</p>
<pre><code>$ nixops ssh -d simple_hydra my-hydra '&lt;create-user-command&gt;'
creating new user `admin`</code></pre>
<p>Putting the command in quotes after the <code>nixops ssh</code> command will send a single instruction over SSH, printing any output.</p>
<p>The command to create an admin user is:</p>
<pre><code>$ hydra-create-user admin \
    --full-name &quot;Bobby Admin&quot; \
    --email-address &quot;bobby@example.org&quot; \
    --password TOTALLYSECURE \
    --role admin</code></pre>
<p>With successful output being something like:</p>
<pre><code>creating new user `admin`</code></pre>
<p>You can now use these credentials to login to your Hydra web UI and start configuring your jobsets and other Hydra settings. We plan to cover more information about writing your own declarative jobsets in a future post.</p>
<p>For the impatient or curious, you can have a look at the setup we use at QFPL for some of our projects:</p>
<ul>
<li><a href="https://github.com/qfpl/waargonaut/tree/master/ci">Waargonaut</a></li>
<li><a href="https://github.com/qfpl/digit/tree/master/ci">Digit</a></li>
<li><a href="https://github.com/qfpl/sv/tree/master/ci">SV</a></li>
</ul>
<h3 id="one-prepared-earlier">One prepared earlier</h3>
<p>If you’re looking at most of the code in this post and thinking that all it would take is some simple inputs and interpolation then you wouldn’t need to write the same <code>nix</code> expressions every time. You would be correct, and Will Fancher - known in some online communities as <code>ElvishJerricco</code> - has done just that in <a href="https://github.com/ElvishJerricco/simple-hydra"><code>simple-hydra</code></a>. This repo has a version of what we have worked through here, but abstracted so that you need only specify some choices along with hostnames etc.</p>
<p>For the sake of your own understanding, it is useful to work through it yourself first so you can more readily experiment and learn about the inner workings.</p>
<h3 id="going-further">Going further</h3>
<p>For those looking for more advanced Hydra information, the <a href="https://github.com/peti/hydra-tutorial">Hydra Tutorial</a> by Github user <code>peti</code> is an excellent resource. The tutorial is also worked through as part of a presentation linked to from the repo: (<a href="https://www.youtube.com/watch?v=RXV0Y5Bn-QQ">presentation link</a>).</p>
<p>That tutorial sets up a more complex Hydra configuration with a ‘master’ machine and ‘slave’ machines, in what would more closely resemble a real world setup.</p>
    </div>
  </div>

  
  <div class="row">
    <div class="col my-2">
      <div class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title">
      <a href="../../../projects/infra">The Functional Infrastructure Project</a>
    </h2>
  </div>
  <div class="panel-body">
    <p>We’re using functional programming to set up the infrastructure that we use from day to day.</p>
  </div>
</div>

    </div>
  </div>
  

  
  <div class="row">
    <div class="col my-2">
      
        <div class="p-1">
  <h4 class="data61-bottom-line">
    > <a href="../../../people/schalmers">Sean Chalmers</a>
  </h4>
  <p>Likes riding motorcycles, lenses, text editors, software that works, and writing documentation. Hates not having errors as values, not being able to use lenses, and writing bios.</p>
</div>

      
    </div>
  </div>
  
</div>

          </div>
          <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li role="presentation"><a href="../../../">Home</a></li>
                  <li role="presentation"><a href="../../../location">Location</a></li>
                  <li role="presentation"><a href="../../../people">People</a></li>
                  <li role="presentation"><a href="../../../projects">Projects</a></li>
                  <li role="presentation"><a href="../../../talks">Talks</a></li>
                  <li role="presentation"><a href="../../../contact">Contact</a></li>
                  <li role="presentation"><a href="../../../archive">Archive</a></li>
              </ul>
          </div>
        </main>

        <footer class="page-footer data61-green">
            <div class="container">
              <div class="row justify-content-centre">
                <div class="col">
                  <a href="https://github.com/qfpl"><i class="text-white fa fa-github fa-2x"></i></a>
                  <a href="https://twitter.com/queenslandfplab"><i class="text-white fa fa-twitter fa-2x"></i></a>
                  <a href="mailto:contact@qfpl.io"><i class="text-white fa fa-envelope-o fa-2x"></i></a>
                </div>
                <div class="col">
                  <a class="float-right" rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
                  </a>
                </div>
              </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>

        <script src="../../../js/bootstrap.min.js"></script>

        
          
        
    </body>
</html>
