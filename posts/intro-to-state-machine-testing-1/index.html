<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Queensland FP Lab - Introduction to state machine testing: part 1</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../../css/custom.css">
        <link rel="stylesheet" href="../../css/syntax.css">
        
          
        
        <link rel="alternate" type="application/atom+xml" title="Queensland Functional Programming Lab Blog" href="../../atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Queensland Functional Programming Lab Blog" href="../../rss.xml">
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106321204-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments)};
          gtag('js', new Date());

          gtag('config', 'UA-106321204-1', { 'anonymize_ip': true });
        </script>
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg data61-green">
              <a class="navbar-brand text-white" href="../../">
                <div class="card brand-logo">
                  <div class="card-body">
                    <div class="brand-anim-block">
                      <img id="qfpl-brand-data61-logo" height="55" alt="Data61 logo" src="../../images/DATA61-CSIRO_MONO.png" />
                    </div>
                  </div>
                </div>
              </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <i class="fa fa-lg fa-fw fa-bars text-white"></i>
              </button>

              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../location">Location</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../people">People</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../talks">Talks</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../contact">Contact</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../archive">Archive</a>
                    </li>
                  </ul>
                </div>
            </nav>
        </header>

        <main>
          <div class="container-fluid">
              <div class="container">
  <div class="row">
    <div class="col my-2">
      <h2>Introduction to state machine testing: part 1</h2>
      <small>Posted on October 29, 2018</small>
    </div>
  </div>

  <div class="row">
    <div class="col my-2">
      <p>Testing can be hard. When working in a conventional imperative setting you end up with a soup of mocks and stubs to hide the state you don’t care about, and that gets old fast. When employing functional programming however, referential transparency has your back. Functions always produce the same outputs given the same inputs. We can choose, or better yet randomly generate, inputs for each function, and check that the corresponding outputs match our expectations. Simple. That is until we realise that we need to test more than each function in isolation. Yes, each function must work as expected, but the system formed through composition of these functions must also be tested to ensure that our system does “the right thing”. This puts us at the boundaries of our application, where we no longer talk about function calls, but HTTP endpoints and database connections. We’re back in the land of mutable state. Not only that — we’re often in the world of mutable state with concurrency. What is a functional programmer to do?!</p>
<p>Fear not. We can test it; we have the technology.</p>
<h2 id="state-machine-testing">State machine testing</h2>
<p>One solution to this problem — one with a great power-to-weight ratio — is state machine testing. The short version is:</p>
<ul>
<li>Model an application’s state as a data type.</li>
<li>Model the inputs that can change the system’s state as data types.</li>
<li>Specify how each model input can be executed against the system.</li>
<li>Specify how to update the model state given outputs from the system.</li>
<li>Write properties to test that the system behaviour and state match the model.</li>
</ul>
<p>It gets better. There’s an excellent property-based testing library called <a href="https://github.com/hedgehogqa/haskell-hedgehog"><code>hedgehog</code></a> that includes facilities for state machine testing. One issue with <code>hedgehog</code> is that it’s state machine testing capabilities aren’t documented in detail, which can make it a little difficult to get started. I gave a talk on this topic at <a href="http://lambdajam.yowconference.com.au/">YOW! Lambda Jam</a> (<a href="../../share/talks/state-machine-testing/">slides</a> and <a href="https://www.youtube.com/watch?v=boBD1qhCQ94">video</a>) in May of 2018, however the 25 minute talk slot made for a fairly fast paced talk. I’m now going to build upon that talk and provide a slower-paced introduction over a series of blog posts.</p>
<h2 id="parallel-state-machine-testing-an-example">Parallel state machine testing: an example</h2>
<p>Now that you hopefully have a high level understanding of the problem we’re trying to solve, I’d like to proceed with an example to whet your appetite for what’s to come in this and future posts. If this example doesn’t make sense, sit tight. Future posts are going to build up piece by piece.</p>
<p>Earlier in 2018 I did some work using state machine testing to test <a href="https://wordpress.org/">WordPress</a>. I did this for two reasons. Firstly, I wanted to demonstrate that these techniques can be employed to test software that doesn’t use functional programming. Secondly, I wanted to investigate how Haskell may be used to test messy APIs designed with dynamic programming languages in mind.</p>
<p>The <a href="https://core.trac.wordpress.org/ticket/44568">first bug</a> I found during this testing is a good example of the power of state machine testing. After modelling a part of WordPress’ API and state, hedgehog was able to find a concurrency issue and provide a minimal example to reproduce the issue. As a side note, I later found out that <a href="https://core.trac.wordpress.org/ticket/44568#comment:1">WordPress isn’t at all thread safe</a>, which makes this result unsurprising.</p>
<p>Here’s part of the output:</p>
<pre><code>112 ┃     f cs s = forAll $ Gen.parallel (Range.linear 1 100) (Range.linear 1 10) s cs
              ┃     │ ━━━ Prefix ━━━
              ┃     │ Var 25 = CreatePost
              ┃     │            (fromList
              ┃     │               [ PostDateGmt :=&gt; Identity 1900 (-01) (-01) 12 : 00 : 00
              ┃     │               , PostSlug :=&gt; Identity (Slug &quot;a&quot;)
              ┃     │               , PostStatus :=&gt; Identity Publish
              ┃     │               , PostTitle :=&gt; Identity (R (L (RCreate &quot;a&quot;)))
              ┃     │               , PostContent :=&gt; Identity (RP (L (PRCreate &quot;a&quot;)))
              ┃     │               , PostAuthor :=&gt; Identity (Author 1)
              ┃     │               , PostExcerpt :=&gt; Identity (RP (L (PRCreate &quot;a&quot;)))
              ┃     │               ])
              ┃     │ 
              ┃     │ ━━━ Branch 1 ━━━
              ┃     │ Var 26 = DeletePost (Var 25) Nothing
              ┃     │ Var 27 = DeletePost (Var 25) Nothing
              ┃     │ 
              ┃     │ ━━━ Branch 2 ━━━
              ┃     │ Var 28 = DeletePost (Var 25) (Just True)

...

no valid interleaving</code></pre>
<p>So what is this telling us? The last line is telling us that there’s “no valid interleaving”. This is because we ran the tests in parallel, and no matter how we interleave the inputs the system’s outputs always fail to match our expectations.</p>
<p>In the other output, hedgehog has provided a minimal example to reproduce the issue:</p>
<ul>
<li>Create a post without any other parallel actions (the <code>Prefix</code>).</li>
<li>Run two delete actions that send the post to the trash (<code>Nothing</code> argument) on one parallel branch (<code>Branch 1</code>).</li>
<li>Run a delete action that actually deletes the post (<code>Just True</code> argument) on the second parallel branch (<code>Branch 2</code>).</li>
</ul>
<p>In short — hedgehog has run random sequences of web requests, in parallel, with random values for the inputs, and found a concurrency bug. Not only that, it has then shrunk both the sequence of web requests <em>and</em> their inputs to provide a small (possibly minimal) example that still results in failure. It gets even better: hedgehog provides the random seed and other relevant information that produced the failure, so when we attempt a fix, we can re-run this exact test and ensure the fix has worked.</p>
<p>If, like me, you find all of this terribly exciting and would like to know more — stay tuned. The rest of this post is a very brief overview of the prerequisites for state machine testing (state machines and property based testing), but we’ll start to get into the nuts and bolts of state machine testing with hedgehog in the next post.</p>
<h2 id="state-machines">State machines</h2>
<p>Before we talk about how state machine <em>testing</em> works, let’s talk about state machines. If you’ve seen state machines before, it’s likely you’ve come across state machine diagrams like the one below for a turnstile. You’ll see it comprises:</p>
<ul>
<li>A set of states: <code>{Locked, Unlocked}</code>.</li>
<li>A set of inputs that cause state transitions: <code>{Push, Coin}</code></li>
<li>An initial state: <code>Locked</code></li>
</ul>
<p><img src="../../../images/posts/state-machine-testing/turnstile.png" alt="turnstile state machine
from Wikipedia" /></p>
<p>If you squint a little, it seems that many common systems are state machines. Especially when one considers that we’re not limited to finite state machines. That is, we’re not limited to testing systems with a finite number of states and inputs. Conversely, while systems with infinte state spaces are testable, we aren’t required to model the entire state space. As we’ll see in subsequent posts, we can start by modelling and testing a small subset of a system’s state space and build up from there.</p>
<p>One common example of state machines being applied in software is video games. In the case of a role playing game (RPG), for example, the states could comprise the product of the player’s position in the world, inventory, health, and current quest. The inputs could be controller inputs from the player, or more abstract actions such as “pick up item”. Finally, the initial state would be whatever initial values the game starts with.</p>
<p>We don’t all get to work on games, so what about the humble web application? It’s also a state machine. Its states are the product of possible values in persisted storage and memory; its inputs are HTTP requests; and its initial state is whatever state it’s in after a clean start.</p>
<h2 id="property-based-testing">Property based testing</h2>
<p>Now that we understand state machines, and have established that many common applications can be modelled using state machines, let’s refresh our memory on property based testing. To begin, let’s consider the canonical example of reversing a list.</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb2-1"><a href="#cb2-1"></a><span class="co">-- Reverse is involutive</span></span>
<span id="cb2-2"><a href="#cb2-2"></a><span class="ot">propReverse ::</span> <span class="dt">Property</span></span>
<span id="cb2-3"><a href="#cb2-3"></a>propReverse <span class="ot">=</span></span>
<span id="cb2-4"><a href="#cb2-4"></a>  property <span class="op">$</span> <span class="kw">do</span></span>
<span id="cb2-5"><a href="#cb2-5"></a>    xs <span class="ot">&lt;-</span> forAll <span class="op">$</span> Gen.list (Range.linear <span class="dv">0</span> <span class="dv">100</span>) Gen.alpha</span>
<span id="cb2-6"><a href="#cb2-6"></a>    <span class="fu">reverse</span> (<span class="fu">reverse</span> xs) <span class="op">===</span> xs</span></code></pre></div>
<p>This function defines a property. It uses a generator — <code>Gen.list (Range.linear 0 100) Gen.alpha</code> — to generate a random list of characters that is between 0 and 100 elements long. This random input is then used to test the property that the result of reversing the list twice, is always equal to the original list: <code>reverse (reverse xs) === xs</code>. It’s a small example, but this aptly captures the essence of property based testing. Generate random inputs, then test that some properties hold for a function given each of those inputs.</p>
<p>Another, more interesting example of a property based test comes from a colleague’s project, <a href="https://github.com/qfpl/hpython"><code>hpython</code></a>. This code is randomly generating python expressions, and then ensuring that the <code>python</code> interpreter agrees with <code>hpython</code>’s notion of validity.</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb3-1"><a href="#cb3-1"></a><span class="ot">syntax_expr ::</span> <span class="dt">FilePath</span> <span class="ot">-&gt;</span> <span class="dt">Property</span></span>
<span id="cb3-2"><a href="#cb3-2"></a>syntax_expr path <span class="ot">=</span></span>
<span id="cb3-3"><a href="#cb3-3"></a>  property <span class="op">$</span> <span class="kw">do</span></span>
<span id="cb3-4"><a href="#cb3-4"></a>    ex <span class="ot">&lt;-</span> forAll <span class="op">$</span> Gen.resize <span class="dv">300</span> General.genExpr</span>
<span id="cb3-5"><a href="#cb3-5"></a>    <span class="kw">let</span> rex <span class="ot">=</span> showExpr ex</span>
<span id="cb3-6"><a href="#cb3-6"></a>    shouldSucceed <span class="ot">&lt;-</span></span>
<span id="cb3-7"><a href="#cb3-7"></a>      <span class="kw">case</span> validateExprIndentation' ex <span class="kw">of</span></span>
<span id="cb3-8"><a href="#cb3-8"></a>        <span class="dt">Failure</span> errs <span class="ot">-&gt;</span> annotateShow errs <span class="op">$&gt;</span> <span class="dt">False</span></span>
<span id="cb3-9"><a href="#cb3-9"></a>        <span class="dt">Success</span> res <span class="ot">-&gt;</span></span>
<span id="cb3-10"><a href="#cb3-10"></a>          <span class="kw">case</span> validateExprSyntax' res <span class="kw">of</span></span>
<span id="cb3-11"><a href="#cb3-11"></a>            <span class="dt">Failure</span> errs'' <span class="ot">-&gt;</span> annotateShow errs'' <span class="op">$&gt;</span> <span class="dt">False</span></span>
<span id="cb3-12"><a href="#cb3-12"></a>            <span class="dt">Success</span> _ <span class="ot">-&gt;</span> <span class="fu">pure</span> <span class="dt">True</span></span>
<span id="cb3-13"><a href="#cb3-13"></a>    annotateShow rex</span>
<span id="cb3-14"><a href="#cb3-14"></a>    runPython3</span>
<span id="cb3-15"><a href="#cb3-15"></a>      path</span>
<span id="cb3-16"><a href="#cb3-16"></a>      shouldSucceed</span>
<span id="cb3-17"><a href="#cb3-17"></a>      rex</span></code></pre></div>
<p>We’ll see plenty more examples of properties as we work through state machine testing. If you want to dive a little deeper into property testing before continuing you can take a look at <a href="https://github.com/hedgehogqa/haskell-hedgehog/tree/master/hedgehog-example/src/Test/Example">the examples in the Hedgehog repo</a>.</p>
<h2 id="the-end-for-now">The end… for now</h2>
<p>That’s it! Hopefully I’ve managed to get you excited about state machine testing, and at least point to the concepts you’ll want to be comfortable with before proceeding. Next time we’ll start to look at how state machine testing in hedgehog actually works.</p>
    </div>
  </div>

  
  <div class="row">
    <div class="col my-2">
      
    </div>
  </div>
  

  
  <div class="row">
    <div class="col my-2">
      
        <div class="p-1">
  <h4 class="data61-bottom-line">
    > <a href="../../people/ajmcmiddlin">Andrew McMiddlin</a>
  </h4>
  <p>Andrew digs referential transparency, static typing, coffee, and table tennis.</p>
</div>

      
    </div>
  </div>
  
</div>

          </div>
          <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li role="presentation"><a href="../../">Home</a></li>
                  <li role="presentation"><a href="../../location">Location</a></li>
                  <li role="presentation"><a href="../../people">People</a></li>
                  <li role="presentation"><a href="../../projects">Projects</a></li>
                  <li role="presentation"><a href="../../talks">Talks</a></li>
                  <li role="presentation"><a href="../../contact">Contact</a></li>
                  <li role="presentation"><a href="../../archive">Archive</a></li>
              </ul>
          </div>
        </main>

        <footer class="page-footer data61-green">
            <div class="container">
              <div class="row justify-content-centre">
                <div class="col">
                  <a href="https://github.com/qfpl"><i class="text-white fa fa-github fa-2x"></i></a>
                  <a href="https://twitter.com/queenslandfplab"><i class="text-white fa fa-twitter fa-2x"></i></a>
                  <a href="mailto:contact@qfpl.io"><i class="text-white fa fa-envelope-o fa-2x"></i></a>
                </div>
                <div class="col">
                  <a class="float-right" rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
                  </a>
                </div>
              </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>

        <script src="../../js/bootstrap.min.js"></script>

        
          
        
    </body>
</html>
