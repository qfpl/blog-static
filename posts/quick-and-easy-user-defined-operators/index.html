<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Queensland FP Lab - Quick and easy user-defined operators with Plated</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../../css/custom.css">
        <link rel="stylesheet" href="../../css/syntax.css">
        
          
        
        <link rel="alternate" type="application/atom+xml" title="Queensland Functional Programming Lab Blog" href="../../atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Queensland Functional Programming Lab Blog" href="../../rss.xml">
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106321204-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments)};
          gtag('js', new Date());

          gtag('config', 'UA-106321204-1', { 'anonymize_ip': true });
        </script>
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg data61-green">
              <a class="navbar-brand text-white" href="../../">
                <div class="card brand-logo">
                  <div class="card-body">
                    <div class="brand-anim-block">
                      <img id="qfpl-brand-data61-logo" height="55" alt="Data61 logo" src="../../images/DATA61-CSIRO_MONO.png" />
                    </div>
                  </div>
                </div>
              </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <i class="fa fa-lg fa-fw fa-bars text-white"></i>
              </button>

              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../location">Location</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../people">People</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../talks">Talks</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../contact">Contact</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../archive">Archive</a>
                    </li>
                  </ul>
                </div>
            </nav>
        </header>

        <main>
          <div class="container-fluid">
              <div class="container">
  <div class="row">
    <div class="col my-2">
      <h2>Quick and easy user-defined operators with Plated</h2>
      <small>Posted on November 17, 2017</small>
    </div>
  </div>

  <div class="row">
    <div class="col my-2">
      <p>All <a href="https://en.wikipedia.org/wiki/Infix_notation">infix</a> operators have <a href="https://en.wikipedia.org/wiki/Order_of_operations">precedence</a> and <a href="https://en.wikipedia.org/wiki/Operator_associativity">associativity</a>. A language that supports user-defined operators should also give the user a way to control these attributes for their custom operators. Modern languages do this in a variety of ways. In Swift, all operators are associated with a <a href="https://developer.apple.com/documentation/swift/operator_declarations">precedence group</a>. User-defined operators in F# get their precedence and associativity from the <a href="https://docs.microsoft.com/en-us/dotnet/fsharp/language-reference/operator-overloading">combination of characters</a> that make up the operator. In Haskell-like languages, the user explicitly states the precedence and associativity using special syntax. For example, <code>infixl 5 +</code> says “the <code>+</code> operator is left-associative and has precedence level 5”.</p>
<p>I like Haskell-style operators out of all these options — I think they’re a more elegant solution to the problem. However, elegance comes at the cost of implementation difficulty.</p>
<p>Haskell-style infix operators are generally implemented like this:</p>
<ul>
<li><p>Define a set of characters that are allowed in operators.</p></li>
<li><p>Add syntax for declaring operator precedence and associativity.</p></li>
<li><p>Parse all operators right-recursively.</p>
<p>The grammar might look something like this: ``` operator_char ::= ‘~’ | ‘!’ | ‘@’ | ‘#’ | ‘$’ | ‘%’ | ‘^’ | ‘&amp;’ | ’*’ | ‘?’ | ‘&gt;’ | ‘&lt;’ | ‘.’ | ‘|’ | ‘-’ | ‘+’ | ‘=’ operator ::= operator_char+</p>
<p>fixity ::= ‘infixr’ | ‘infixl’ infix_decl ::= fixity [0-9]+ operator declaration ::= infix_decl | …</p>
<p>non_operator_expr ::= ‘(’ expr ‘)’ | … expr ::= non_operator_expr (operator non_operator_expr)* ```</p></li>
<li><p>Collect the operator precedences and associativities from the parsed output</p></li>
<li><p>Re-write the parsed expressions according to this information</p>
<p>This is done in two parts:</p>
<ul>
<li>Associativity correction</li>
<li>Precedence correction</li>
</ul></li>
</ul>
<h2 id="re-association">Re-association</h2>
<p>Consider the input <code>2 - 3 + 4</code>. We implicitly read this as <code>[2 - 3] + 4</code>, because <code>-</code> and <code>+</code> have the same precedence and are left-associative. According to our grammar this expression will always be parsed as <code>2 - [3 + 4]</code>, which has a completely different meaning. Changing the position of the brackets accomplished by changing which operator is at the top of the tree. <code>(-)</code> is at the top of the tree in the expression <code>2 - [3 + 4]</code>, but <code>(+)</code> is at the top in <code>[2 - 3] + 4</code>. Notice that the order of the leaves — <code>2, 3, 4</code> — stays the same regardless of how the tree is transformed.</p>
<p>The re-association algorithm for a tree of height two looks like this:</p>
<pre><code>Left associative operators:

  OP1(prec=X, assoc=Left)
  /             \
 /               \
A      OP2(prec=Y, assoc=Left)
       /                 \
      /                   \
     B                     C


if X == Y, becomes


            OP2(prec=Y, assoc=Left)
            /                \
           /                  \
  OP1(prec=X, assoc=Left)      C
  /                 \
 /                   \
A                     B</code></pre>
<pre><code>Right associative operators

            OP1(prec=X, assoc=Right)
            /                \
           /                  \
  OP2(prec=Y, assoc=Right)     C
  /                 \
 /                   \
A                     B


if X == Y, becomes


  OP2(prec=Y, assoc=Right)
  /             \
 /               \
A      OP1(prec=X, assoc=Right)
       /                 \
      /                   \
     B                     C</code></pre>
<p>If a parent and child node have different precedences and different associativities, then they won’t be re-associated. If they have the same precedence but different associativities, an “ambiguous parse” error is raised.</p>
<p>If we have two operators <code>!</code> and <code>^</code>, with the same precedence and different associativity, any unparenthesised expression that uses them has two valid parenthesisations — <code>5 ^ 4 ! 3</code> could be <code>[5 ^ 4] ! 3</code> or <code>5 ^ [4 ! 3]</code>.</p>
<h2 id="precedence-correction">Precedence Correction</h2>
<p>Consider the input <code>2 * 3 + 4</code>. This is read as <code>[2 * 3] + 4</code>, because <code>*</code> has higher precedence than <code>+</code>, but it will be parsed as <code>2 * [3 + 4]</code>.</p>
<p>The precedence-correction algorithm looks like this:</p>
<pre><code>  OP1(prec=X, assoc=?)
  /             \
 /               \
A      OP2(prec=Y, assoc=??)
       /                 \
      /                   \
     B                     C


if X &gt; Y, becomes


            OP2(prec=Y, assoc=??)
            /                \
           /                  \
  OP1(prec=X, assoc=?)         C
  /                 \
 /                   \
A                     B


and



            OP1(prec=X, assoc=?)
            /                \
           /                  \
  OP2(prec=Y, assoc=??)        C
  /                 \
 /                   \
A                     B


if X &gt; Y, becomes


  OP2(prec=Y, assoc=??)
  /             \
 /               \
A      OP1(prec=X, assoc=?)
       /                 \
      /                   \
     B                     C</code></pre>
<h2 id="putting-it-together">Putting it together</h2>
<p>This seems fairly straightforward when thinking about trees of height 2, but how do you generalise it to trees of any height?</p>
<p>This is where <code>Plated</code> comes in. To make your datatype an instance of <a href="https://hackage.haskell.org/package/lens/docs/Control-Lens-Plated.html#t:Plated">Plated</a>, you write a <a href="https://hackage.haskell.org/package/lens/docs/Control-Lens-Type.html#t:Traversal">Traversal</a> that operates over its immediate self-similar children. <a href="https://hackage.haskell.org/package/lens/docs/Control-Lens-Plated.html#v:rewriteM">rewriteM</a> then allows you to write transformations on trees as deep or as shallow as you wish, interleaving a monadic context, and will recursively apply those transformations from the bottom of a tree upwards until it can no longer be transformed.</p>
<p>Good abstractions reduce boilerplate and help you focus on what’s important. The “recursive bottom-up tree rewriting” algorithm has already been written for us. Using <code>Plated</code>, we need only consider the simplest case for re-ordering the tree, and then it scales for free.</p>
<p><br></p>
<p>Let’s write some code.</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb4-1"><a href="#cb4-1"></a><span class="kw">module</span> <span class="dt">Operators</span> <span class="kw">where</span></span>
<span id="cb4-2"><a href="#cb4-2"></a></span>
<span id="cb4-3"><a href="#cb4-3"></a><span class="kw">import</span> <span class="dt">Control.Applicative</span> ((&lt;|&gt;), liftA2)</span>
<span id="cb4-4"><a href="#cb4-4"></a><span class="kw">import</span> <span class="dt">Control.Lens.Plated</span> (<span class="dt">Plated</span>(..), rewriteM)</span>
<span id="cb4-5"><a href="#cb4-5"></a><span class="kw">import</span> <span class="dt">Data.Maybe</span> (fromMaybe)</span></code></pre></div>
<p><br></p>
Our syntax tree will consist of binary operators and numbers. The <code>Parens</code> node is for explicit parenthesisation.
<div class="sourceCode" id="cb5"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb5-1"><a href="#cb5-1"></a><span class="kw">data</span> <span class="dt">Expr</span></span>
<span id="cb5-2"><a href="#cb5-2"></a>  <span class="ot">=</span> <span class="dt">Parens</span> <span class="dt">Expr</span></span>
<span id="cb5-3"><a href="#cb5-3"></a>  <span class="op">|</span> <span class="dt">BinOp</span> <span class="dt">String</span> <span class="dt">Expr</span> <span class="dt">Expr</span></span>
<span id="cb5-4"><a href="#cb5-4"></a>  <span class="op">|</span> <span class="dt">Number</span> <span class="dt">Int</span></span>
<span id="cb5-5"><a href="#cb5-5"></a>  <span class="kw">deriving</span> (<span class="dt">Eq</span>, <span class="dt">Ord</span>, <span class="dt">Show</span>)</span></code></pre></div>
<p><br></p>
The wonderful <code>Plated</code> instance.
<div class="sourceCode" id="cb6"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb6-1"><a href="#cb6-1"></a><span class="kw">instance</span> <span class="dt">Plated</span> <span class="dt">Expr</span> <span class="kw">where</span></span>
<span id="cb6-2"><a href="#cb6-2"></a>  plate f (<span class="dt">Parens</span> a) <span class="ot">=</span> <span class="dt">Parens</span> <span class="op">&lt;$&gt;</span> f a</span>
<span id="cb6-3"><a href="#cb6-3"></a>  plate f (<span class="dt">BinOp</span> n a b) <span class="ot">=</span> <span class="dt">BinOp</span> n <span class="op">&lt;$&gt;</span> f a <span class="op">&lt;*&gt;</span> f b</span>
<span id="cb6-4"><a href="#cb6-4"></a>  plate _ a <span class="ot">=</span> <span class="fu">pure</span> a</span></code></pre></div>
<p><br></p>
Operators have an associativity and a precedence.
<div class="sourceCode" id="cb7"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb7-1"><a href="#cb7-1"></a><span class="kw">data</span> <span class="dt">Associativity</span></span>
<span id="cb7-2"><a href="#cb7-2"></a>  <span class="ot">=</span> <span class="dt">AssocL</span></span>
<span id="cb7-3"><a href="#cb7-3"></a>  <span class="op">|</span> <span class="dt">AssocR</span></span>
<span id="cb7-4"><a href="#cb7-4"></a>  <span class="kw">deriving</span> (<span class="dt">Eq</span>, <span class="dt">Ord</span>, <span class="dt">Show</span>)</span>
<span id="cb7-5"><a href="#cb7-5"></a></span>
<span id="cb7-6"><a href="#cb7-6"></a><span class="kw">data</span> <span class="dt">OperatorInfo</span></span>
<span id="cb7-7"><a href="#cb7-7"></a>  <span class="ot">=</span> <span class="dt">OperatorInfo</span></span>
<span id="cb7-8"><a href="#cb7-8"></a>  {<span class="ot"> opAssoc ::</span> <span class="dt">Associativity</span></span>
<span id="cb7-9"><a href="#cb7-9"></a>  ,<span class="ot"> opPrecedence ::</span> <span class="dt">Int</span></span>
<span id="cb7-10"><a href="#cb7-10"></a>  }</span></code></pre></div>
<p><br></p>
An <code>OpTable</code> is a map from operator names to their <code>OperatorInfo</code>.
<div class="sourceCode" id="cb8"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb8-1"><a href="#cb8-1"></a><span class="kw">type</span> <span class="dt">OpTable</span> <span class="ot">=</span> [(<span class="dt">String</span>, <span class="dt">OperatorInfo</span>)]</span></code></pre></div>
<p><br></p>
Our reordering function could fail due to ambiguity
<div class="sourceCode" id="cb9"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb9-1"><a href="#cb9-1"></a><span class="kw">data</span> <span class="dt">ParseError</span> <span class="ot">=</span> <span class="dt">AmbiguousParse</span></span>
<span id="cb9-2"><a href="#cb9-2"></a>  <span class="kw">deriving</span> (<span class="dt">Eq</span>, <span class="dt">Show</span>, <span class="dt">Ord</span>)</span></code></pre></div>
<p><br></p>
<p>Re-association. This code crashes when an operator is missing from the table.</p>
<ul>
<li><p>If the input node is an operator, and:</p>
<ul>
<li>It is left-associative:
<ul>
<li>Inspect its right child
<ul>
<li>If the right child node is an operator and has equal precedence to the input node
<ul>
<li>And is also left-associative, then re-order the tree to be left-associative</li>
<li>And is right-associative, then report an ambiguity</li>
</ul></li>
</ul></li>
<li>Inspect its left child
<ul>
<li>If the left child node is an operator, has equal precedence to the input node and is right-associative, then report an ambiguity</li>
</ul></li>
</ul></li>
<li>It is right-associative
<ul>
<li>Inspect its left child
<ul>
<li>If the left child node is an operator and has equal precedence to the input node
<ul>
<li>And is also right-associative, then re-order the tree to be right-associative</li>
<li>And is left-associative, then report an ambiguity</li>
</ul></li>
</ul></li>
<li>Inspect its right child
<ul>
<li>If the right child node is an operator, has equal precedence to the input node and is left-associative, then report an ambiguity</li>
</ul></li>
</ul></li>
</ul></li>
<li><p>Otherwise, do nothing</p></li>
</ul>
<div class="sourceCode" id="cb10"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb10-1"><a href="#cb10-1"></a><span class="ot">associativity ::</span> <span class="dt">OpTable</span> <span class="ot">-&gt;</span> <span class="dt">Expr</span> <span class="ot">-&gt;</span> <span class="dt">Either</span> <span class="dt">ParseError</span> (<span class="dt">Maybe</span> <span class="dt">Expr</span>)</span>
<span id="cb10-2"><a href="#cb10-2"></a>associativity table (<span class="dt">BinOp</span> name l r)</span>
<span id="cb10-3"><a href="#cb10-3"></a>  <span class="op">|</span> <span class="dt">Just</span> entry <span class="ot">&lt;-</span> <span class="fu">lookup</span> name table <span class="ot">=</span></span>
<span id="cb10-4"><a href="#cb10-4"></a>      <span class="kw">case</span> opAssoc entry <span class="kw">of</span></span>
<span id="cb10-5"><a href="#cb10-5"></a>        <span class="dt">AssocL</span></span>
<span id="cb10-6"><a href="#cb10-6"></a>          <span class="op">|</span> <span class="dt">BinOp</span> name' l' r' <span class="ot">&lt;-</span> r</span>
<span id="cb10-7"><a href="#cb10-7"></a>          , <span class="dt">Just</span> entry' <span class="ot">&lt;-</span> <span class="fu">lookup</span> name' table</span>
<span id="cb10-8"><a href="#cb10-8"></a>          , opPrecedence entry <span class="op">==</span> opPrecedence entry' <span class="ot">-&gt;</span></span>
<span id="cb10-9"><a href="#cb10-9"></a>              <span class="kw">case</span> opAssoc entry' <span class="kw">of</span></span>
<span id="cb10-10"><a href="#cb10-10"></a>                <span class="dt">AssocL</span> <span class="ot">-&gt;</span> <span class="dt">Right</span> <span class="op">.</span> <span class="dt">Just</span> <span class="op">$</span> <span class="dt">BinOp</span> name' (<span class="dt">BinOp</span> name l l') r'</span>
<span id="cb10-11"><a href="#cb10-11"></a>                <span class="dt">AssocR</span> <span class="ot">-&gt;</span> <span class="dt">Left</span> <span class="dt">AmbiguousParse</span></span>
<span id="cb10-12"><a href="#cb10-12"></a>          <span class="op">|</span> <span class="dt">BinOp</span> name' _ _ <span class="ot">&lt;-</span> l</span>
<span id="cb10-13"><a href="#cb10-13"></a>          , <span class="dt">Just</span> entry' <span class="ot">&lt;-</span> <span class="fu">lookup</span> name' table</span>
<span id="cb10-14"><a href="#cb10-14"></a>          , opPrecedence entry <span class="op">==</span> opPrecedence entry'</span>
<span id="cb10-15"><a href="#cb10-15"></a>          , <span class="dt">AssocR</span> <span class="ot">&lt;-</span> opAssoc entry' <span class="ot">-&gt;</span></span>
<span id="cb10-16"><a href="#cb10-16"></a>              <span class="dt">Left</span> <span class="dt">AmbiguousParse</span></span>
<span id="cb10-17"><a href="#cb10-17"></a>          <span class="op">|</span> <span class="fu">otherwise</span> <span class="ot">-&gt;</span> <span class="dt">Right</span> <span class="dt">Nothing</span></span>
<span id="cb10-18"><a href="#cb10-18"></a>        <span class="dt">AssocR</span></span>
<span id="cb10-19"><a href="#cb10-19"></a>          <span class="op">|</span> <span class="dt">BinOp</span> name' l' r' <span class="ot">&lt;-</span> l</span>
<span id="cb10-20"><a href="#cb10-20"></a>          , <span class="dt">Just</span> entry' <span class="ot">&lt;-</span> <span class="fu">lookup</span> name' table</span>
<span id="cb10-21"><a href="#cb10-21"></a>          , opPrecedence entry <span class="op">==</span> opPrecedence entry' <span class="ot">-&gt;</span></span>
<span id="cb10-22"><a href="#cb10-22"></a>              <span class="kw">case</span> opAssoc entry' <span class="kw">of</span></span>
<span id="cb10-23"><a href="#cb10-23"></a>                <span class="dt">AssocL</span> <span class="ot">-&gt;</span> <span class="dt">Left</span> <span class="dt">AmbiguousParse</span></span>
<span id="cb10-24"><a href="#cb10-24"></a>                <span class="dt">AssocR</span> <span class="ot">-&gt;</span> <span class="dt">Right</span> <span class="op">.</span> <span class="dt">Just</span> <span class="op">$</span> <span class="dt">BinOp</span> name' l' (<span class="dt">BinOp</span> name r' r)</span>
<span id="cb10-25"><a href="#cb10-25"></a>          <span class="op">|</span> <span class="dt">BinOp</span> name' _ _ <span class="ot">&lt;-</span> r</span>
<span id="cb10-26"><a href="#cb10-26"></a>          , <span class="dt">Just</span> entry' <span class="ot">&lt;-</span> <span class="fu">lookup</span> name' table</span>
<span id="cb10-27"><a href="#cb10-27"></a>          , opPrecedence entry <span class="op">==</span> opPrecedence entry'</span>
<span id="cb10-28"><a href="#cb10-28"></a>          , <span class="dt">AssocL</span> <span class="ot">&lt;-</span> opAssoc entry' <span class="ot">-&gt;</span></span>
<span id="cb10-29"><a href="#cb10-29"></a>              <span class="dt">Left</span> <span class="dt">AmbiguousParse</span></span>
<span id="cb10-30"><a href="#cb10-30"></a>          <span class="op">|</span> <span class="fu">otherwise</span> <span class="ot">-&gt;</span> <span class="dt">Right</span> <span class="dt">Nothing</span></span>
<span id="cb10-31"><a href="#cb10-31"></a>associativity _ _ <span class="ot">=</span> <span class="dt">Right</span> <span class="dt">Nothing</span></span></code></pre></div>
<p><br></p>
<p>Precedence correction. This code also crashes when operators are missing from the operator table.</p>
<p>This is broken down into two phases- making sure the left branch is precedence-correct with respect to the input node, and then doing the same for the left branch.</p>
For each branch, if that branch contains an operator with a lower precedence than the input node, re-order the tree so the lower-precedence operator is at the top.
<div class="sourceCode" id="cb11"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb11-1"><a href="#cb11-1"></a><span class="ot">precedence ::</span> <span class="dt">OpTable</span> <span class="ot">-&gt;</span> <span class="dt">Expr</span> <span class="ot">-&gt;</span> <span class="dt">Maybe</span> <span class="dt">Expr</span></span>
<span id="cb11-2"><a href="#cb11-2"></a>precedence table e<span class="op">@</span>(<span class="dt">BinOp</span> name _ _)</span>
<span id="cb11-3"><a href="#cb11-3"></a>  <span class="op">|</span> <span class="dt">Just</span> entry <span class="ot">&lt;-</span> <span class="fu">lookup</span> name table <span class="ot">=</span></span>
<span id="cb11-4"><a href="#cb11-4"></a>      checkR entry <span class="op">$</span> fromMaybe e (checkL entry e)</span>
<span id="cb11-5"><a href="#cb11-5"></a>  <span class="kw">where</span></span>
<span id="cb11-6"><a href="#cb11-6"></a>    checkL entry (<span class="dt">BinOp</span> name l c) <span class="ot">=</span></span>
<span id="cb11-7"><a href="#cb11-7"></a>      <span class="kw">case</span> l <span class="kw">of</span></span>
<span id="cb11-8"><a href="#cb11-8"></a>        <span class="dt">BinOp</span> name' a b </span>
<span id="cb11-9"><a href="#cb11-9"></a>          <span class="op">|</span> <span class="dt">Just</span> entry' <span class="ot">&lt;-</span> <span class="fu">lookup</span> name' table</span>
<span id="cb11-10"><a href="#cb11-10"></a>          , opPrecedence entry' <span class="op">&lt;</span> opPrecedence entry <span class="ot">-&gt;</span></span>
<span id="cb11-11"><a href="#cb11-11"></a>              <span class="dt">Just</span> <span class="op">$</span> <span class="dt">BinOp</span> name' a (<span class="dt">BinOp</span> name b c)</span>
<span id="cb11-12"><a href="#cb11-12"></a>        _ <span class="ot">-&gt;</span> <span class="dt">Nothing</span></span>
<span id="cb11-13"><a href="#cb11-13"></a>    checkL _ _ <span class="ot">=</span> <span class="dt">Nothing</span></span>
<span id="cb11-14"><a href="#cb11-14"></a></span>
<span id="cb11-15"><a href="#cb11-15"></a>    checkR entry (<span class="dt">BinOp</span> name a r) <span class="ot">=</span></span>
<span id="cb11-16"><a href="#cb11-16"></a>      <span class="kw">case</span> r <span class="kw">of</span></span>
<span id="cb11-17"><a href="#cb11-17"></a>        <span class="dt">BinOp</span> name' b c</span>
<span id="cb11-18"><a href="#cb11-18"></a>          <span class="op">|</span> <span class="dt">Just</span> entry' <span class="ot">&lt;-</span> <span class="fu">lookup</span> name' table</span>
<span id="cb11-19"><a href="#cb11-19"></a>          , opPrecedence entry' <span class="op">&lt;</span> opPrecedence entry <span class="ot">-&gt;</span></span>
<span id="cb11-20"><a href="#cb11-20"></a>              <span class="dt">Just</span> <span class="op">$</span> <span class="dt">BinOp</span> name' (<span class="dt">BinOp</span> name a b) c</span>
<span id="cb11-21"><a href="#cb11-21"></a>        _ <span class="ot">-&gt;</span> <span class="dt">Nothing</span></span>
<span id="cb11-22"><a href="#cb11-22"></a>    checkR _ _ <span class="ot">=</span> <span class="dt">Nothing</span></span>
<span id="cb11-23"><a href="#cb11-23"></a>precedence _ _ <span class="ot">=</span> <span class="dt">Nothing</span></span></code></pre></div>
<p><br></p>
<code>precedence</code> and <code>associativity</code> have type <code>Expr -&gt; Maybe Expr</code> because eventually the transformations will no longer be applicable. We can use <code>liftA2 (liftA2 (&lt;|&gt;))</code> to combine the two rewrite rules, and <code>rewriteM</code> will run until one produces a <code>Left</code>, or until both always produce <code>Right Nothing</code>
<div class="sourceCode" id="cb12"><pre class="sourceCode haskell literate"><code class="sourceCode haskell"><span id="cb12-1"><a href="#cb12-1"></a><span class="ot">reorder ::</span> <span class="dt">OpTable</span> <span class="ot">-&gt;</span> <span class="dt">Expr</span> <span class="ot">-&gt;</span> <span class="dt">Either</span> <span class="dt">ParseError</span> <span class="dt">Expr</span></span>
<span id="cb12-2"><a href="#cb12-2"></a>reorder table <span class="ot">=</span></span>
<span id="cb12-3"><a href="#cb12-3"></a>  rewriteM <span class="op">$</span></span>
<span id="cb12-4"><a href="#cb12-4"></a>  liftA2 (liftA2 (<span class="op">&lt;|&gt;</span>)) (<span class="dt">Right</span> <span class="op">.</span> precedence table) (associativity table)</span></code></pre></div>
<p><br></p>
<p>Let’s try it on the expression <code>5 - 4 + 3 * 2 + 1</code>. It will be parsed as <code>5 - [4 + [3 * [2 + 1]]]</code>, but after re-ordering should become <code>[[5 - 4] + [3 * 2]] + 1</code>.</p>
<pre><code>ghci&gt; let o = [(&quot;+&quot;, OperatorInfo AssocL 5), (&quot;-&quot;, OperatorInfo AssocL 5), (&quot;*&quot;, OperatorInfo AssocL 6)]
ghci&gt; let input = BinOp &quot;-&quot; (Number 5) (BinOp &quot;+&quot; (Number 4) (BinOp &quot;*&quot; (Number 3) (BinOp &quot;+&quot; (Number 2) (Number 1))))
ghci&gt; reorder o input
Right (BinOp &quot;+&quot; (BinOp &quot;+&quot; (BinOp &quot;-&quot; (Number 5) (Number 4)) (BinOp &quot;*&quot; (Number 3) (Number 2))) (Number 1))</code></pre>
<p>We can also use <code>Parens</code> to explicitly parenthesise the expression. If we input <code>5 - (4 + (3 * (2 + 1)))</code>, it will not be re-ordered at all.</p>
<pre><code>ghci&gt; let input = BinOp &quot;-&quot; (Number 5) (Parens $ BinOp &quot;+&quot; (Number 4) (Parens $ BinOp &quot;*&quot; (Number 3) (Parens $ BinOp &quot;+&quot; (Number 2) (Number 1))))
ghci&gt; reorder o input
Right (BinOp &quot;-&quot; (Number 5) (Parens (BinOp &quot;+&quot; (Number 4) (Parens (BinOp &quot;*&quot; (Number 3) (Parens (BinOp &quot;+&quot; (Number 2) (Number 1))))))))

ghci&gt; reorder o input == Right input
True</code></pre>
<p>Ambiguous expressions are reported. Here’s the example from earlier — <code>5 ^ 4 ! 3</code>:</p>
<pre><code>ghci&gt; let o = [(&quot;^&quot;, OperatorInfo AssocL 5), (&quot;!&quot;, OperatorInfo AssocR 5)]
ghci&gt; let input = BinOp &quot;^&quot; (Number 5) (BinOp &quot;!&quot; (Number 4) (Number 3))
ghci&gt; reorder o input
Left AmbiguousParse

ghci&gt; let input = BinOp &quot;!&quot; (BinOp &quot;^&quot; (Number 5) (Number 4)) (Number 3)
ghci&gt; reorder o input
Left AmbiguousParse</code></pre>
<p>And are resolved by adding explicit parentheses — <code>5 ^ (4 ! 3)</code> and <code>(5 ^ 4) ! 3</code> respectively:</p>
<pre><code>ghci&gt; let o = [(&quot;^&quot;, OperatorInfo AssocL 5), (&quot;!&quot;, OperatorInfo AssocR 5)]
ghci&gt; let input = BinOp &quot;^&quot; (Number 5) (Parens $ BinOp &quot;!&quot; (Number 4) (Number 3))
ghci&gt; reorder o input
Right (BinOp &quot;!&quot; (Number 5) (Parens (BinOp &quot;^&quot; (Number 4) (Number 3))))

ghci&gt; let input = BinOp &quot;!&quot; (Parens $ BinOp &quot;^&quot; (Number 5) (Number 4)) (Number 3)
ghci&gt; reorder o input
Right (BinOp &quot;!&quot; (Parens (BinOp &quot;^&quot; (Number 5) (Number 4)) (Number 3)))</code></pre>
    </div>
  </div>

  
  <div class="row">
    <div class="col my-2">
      
    </div>
  </div>
  

  
  <div class="row">
    <div class="col my-2">
      
        <div class="p-1">
  <h4 class="data61-bottom-line">
    > <a href="../../people/ielliott">Isaac Elliott</a>
  </h4>
  <p>Isaac <em>really</em> likes types</p>
</div>

      
    </div>
  </div>
  
</div>

          </div>
          <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li role="presentation"><a href="../../">Home</a></li>
                  <li role="presentation"><a href="../../location">Location</a></li>
                  <li role="presentation"><a href="../../people">People</a></li>
                  <li role="presentation"><a href="../../projects">Projects</a></li>
                  <li role="presentation"><a href="../../talks">Talks</a></li>
                  <li role="presentation"><a href="../../contact">Contact</a></li>
                  <li role="presentation"><a href="../../archive">Archive</a></li>
              </ul>
          </div>
        </main>

        <footer class="page-footer data61-green">
            <div class="container">
              <div class="row justify-content-centre">
                <div class="col">
                  <a href="https://github.com/qfpl"><i class="text-white fa fa-github fa-2x"></i></a>
                  <a href="https://twitter.com/queenslandfplab"><i class="text-white fa fa-twitter fa-2x"></i></a>
                  <a href="mailto:contact@qfpl.io"><i class="text-white fa fa-envelope-o fa-2x"></i></a>
                </div>
                <div class="col">
                  <a class="float-right" rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
                  </a>
                </div>
              </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>

        <script src="../../js/bootstrap.min.js"></script>

        
          
        
    </body>
</html>
