<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Queensland FP Lab - Automatic Testing of Haskell Projects with Travis CI</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../../css/custom.css">
        <link rel="stylesheet" href="../../css/syntax.css">
        
          
        
        <link rel="alternate" type="application/atom+xml" title="Queensland Functional Programming Lab Blog" href="../../atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Queensland Functional Programming Lab Blog" href="../../rss.xml">
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106321204-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments)};
          gtag('js', new Date());

          gtag('config', 'UA-106321204-1', { 'anonymize_ip': true });
        </script>
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg data61-green">
              <a class="navbar-brand text-white" href="../../">
                <div class="card brand-logo">
                  <div class="card-body">
                    <div class="brand-anim-block">
                      <img id="qfpl-brand-data61-logo" height="55" alt="Data61 logo" src="../../images/DATA61-CSIRO_MONO.png" />
                    </div>
                  </div>
                </div>
              </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <i class="fa fa-lg fa-fw fa-bars text-white"></i>
              </button>

              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../location">Location</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../people">People</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../talks">Talks</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../contact">Contact</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../archive">Archive</a>
                    </li>
                  </ul>
                </div>
            </nav>
        </header>

        <main>
          <div class="container-fluid">
              <div class="container">
  <div class="row">
    <div class="col my-2">
      <h2>Automatic Testing of Haskell Projects with Travis CI</h2>
      <small>Posted on November 26, 2019</small>
    </div>
  </div>

  <div class="row">
    <div class="col my-2">
      <p>In this post you will learn how to quickly and easily set up continuous integration for your open source Haskell projects hosted on GitHub. Every time you push a commit, a free service called Travis CI will compile your code and run any test suites you have. I find this extremely valuable and I hope you will too. Best of all, there’s very little effort involved.</p>
<p>Before we begin, this guide is for projects built with cabal. If you primarily use <code>stack</code> or <code>nix</code> for building your code, this post will not be very useful to you.</p>
<h2 id="motivation">Motivation</h2>
<p>This kind of automated testing is great because it helps me catch errors on all the GHC versions my code claims to support. This is much easier than testing with every GHC version every time I want to push a commit, and gives me higher confidence. Furthermore, as a contributor to other people’s open source projects, I feel more confident that my pull request is correct when Travis or other CI passes. As a maintainer, this also helps me screen incoming pull requests on my own projects.</p>
<h2 id="instructions">Instructions</h2>
<p>For Travis to run, you need to enable Travis on your repository at <a href="https://travis-ci.org/">https://travis-ci.org/</a>. If you visit that link, you should be able to sign in with your GitHub account and enable the repository by following their instructions.</p>
<p>Next, you need a <code>.travis.yml</code> script to your repository to tell Travis how to build your project.</p>
<h3 id="generating-a-.travis.yml">Generating a <code>.travis.yml</code></h3>
<p>There is a tool to automatically generate .travis.yml for Haskell projects, called <code>haskell-ci</code>. You could always write your own <code>.travis.yml</code> file, but I prefer this way because <code>haskell-ci</code> generates scripts that are very featureful, including things like caching to speed up subsequent builds, and I get to offload knowing how Travis works.</p>
<p>You can install <code>haskell-ci</code> from Hackage:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1"></a>$ <span class="ex">cabal</span> new-update</span>
<span id="cb1-2"><a href="#cb1-2"></a>$ <span class="ex">cabal</span> new-install haskell-ci</span></code></pre></div>
<p>You should now have <code>haskell-ci</code> available at your shell, assuming you have already added <code>~/.cabal/bin</code> to your <code>PATH</code>.</p>
<p>Next, you need to add some metadata to your cabal file: the <code>tested-with</code> field. This field will tell <code>haskell-ci</code> which GHC versions you want to test.</p>
<p>Add a line like the following to your cabal file:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb2-1"><a href="#cb2-1"></a>tested<span class="op">-</span>with<span class="op">:</span> <span class="dt">GHC</span> <span class="op">==</span> <span class="fl">8.8</span><span class="op">.</span><span class="dv">1</span></span></code></pre></div>
<p>If you support more than one GHC version in your project, you can comma separate them:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb3-1"><a href="#cb3-1"></a>tested<span class="op">-</span>with<span class="op">:</span></span>
<span id="cb3-2"><a href="#cb3-2"></a>  <span class="dt">GHC</span> <span class="op">==</span> <span class="fl">8.8</span><span class="op">.</span><span class="dv">1</span>,</span>
<span id="cb3-3"><a href="#cb3-3"></a>  <span class="dt">GHC</span> <span class="op">==</span> <span class="fl">8.6</span><span class="op">.</span><span class="dv">5</span>,</span>
<span id="cb3-4"><a href="#cb3-4"></a>  <span class="dt">GHC</span> <span class="op">==</span> <span class="fl">8.4</span><span class="op">.</span><span class="dv">4</span></span></code></pre></div>
<p>Now you can run <code>haskell-ci</code> on your cabal file to generate a <code>.travis.yml</code> file:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1"></a>$ <span class="ex">haskell-ci</span> my-awesome-package.cabal</span></code></pre></div>
<p>If you’re using a multi-package project with a <code>cabal.project</code> file, you can instead run:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1"></a>$ <span class="ex">haskell-ci</span> cabal.project</span></code></pre></div>
<p>This gives you CI that builds all the packages in your project together. You can find an example of this in <a href="https://github.com/qfpl/sv">the repository for sv</a>, QFPL’s CSV library. You will need to add <code>tested-with</code> metadata to each of the cabal files in your project, and they have to agree on GHC versions.</p>
<p>Now you can commit your <code>.travis.yml</code> file, push the commit to GitHub, and a Travis build should be automatically kicked off. Now you’re good to go!</p>
<h3 id="maintenance">Maintenance</h3>
<p>When a new GHC version is released, update your library to support it and then update your <code>tested-with</code> clause. You will also need to update <code>haskell-ci</code> itself, since it needs a new release to learn about the new GHC version.</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1"></a>$ <span class="ex">cabal</span> new-update</span>
<span id="cb6-2"><a href="#cb6-2"></a>$ <span class="fu">rm</span> ~/.cabal/bin/haskell-ci <span class="co"># delete the old binary</span></span>
<span id="cb6-3"><a href="#cb6-3"></a>$ <span class="ex">cabal</span> new-install haskell-ci <span class="co"># install the new version</span></span></code></pre></div>
<p>Now you can run <code>haskell-ci</code> again as above. That’s all.</p>
<h2 id="conclusion">Conclusion</h2>
<p>I find Travis CI with the <code>haskell-ci</code> tool a very low effort way to test every commit of my Haskell code against multiple GHC versions. That is very valuable to me, and I hope it is valuable to you too.</p>
    </div>
  </div>

  
  <div class="row">
    <div class="col my-2">
      <div class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title">
      <a href="../../projects/infra">The Functional Infrastructure Project</a>
    </h2>
  </div>
  <div class="panel-body">
    <p>We’re using functional programming to set up the infrastructure that we use from day to day.</p>
  </div>
</div>

    </div>
  </div>
  

  
  <div class="row">
    <div class="col my-2">
      
        <div class="p-1">
  <h4 class="data61-bottom-line">
    > <a href="../../people/gwilson">George Wilson</a>
  </h4>
  <p>George Wilson is an enthusiastic programmer at the Queensland Functional Programming Lab. George enjoys writing code, and speaking and writing about Haskell and functional programming.</p>
</div>

      
    </div>
  </div>
  
</div>

          </div>
          <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li role="presentation"><a href="../../">Home</a></li>
                  <li role="presentation"><a href="../../location">Location</a></li>
                  <li role="presentation"><a href="../../people">People</a></li>
                  <li role="presentation"><a href="../../projects">Projects</a></li>
                  <li role="presentation"><a href="../../talks">Talks</a></li>
                  <li role="presentation"><a href="../../contact">Contact</a></li>
                  <li role="presentation"><a href="../../archive">Archive</a></li>
              </ul>
          </div>
        </main>

        <footer class="page-footer data61-green">
            <div class="container">
              <div class="row justify-content-centre">
                <div class="col">
                  <a href="https://github.com/qfpl"><i class="text-white fa fa-github fa-2x"></i></a>
                  <a href="https://twitter.com/queenslandfplab"><i class="text-white fa fa-twitter fa-2x"></i></a>
                  <a href="mailto:contact@qfpl.io"><i class="text-white fa fa-envelope-o fa-2x"></i></a>
                </div>
                <div class="col">
                  <a class="float-right" rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
                  </a>
                </div>
              </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>

        <script src="../../js/bootstrap.min.js"></script>

        
          
        
    </body>
</html>
