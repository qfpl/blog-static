<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Queensland FP Lab - Our Approach to Haskell Dependencies</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../../css/custom.css">
        <link rel="stylesheet" href="../../css/syntax.css">
        
          
        
        <link rel="alternate" type="application/atom+xml" title="Queensland Functional Programming Lab Blog" href="../../atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Queensland Functional Programming Lab Blog" href="../../rss.xml">
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106321204-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments)};
          gtag('js', new Date());

          gtag('config', 'UA-106321204-1', { 'anonymize_ip': true });
        </script>
    </head>
    <body>

        <header>
            <nav class="navbar navbar-expand-lg data61-green">
              <a class="navbar-brand text-white" href="../../">
                <div class="card brand-logo">
                  <div class="card-body">
                    <div class="brand-anim-block">
                      <img id="qfpl-brand-data61-logo" height="55" alt="Data61 logo" src="../../images/DATA61-CSIRO_MONO.png" />
                    </div>
                  </div>
                </div>
              </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <i class="fa fa-lg fa-fw fa-bars text-white"></i>
              </button>

              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../location">Location</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../people">People</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../projects">Projects</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../talks">Talks</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../contact">Contact</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="../../archive">Archive</a>
                    </li>
                  </ul>
                </div>
            </nav>
        </header>

        <main>
          <div class="container-fluid">
              <div class="container">
  <div class="row">
    <div class="col my-2">
      <h2>Our Approach to Haskell Dependencies</h2>
      <small>Posted on September 11, 2017</small>
    </div>
  </div>

  <div class="row">
    <div class="col my-2">
      <p>It is no secret that we at the Queensland Functional Programming lab love Haskell. In particular we are interested in writing libraries for others to depend on. Primarily, Haskell packages come from <a href="https://hackage.haskell.org/">Hackage</a> and are managed with <a href="https://www.haskell.org/cabal/">cabal</a><a href="#fn1" class="footnote-ref" id="fnref1" role="doc-noteref"><sup>1</sup></a></p>
<p>This brings us to the question of how to specify our packages’ version numbers and our own dependencies in such a way that a user is likely to have a good day. This article gives a brief overview of our approach.</p>
<h1 id="glasgow-haskell-compiler-versions">Glasgow Haskell Compiler Versions</h1>
<p>Firstly we make sure our packages support the most recent stable major release of GHC, as that is usually what we are developing against. At the time of writing that is version 8.2.1.</p>
<p>Since not everyone upgrades immediately, we also try our best to support the second-most recent major version, which is 8.0.2 at the time of writing.</p>
<p>When possible, we also try to support older major versions of GHC shipped by more slowly-moving Linux distributions: examples include <a href="https://packages.ubuntu.com/search?keywords=ghc">Ubuntu</a> or <a href="https://software.opensuse.org/package/ghc">OpenSUSE</a>. At the time of writing that means we’re supporting at least as far back as version 7.10.3</p>
<p>If you are using an older major version of GHC than is listed here and you’d like to use our software, please get in <a href="../../contact">contact</a> with us, or open an issue on our <a href="https://github.com/qfpl">GitHub</a> We can see what we can do to get the code working on your GHC.</p>
<h1 id="version-bounds">Version Bounds</h1>
<p>Our libraries tend to depend on many other libraries, which are managed using cabal and its version bounds system.</p>
<p>The <a href="https://pvp.haskell.org/">Package Version Policy</a> explains how version numbers of Haskell packages work and how bounds should be specified. In particular, the PVP says that both lower and upper bounds shall be specified for every dependency. It is our experience that packages which comply with this version bound policy tend to have a better track record for installing correctly. Particularly the lack of upper bounds can lead to cabal selecting bad install plans in old versions of the package, or with new versions of GHC.</p>
<h3 id="determining-appropriate-lower-bounds">Determining appropriate lower bounds</h3>
<p>To determine appropriate lower bounds for a particular package, we check the changelog of the package in question and assess this based on what features we need. Often we will not need the entire power of the library, so we can go a few major versions back.</p>
<p>We also check the <a href="https://matrix.hackage.haskell.org">Hackage matrix builder</a> and avoid versions which have red squares, or don’t support GHC versions we’re targeting.</p>
<h3 id="determining-appropriate-upper-bounds">Determining appropriate upper bounds</h3>
<p>Determining the correct upper bound is more difficult. Recall that the first two components of a version number as specified by the PVP together form the major version. So a major version looks like 4.12 rather than 4.</p>
<p>To be on the safe side, we often select the major version number immediately after the major version you are using.</p>
<p>A more flexible approach is to put the bound on the first segment of the version only. An example of an upper bound like this is <code>base &gt;= 4.7 &amp;&amp; &lt; 5</code>. This kind of dependency seems common among more experienced Haskellers for certain libraries, such as base. It cannot be recommended in general because a change in the second segment of a version can still spell disaster for an unsuspecting project.</p>
<p>If we are not sure what to do with your bounds, we often look at the libraries of well-regarded Haskellers, particularly if they depend on the package we’re interested in. The <a href="https://github.com/ekmett/lens">lens</a> library, for example, has a lot of dependencies so it has a lot of bounds from which to draw inspiration.</p>
<h2 id="tools-we-find-useful">Tools we find useful</h2>
<h3 id="travisci">TravisCI</h3>
<p><a href="https://travis-ci.org/">Travis CI</a> is a useful continuous integration tool that’s free for open source projects. We use <a href="https://github.com/hvr/multi-ghc-travis">this script</a> to generate a sensible default Travis file for all the GHC versions we care about. It seems to work very well.</p>
<p>The advantage of a tool like Travis is that your branches and pull requests will be compiled against each version of GHC specified, which helps make sure we’re supporting older GHCs even if nobody on the team is running them.</p>
<h3 id="hackage-matrix-builder">Hackage Matrix Builder</h3>
<p>It is very useful to check the <a href="https://matrix.hackage.haskell.org">Hackage matrix builder</a> to check on older versions of our own libraries, to see whether they need any bounds adjustments. It is also useful to check the state of packages you intend to depend on, as discussed above.</p>
<h3 id="hackage-metadata-revisions">Hackage Metadata Revisions</h3>
<p>When versions of our package start going red in the hackage matrix builder, there is a solution. Hackage accepts metadata revisions, meaning version bounds can be altered retroactively. This feature is very useful to fix red entries in the matrix, or fence off versions which are known to be bad, for instance if they include a bug.</p>
<p>The existence of this feature is not a good reason to leave off upper bounds; upper bounds are preventative, whereas metadata revisions are merely curative.</p>
<section class="footnotes" role="doc-endnotes">
<hr />
<ol>
<li id="fn1" role="doc-endnote"><p>For our own work, we mostly use cabal with <a href="../../posts/nix/introducing-nix">Nix</a>.<a href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
</section>
    </div>
  </div>

  
  <div class="row">
    <div class="col my-2">
      <div class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title">
      <a href="../../projects/infra">The Functional Infrastructure Project</a>
    </h2>
  </div>
  <div class="panel-body">
    <p>We’re using functional programming to set up the infrastructure that we use from day to day.</p>
  </div>
</div>

    </div>
  </div>
  

  
  <div class="row">
    <div class="col my-2">
      
        <div class="p-1">
  <h4 class="data61-bottom-line">
    > <a href="../../people/gwilson">George Wilson</a>
  </h4>
  <p>George Wilson is an enthusiastic programmer at the Queensland Functional Programming Lab. George enjoys writing code, and speaking and writing about Haskell and functional programming.</p>
</div>

      
    </div>
  </div>
  
</div>

          </div>
          <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li role="presentation"><a href="../../">Home</a></li>
                  <li role="presentation"><a href="../../location">Location</a></li>
                  <li role="presentation"><a href="../../people">People</a></li>
                  <li role="presentation"><a href="../../projects">Projects</a></li>
                  <li role="presentation"><a href="../../talks">Talks</a></li>
                  <li role="presentation"><a href="../../contact">Contact</a></li>
                  <li role="presentation"><a href="../../archive">Archive</a></li>
              </ul>
          </div>
        </main>

        <footer class="page-footer data61-green">
            <div class="container">
              <div class="row justify-content-centre">
                <div class="col">
                  <a href="https://github.com/qfpl"><i class="text-white fa fa-github fa-2x"></i></a>
                  <a href="https://twitter.com/queenslandfplab"><i class="text-white fa fa-twitter fa-2x"></i></a>
                  <a href="mailto:contact@qfpl.io"><i class="text-white fa fa-envelope-o fa-2x"></i></a>
                </div>
                <div class="col">
                  <a class="float-right" rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
                  </a>
                </div>
              </div>
            </div>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>

        <script src="../../js/bootstrap.min.js"></script>

        
          
        
    </body>
</html>
